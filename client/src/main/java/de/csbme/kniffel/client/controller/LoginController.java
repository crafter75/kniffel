package de.csbme.kniffel.client.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXCheckBox;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import de.csbme.kniffel.client.Attributes;
import de.csbme.kniffel.client.Main;
import de.csbme.kniffel.client.util.*;
import de.csbme.kniffel.lib.error.HTTPCodes;
import de.csbme.kniffel.lib.network.packet.UserLoginPacket;
import de.csbme.kniffel.lib.network.packet.UserRegisterPacket;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.web.WebView;
import lombok.Getter;

import java.net.URL;
import java.util.ResourceBundle;

@Getter
public class LoginController implements Initializable {

    public static final int MIN_USERNAME_LENGTH = 5, MAX_USERNAME_LENGTH = 12;

    public static final int MIN_PASSWORD_LENGTH = 6, MAX_PASSWORD_LENGTH = 32;

    @FXML
    GridPane grdMain, grdLogin;

    @FXML
    WebView wvBackground;

    @FXML
    Pane pnToS;

    @FXML
    StackPane stckSignIn, stckSignUp;

    @FXML
    BorderPane brdrSignIn, brdrSignUp;

    @FXML
    Rectangle lnSignIn, lnSignUp;

    @FXML
    ImageView ivDiceSignIn, ivDiceSignUp;

    @FXML
    Label lblTitleSignIn, lblTitleSignUp, lblToLogin, lblToRegister, lblGast;

    @FXML
    JFXTextField txtUsernameSignIn, txtUsernameSignUp;

    @FXML
    JFXPasswordField pwdSignIn, pwdSignUp, pwdConfirmSignUp;

    @FXML
    JFXCheckBox cbSavePassword, cbToS;

    @FXML
    JFXButton btnSignIn, btnSignUp;

    /**
     * Called when a player logged in
     *
     * @param event for the login
     */
    private void onLoginActionEvent( ActionEvent event ) {
        // Check if client connected
        if ( !Main.getInstance().getNettyClient().isConnected() ) {
            NotificationUtil.show( "Server offline", "Es konnte keine Verbindung zum Server hergestellt werden.",
                    stckSignIn );
            return;
        }

        // Get username & password
        String username = txtUsernameSignIn.getText();
        String password = Hasher.getHashedPassword( pwdSignIn.getText() );

        // Check if save password enable
        if ( cbSavePassword.isSelected() ) {
            // Save password local
            LocalDataUtil.setProperty( "username", username );
            LocalDataUtil.setProperty( "password", password );
        } else {
            // Remove password
            LocalDataUtil.removeProperty( "username" );
            LocalDataUtil.removeProperty( "password" );
        }

        // Send packet to server
        Main.getInstance().getNettyClient().sendPacket( new UserLoginPacket( username, password ) );
    }

    /**
     * Called when a player logged in as guest
     *
     * @param event for the login as guest
     */
    private void onGuestMouseEvent( MouseEvent event ) {
        try {
            // Switch to main menu
            WindowUtil.switchWindow( Attributes.getInstance().getMainMenuFXML(), grdMain );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Called when a player signed up
     *
     * @param event for the sign up
     */
    private void onSignUpActionEvent( ActionEvent event ) {
        // Check if client connected
        if ( !Main.getInstance().getNettyClient().isConnected() ) {
            NotificationUtil.show( "Server offline", "Es konnte keine Verbindung zum Server hergestellt werden.",
                    stckSignUp );
            return;
        }

        // Get username & password
        String username = txtUsernameSignUp.getText();
        String password = pwdSignUp.getText();

        // Check if password equals
        if ( !password.equals( pwdConfirmSignUp.getText() ) ) {
            // No, show notification
            NotificationUtil.show( HTTPCodes.ERROR_400.toString(), "Passwörter stimmen nicht überein. Versuchen Sie es erneut.", stckSignUp, brdrSignUp );
            pwdSignUp.getStyleClass().add( "errorTestFocus" );
            pwdConfirmSignUp.getStyleClass().add( "errorTestFocus" );
            return;
        }

        // Check if username length allowed
        if ( username.length() > MAX_USERNAME_LENGTH || username.length() < MIN_USERNAME_LENGTH ) {
            // No, show notification
            NotificationUtil.show( HTTPCodes.ERROR_400.toString(), "Der Nutzername muss zwischen " + MIN_USERNAME_LENGTH + " - " + MAX_USERNAME_LENGTH + " Zeichen lang sein. Versuchen Sie es erneut.", stckSignUp, brdrSignUp );
            txtUsernameSignUp.getStyleClass().add( "errorTestFocus" );
            return;
        }

        // Check if password length allowed
        if ( password.length() > MAX_PASSWORD_LENGTH || password.length() < MIN_PASSWORD_LENGTH ) {
            // No, show notification
            NotificationUtil.show( HTTPCodes.ERROR_400.toString(), "Das Passwort muss zwischen " + MIN_PASSWORD_LENGTH + " - " + MAX_PASSWORD_LENGTH + " Zeichen lang sein. Versuchen Sie es erneut.", stckSignUp, brdrSignUp );
            pwdSignUp.getStyleClass().add( "errorTestFocus" );
            return;
        }

        // Check if conditions accepted
        if ( !cbToS.isSelected() ) {
            // No, show notification
            NotificationUtil.show( HTTPCodes.ERROR_400.toString(), "Sie müssen die Nutzungsbedingungen akzeptieren. Versuchen Sie es erneut.", stckSignUp, brdrSignUp );
            cbToS.getStyleClass().add( "errorTestToggle" );
            return;
        }

        // Send packet to server
        Main.getInstance().getNettyClient().sendPacket( new UserRegisterPacket( username,
                Hasher.getHashedPassword( password ) ) );
    }

    /**
     * Called when a player switched to the sign up page
     *
     * @param event for the sign up
     */
    private void onGoToSignUpMouseEvent( MouseEvent event ) {
        brdrSignIn.setVisible( true );

        // Update state
        Main.getInstance().setCurrentState( State.REGISTER );

        this.gridPaneRow( 487 );
        this.stckSignUp.setVisible( true );
    }

    /**
     * Called when a player switched to the login page
     *
     * @param event for the login
     */
    private void onGoToSignInMouseEvent( MouseEvent event ) {
        brdrSignUp.setVisible( true );

        // Update state
        Main.getInstance().setCurrentState( State.LOGIN );

        this.gridPaneRow( 432 );
        this.stckSignUp.setVisible( false );
    }

    /**
     * Change height to grid pane
     *
     * @param height to set
     */
    private void gridPaneRow( int height ) {
        this.grdLogin.getRowConstraints().get( 1 ).setMinHeight( height );
        this.grdLogin.getRowConstraints().get( 1 ).setPrefHeight( height );
        this.grdLogin.getRowConstraints().get( 1 ).setMaxHeight( height );
    }

    @Override
    public void initialize( URL location, ResourceBundle resources ) {
        // Update state
        Main.getInstance().setCurrentState( State.LOGIN );

        // Set username if saved in local file
        LocalDataUtil.getProperty( "username" ).ifPresent( username ->
                txtUsernameSignIn.setText( username ) );

        // Set password if saved in local file
        LocalDataUtil.getProperty( "password" ).ifPresent( password -> {
            // Set password
            pwdSignIn.setText( password );

            // Enable checkbox for save password
            cbSavePassword.setSelected( true );
        } );

        brdrSignIn.setVisible( true );

        // Register events
        this.registerEvents();

        // Register Title Bar
        new TimelineUtil( 1L,
                e -> TitleBarUtil.show( grdMain, Main.getInstance().getStage(), true, false, true, true ) ).start();


        pnToS.setOnMousePressed( e -> NotificationUtil.show( "ToS", "Ich erkläre mich einverstanden, dass meine IP Adresse im System geloggt wird.", stckSignUp, brdrSignUp ) );
    }

    /**
     * Register all login events
     */
    private void registerEvents() {
//        this.particle.setOnAction(e -> CounterUtil.customBackground(wvBackground, particle, animatedBackground));
//        this.animatedBackground.setOnAction(e -> CounterUtil.customBackground(wvBackground, particle, animatedBackground));
        this.btnSignIn.setOnAction( this::onLoginActionEvent );
        this.btnSignUp.setOnAction( this::onSignUpActionEvent );
        this.lblToRegister.setOnMouseClicked( this::onGoToSignUpMouseEvent );
        this.lblToLogin.setOnMouseClicked( this::onGoToSignInMouseEvent );
        this.lblGast.setOnMouseClicked( this::onGuestMouseEvent );
    }

    private enum AnimationState {
        START,
        CLOSE
    }


}
