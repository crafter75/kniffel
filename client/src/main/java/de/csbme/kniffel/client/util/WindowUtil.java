package de.csbme.kniffel.client.util;

import de.csbme.kniffel.client.Attributes;
import de.csbme.kniffel.client.Main;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.net.URL;

public class WindowUtil {

    /**
     * Create window from fxml
     *
     * @param fxml of the window
     * @throws Exception
     */
    public static void createWindow(URL fxml) throws Exception {
        // Create empty stage
        Stage stage = new Stage();

        // Init FXML loader & load the UI
        FXMLLoader fxmlLoader = new FXMLLoader(fxml);
        Parent root = fxmlLoader.load();

        // Set new controller from fxml loader
        Attributes.getInstance().setCurrentController(fxmlLoader.getController());

        // Init style
        stage.initStyle(StageStyle.UNDECORATED);

        // Create new scene from parent
        Scene scene = new Scene(root);

        // Add current stylesheet
        scene.getStylesheets().addAll(Attributes.getInstance().getStylesheet(Attributes.getInstance().getCurrentTheme()));

        // Taskbar Title & Icon
        stage.setTitle(Attributes.getInstance().getNAME());
        stage.getIcons().add(new Image(Main.class.getResourceAsStream("/img/icon.png")));

        // Set scene to stage
        stage.setScene(scene);

        // Show stage
        stage.show();

        // Set new stage
        Main.getInstance().setStage(stage);
    }

    /**
     * Close a window
     *
     * @param node from the window
     */
    public static void closeWindow(Node node) {
        ((Stage) node.getScene().getWindow()).close();
    }

    /**
     * Switch window
     *
     * @param fxml of the window to load
     * @param node of the windiw to close
     * @throws Exception
     */
    public static void switchWindow(URL fxml, Node node) throws Exception {
        createWindow(fxml);
        closeWindow(node);
    }

}
