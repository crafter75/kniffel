package de.csbme.kniffel.client;

import de.csbme.kniffel.client.controller.MainMenuController;
import de.csbme.kniffel.client.user.User;
import de.csbme.kniffel.client.util.State;
import de.csbme.kniffel.client.util.Style;
import de.csbme.kniffel.client.util.Theme;
import de.csbme.kniffel.lib.game.Game;
import de.csbme.kniffel.lib.game.util.Dice;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import lombok.Getter;
import lombok.Setter;

import java.net.URL;
import java.util.Optional;

@Getter
public final class Attributes {

    @Getter
    private static Attributes instance = new Attributes();

    // Paths
    private final String RESOURCE_PATH_FXML = "/fxml/", RESOURCE_PATH_IMG = "/img/", RESOURCE_PATH_CSS = "/css/",
            RESOURCE_PATH_TXT = "/txt/", RESOURCE_PATH_HTML = "/html/", RESOURCE_PATH_SOUND = "/sound/";

    // Metadata
    private final String NAME = "Kniffel", AUTHOR = "Thorsten Engel & Jannik Buscha";

    // Stylesheet
    private final String DARK = getClass().getResource( RESOURCE_PATH_CSS + "dark.css" ).toExternalForm();

    // Theme
    private Theme currentTheme = Theme.DARK;

    /**
     * Set a new Theme
     *
     * @param theme to set
     */
    public void setCurrentTheme( Theme theme ) {
        this.currentTheme = theme;
        Main.getInstance().getStage().getScene().getStylesheets().clear();
        Main.getInstance().getStage().getScene().getStylesheets().add( this.getStylesheet( theme ) );
    }

    /**
     * Get the stylesheet from Theme
     *
     * @param theme of the stylesheet
     * @return the stylesheet
     */
    public String getStylesheet( Theme theme ) {
        switch ( theme ) {
            case DARK:
                return DARK;
            default:
                return DARK;
        }
    }

    // Site
    private final String ABSTRACT_SITE = getClass().getResource( RESOURCE_PATH_HTML + "Abstract.html" ).toString();
    private final String BOX_SITE = getClass().getResource( RESOURCE_PATH_HTML + "Box.html" ).toString();
    private final String BUBBLE_SITE = getClass().getResource( RESOURCE_PATH_HTML + "Bubble.html" ).toString();
    private final String NONE_SITE = getClass().getResource( RESOURCE_PATH_HTML + "None.html" ).toString();
    private final String PARALLAX_SITE = getClass().getResource( RESOURCE_PATH_HTML + "Parallax.html" ).toString();

    // Styles
    private Style currentStyle = Style.BOX;

    /**
     * Set a new Style
     *
     * @param style to set
     */
    public void setCurrentStyle( Style style ) {
        this.currentStyle = style;

        // Check if main menu to reload style
        if ( Main.getInstance().getCurrentState().equals( State.MAIN_MENU ) ) {
            // Get controller
            MainMenuController mainMenuController = this.getCurrentController( MainMenuController.class );

            // Reset content
            mainMenuController.getWvBackground().getEngine().loadContent( "" );

            // Set new one
            mainMenuController.getWvBackground().getEngine().load( this.getSite( this.currentStyle ) );
        }
    }

    /**
     * Get the site from Style
     *
     * @param style of the site
     * @return the site
     */
    public String getSite( Style style ) {
        switch ( style ) {
            case ABSTRACT:
                return ABSTRACT_SITE;
            case BOX:
                return BOX_SITE;
            case BUBBLE:
                return BUBBLE_SITE;
            case PARALLAX:
                return PARALLAX_SITE;
            case NONE:
                return NONE_SITE;
            default:
                return BOX_SITE;
        }
    }

    // Images
    private final String DICE_PATH = RESOURCE_PATH_IMG + "gif/%s.gif";

    /**
     * Get the image (gif) for the dice
     *
     * @param dice of the image
     * @return the image
     */
    public Image getImageForDice( Dice dice ) {
        return new Image( Attributes.class.getResourceAsStream( String.format( this.DICE_PATH, dice.getValue() ) ) );
    }

    // Text File
    private final String CREDITS = getClass().getResource( RESOURCE_PATH_TXT + "credits.txt" ).toExternalForm();

    // Icons
    private final String BACK_ICON = "M8,16.5A8.5,8.5,0,1,0,16.5,8,8.5,8.5,0,0,0,8,16.5Zm15.355,0A6.855,6.855,0,1,1,16.5,9.645,6.853,6.853,0,0,1,23.355,16.5Zm-2.468-.685v1.371a.412.412,0,0,1-.411.411H16.5v2.3a.412.412,0,0,1-.7.291L12.4,16.791a.412.412,0,0,1,0-.583L15.8,12.816a.411.411,0,0,1,.7.291v2.3h3.976A.412.412,0,0,1,20.887,15.815Z";
    private final String SAVE_ICON = "M16.467,35.717l-3.183-3.183A1.821,1.821,0,0,0,12,32H1.821A1.821,1.821,0,0,0,0,33.821V47.179A1.821,1.821,0,0,0,1.821,49H15.179A1.821,1.821,0,0,0,17,47.179V37a1.821,1.821,0,0,0-.533-1.288ZM8.5,46.571a2.429,2.429,0,1,1,2.429-2.429A2.429,2.429,0,0,1,8.5,46.571Zm3.643-11.555V38.83a.455.455,0,0,1-.455.455h-8.8a.455.455,0,0,1-.455-.455V34.884a.455.455,0,0,1,.455-.455h8.672a.455.455,0,0,1,.322.133l.132.132a.455.455,0,0,1,.133.322Z";
    private final String PLAY_ICON = "M14.091,7.159,2.4.25A1.585,1.585,0,0,0,0,1.621V15.437a1.592,1.592,0,0,0,2.4,1.371L14.091,9.9a1.592,1.592,0,0,0,0-2.742Z";

    // Sound
    private final String ROLL_SOUND = getClass().getResource( RESOURCE_PATH_SOUND + "roll.wav" ).toExternalForm();
    private final String TIP_SOUND = getClass().getResource( RESOURCE_PATH_SOUND + "tip.wav" ).toExternalForm();
    private final String BUTTON_SOUND = getClass().getResource( RESOURCE_PATH_SOUND + "button.wav" ).toExternalForm();

    // Controller
    @Setter
    private Initializable currentController;

    public <T extends Initializable> T getCurrentController( Class<T> clazz ) {
        if ( !clazz.isInstance( this.currentController ) ) {
            throw new ClassCastException( "Can't cast controller to " + clazz.getSimpleName() );
        }
        return clazz.cast( this.currentController );
    }

    // FXML
    private URL splashScreenFXML = getClass().getResource( RESOURCE_PATH_FXML + "splashscreen.fxml" );
    private URL loginFXML = getClass().getResource( RESOURCE_PATH_FXML + "login.fxml" );
    private URL gameFXML = getClass().getResource( RESOURCE_PATH_FXML + "game.fxml" );
    private URL mainMenuFXML = getClass().getResource( RESOURCE_PATH_FXML + "mainmenu.fxml" );
    private URL settingsFXML = getClass().getResource( RESOURCE_PATH_FXML + "settings.fxml" );
    private URL statisticsFXML = getClass().getResource( RESOURCE_PATH_FXML + "statistics.fxml" );
    private URL spFXML = getClass().getResource( RESOURCE_PATH_FXML + "sp.fxml" );
    private URL mpLocaleFXML = getClass().getResource( RESOURCE_PATH_FXML + "mplocale.fxml" );
    private URL mpOnlineFXML = getClass().getResource( RESOURCE_PATH_FXML + "mponline.fxml" );
    private URL aboutFXML = getClass().getResource( RESOURCE_PATH_FXML + "about.fxml" );

    // User
    @Setter
    private Optional<User> user = Optional.empty();

    // Game
    @Setter
    private Optional<Game> game = Optional.empty();

}