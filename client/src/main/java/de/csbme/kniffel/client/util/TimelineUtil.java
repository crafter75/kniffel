package de.csbme.kniffel.client.util;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;

public class TimelineUtil {

    private Timeline timeline;

    public TimelineUtil() {
        this.timeline = new Timeline();
    }

    public TimelineUtil( long millis, EventHandler<ActionEvent> event ) {
        this();
        this.addFrame( millis, event );
    }

    public TimelineUtil( long millis, KeyValue keyValue ) {
        this();
        this.addFrame( millis, keyValue );
    }

    public TimelineUtil( KeyFrame... keyFrames ) {
        this();
        this.timeline.getKeyFrames().addAll( keyFrames );
    }

    /**
     * Add a event frame
     *
     * @param millis to wait
     * @param event  to execute after the delay
     * @return TimelineUtil object
     */
    public TimelineUtil addFrame( long millis, EventHandler<ActionEvent> event ) {
        // Add key frame
        this.timeline.getKeyFrames().add( new KeyFrame( ( millis == 0L ? Duration.ZERO : Duration.millis( millis ) ), event ) );
        return this;
    }

    /**
     * Add a key value frame
     *
     * @param millis   to wait
     * @param keyValue to add
     * @return TimelineUtil object
     */
    public TimelineUtil addFrame( long millis, KeyValue keyValue ) {
        // Add key frame
        this.timeline.getKeyFrames().add( new KeyFrame( ( millis == 0L ? Duration.ZERO : Duration.millis( millis ) ), keyValue ) );
        return this;
    }

    /**
     * Start the Timeline
     */
    public void start() {
        this.timeline.playFromStart();
    }
}
