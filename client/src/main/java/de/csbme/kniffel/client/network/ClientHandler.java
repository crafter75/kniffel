package de.csbme.kniffel.client.network;

import com.jfoenix.controls.events.JFXDialogEvent;
import de.csbme.kniffel.client.Attributes;
import de.csbme.kniffel.client.Main;
import de.csbme.kniffel.client.controller.GameController;
import de.csbme.kniffel.client.controller.LoginController;
import de.csbme.kniffel.client.controller.MainMenuController;
import de.csbme.kniffel.client.controller.StatisticsController;
import de.csbme.kniffel.client.controller.gamesettings.MpOnlineController;
import de.csbme.kniffel.client.user.User;
import de.csbme.kniffel.client.util.*;
import de.csbme.kniffel.lib.error.HTTPCodes;
import de.csbme.kniffel.lib.game.category.Category;
import de.csbme.kniffel.lib.game.player.AbstractEntity;
import de.csbme.kniffel.lib.game.player.GamePlayer;
import de.csbme.kniffel.lib.game.util.Roll;
import de.csbme.kniffel.lib.network.Packet;
import de.csbme.kniffel.lib.network.packet.*;
import de.csbme.kniffel.lib.network.packet.game.*;
import de.csbme.kniffel.lib.network.packet.lobby.LobbyCreatePacket;
import de.csbme.kniffel.lib.network.packet.lobby.LobbyDeletePacket;
import de.csbme.kniffel.lib.network.packet.lobby.LobbyUpdatePacket;
import de.csbme.kniffel.lib.user.Settings;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.ReferenceCountUtil;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ClientHandler extends SimpleChannelInboundHandler<Object> {

    private NettyClient nettyClient;

    ClientHandler( NettyClient nettyClient ) {
        this.nettyClient = nettyClient;
    }

    @Override
    protected void channelRead0( ChannelHandlerContext channelHandlerContext, Object object ) throws Exception {
        try {
            if ( !( object instanceof Packet ) ) {
                return;
            }
            Channel channel = channelHandlerContext.channel();
            Packet packet = (Packet) object;

            // Packet for errors
            if ( packet instanceof ErrorPacket ) {
                ErrorPacket errorPacket = (ErrorPacket) packet;

                // Run in fx thread
                Platform.runLater( () -> {
                    // Switch state
                    switch ( Main.getInstance().getCurrentState() ) {
                        case LOGIN: {
                            // Get login controller
                            LoginController loginController = Attributes.getInstance().getCurrentController( LoginController.class );

                            // Show notification with error
                            NotificationUtil.show( errorPacket.getErrorCode().toString(), errorPacket.getMessage(),
                                    loginController.getStckSignIn(), loginController.getBrdrSignIn() );

                            // Check if specific error code
                            if ( errorPacket.getErrorCode().equals( HTTPCodes.ERROR_400 ) ) {
                                // Add style class to username and password field
                                loginController.getTxtUsernameSignIn().getStyleClass().add( "errorTestFocus" );
                                loginController.getPwdSignIn().getStyleClass().add( "errorTestFocus" );
                            }
                            return;
                        }
                        case REGISTER: {
                            // Get login controller
                            LoginController loginController = Attributes.getInstance().getCurrentController( LoginController.class );

                            // Show notification with error
                            NotificationUtil.show( errorPacket.getErrorCode().toString(), errorPacket.getMessage(),
                                    loginController.getStckSignUp(), loginController.getBrdrSignUp() );

                            if ( errorPacket.getErrorCode().equals( HTTPCodes.ERROR_409 ) ) {
                                // Add style class to username
                                loginController.getTxtUsernameSignUp().getStyleClass().add( "errorTestFocus" );
                            }
                            return;
                        }
                        case MULTIPLAYER_ONLINE: {
                            // Get mp online controller
                            MpOnlineController mpOnlineController = Attributes.getInstance().getCurrentController( MpOnlineController.class );

                            // Show notification with error
                            NotificationUtil.show( errorPacket.getErrorCode().toString(), errorPacket.getMessage(),
                                    mpOnlineController.getStckSettings(), mpOnlineController.getAnchMpOnline() );
                            return;
                        }
                        case MAIN_MENU: {
                            // Get main menu controller
                            MainMenuController mainMenuController = Attributes.getInstance().getCurrentController( MainMenuController.class );

                            // Show notification with error
                            NotificationUtil.show( errorPacket.getErrorCode().toString(), errorPacket.getMessage(),
                                    mainMenuController.getStckMenu(), mainMenuController.getAnchMenu() );
                            return;
                        }
                        case STATISTICS: {
                            // Get statistic controller
                            StatisticsController statisticsController = Attributes.getInstance().getCurrentController( StatisticsController.class );

                            // Show notification with error TODO need stack pane in statistics
                            //NotificationUtil.show( errorPacket.getErrorCode().toString(), errorPacket.getMessage(),
                            //        statisticsController.getGrdMain(), statisticsController.getGrdMain() );
                            return;
                        }
                    }
                } );
                return;
            }

            // Packet for registration complete
            if ( packet instanceof UserRegisterCompletePacket ) {
                UserRegisterCompletePacket userRegisterCompletePacket = (UserRegisterCompletePacket) packet;

                if ( !Main.getInstance().getCurrentState().equals( State.REGISTER ) ) return;
                LoginController loginController = Attributes.getInstance().getCurrentController( LoginController.class );

                // Run task in main javafx thread
                Platform.runLater( () -> {
                    // Create close event for the dialog to switch to the login gui
                    EventHandler<JFXDialogEvent> handler = event -> {
                        // Open login
                        Scene scene = Main.getInstance().getStage().getScene();
                        loginController.getLblToLogin().fireEvent( new MouseEvent( MouseEvent.MOUSE_CLICKED,
                                scene.getX(), scene.getY(), scene.getX(), scene.getY(), MouseButton.PRIMARY, 1,
                                true, true, true, true, true,
                                true, true, true, true, true,
                                null ) );
                    };

                    // Open notification with close event handler
                    NotificationUtil.show( "Erfolgreich registriert", "Du wurdest erfolgreich registriert! \nUser-ID: "
                                    + userRegisterCompletePacket.getId() + "\nUsename: " + userRegisterCompletePacket.getUsername(),
                            loginController.getStckSignUp(), loginController.getBrdrSignUp(), handler );
                } );
                return;
            }

            // Packet for login successful
            if ( packet instanceof UserLoginCallbackPacket ) {
                UserLoginCallbackPacket userLoginCallbackPacket = (UserLoginCallbackPacket) packet;

                // Set user
                User user = new User( userLoginCallbackPacket.getId(), userLoginCallbackPacket.getUsername(),
                        userLoginCallbackPacket.getRegistrationDate(), userLoginCallbackPacket.getSettings() );
                Attributes.getInstance().setUser( Optional.of( user ) );

                // Run task in main javafx thread
                Platform.runLater( () -> {
                    // Set current theme
                    Attributes.getInstance().setCurrentTheme(
                            Theme.values()[user.getSettings().get( Settings.Key.THEME_STYLE, Integer.class )] );

                    // Open menu
                    LoginController loginController = Attributes.getInstance().getCurrentController( LoginController.class );
                    try {
                        WindowUtil.switchWindow( Attributes.getInstance().getMainMenuFXML(), loginController.getGrdMain() );
                    } catch ( Exception e1 ) {
                        NotificationUtil.show( "Menu error", e1.getMessage(),
                                loginController.getStckSignIn(), loginController.getBrdrSignIn() );
                    }
                } );
                return;
            }

            // Packet for stats response
            if ( packet instanceof UserStatsResponsePacket ) {
                UserStatsResponsePacket userStatsResponsePacket = (UserStatsResponsePacket) packet;

                // Check if statistics window
                if ( !Main.getInstance().getCurrentState().equals( State.STATISTICS ) ) return;

                // Get statistics controller and show stats in fx thread
                Platform.runLater( () -> Attributes.getInstance().getCurrentController( StatisticsController.class ).showStats(
                        userStatsResponsePacket.getUsername(), userStatsResponsePacket.getStats(), userStatsResponsePacket.getRanking() ) );
                return;
            }

            // Packet for ranking response
            if ( packet instanceof UserRankingResponsePacket ) {
                UserRankingResponsePacket userRankingResponsePacket = (UserRankingResponsePacket) packet;

                // Check if statistics window
                if ( !Main.getInstance().getCurrentState().equals( State.STATISTICS ) ) return;

                // Get statistics controller and show ranking in fx thread
                Platform.runLater( () -> Attributes.getInstance().getCurrentController( StatisticsController.class ).showRanking(
                        userRankingResponsePacket.getRankingEntries() ) );
                return;
            }

            // Packet for game start
            if ( packet instanceof GameStartPacket ) {
                GameStartPacket gameStartPacket = (GameStartPacket) packet;

                if ( !Main.getInstance().getCurrentState().equals( State.MULTIPLAYER_ONLINE ) ) return;

                // Set game
                Attributes.getInstance().setGame( Optional.ofNullable( gameStartPacket.getGame() ) );

                // Run task in main javafx thread
                Platform.runLater( () -> {
                    // Get controller
                    MpOnlineController mpOnlineController = Attributes.getInstance().getCurrentController( MpOnlineController.class );

                    // Close dialog
                    if ( mpOnlineController.getCurrentLobbyDialog() != null ) {
                        mpOnlineController.setDeleteOnExit( false );
                        mpOnlineController.getCurrentLobbyDialog().close();
                        mpOnlineController.setCurrentLobbyDialog( null );
                    }

                    try {
                        // Switch window
                        WindowUtil.switchWindow( Attributes.getInstance().getGameFXML(), mpOnlineController.getGrdMain() );
                    } catch ( Exception e1 ) {
                        // Show error
                        NotificationUtil.show( "Game Error", e1.getMessage(),
                                mpOnlineController.getStckSettings(), mpOnlineController.getAnchMpOnline() );
                    }
                } );
                return;
            }

            // Packet for game roll end
            if ( packet instanceof GameDiceRollEndPacket ) {
                GameDiceRollEndPacket gameDiceRollEndPacket = (GameDiceRollEndPacket) packet;

                // Get player from game and set points
                Attributes.getInstance().getGame().ifPresent( game -> game.getEntities().stream().filter( e ->
                        e.getName().equalsIgnoreCase( gameDiceRollEndPacket.getName() ) ).findFirst().ifPresent( entity -> {
                    // Set new points to category
                    Category category = gameDiceRollEndPacket.getCategory();
                    int points = gameDiceRollEndPacket.getPoints();
                    if ( !entity.hasCategory( category ) ) {
                        entity.setPoints( category, points );
                    }

                    // Refresh stats table in fx thread
                    Platform.runLater( () -> game.getGameHandlers().forEach( handler -> handler.handleDiceRollEnd( entity, category, points ) ) );
                } ) );
                return;
            }

            // Packet for game next player
            if ( packet instanceof GameNextPlayerPacket ) {
                GameNextPlayerPacket gameNextPlayerPacket = (GameNextPlayerPacket) packet;

                // Get game
                Attributes.getInstance().getGame().ifPresent( game -> {
                    // Get player from name
                    Optional<AbstractEntity> optionalEntity = game.getEntities().stream().filter( e -> e.getName().equalsIgnoreCase( gameNextPlayerPacket.getName() ) ).findFirst();

                    // Check if exists
                    if ( optionalEntity.isPresent() ) {
                        // Set current index
                        game.setCurrentEntityIndex( game.getEntities().indexOf( optionalEntity.get() ) );

                        // Call handler for next entity in fx thread
                        Platform.runLater( () -> game.getGameHandlers().forEach( handler -> handler.handleNextEntity( optionalEntity.get() ) ) );
                    }
                } );
                return;
            }

            // Packet for game roll
            if ( packet instanceof GameDiceRollPacket ) {
                GameDiceRollPacket gameDiceRollPacket = (GameDiceRollPacket) packet;

                // Get game
                Attributes.getInstance().getGame().ifPresent( game -> {
                    // Create roll
                    Roll roll = new Roll( game, game.getCurrentEntity() );
                    roll.setDice( gameDiceRollPacket.getDice() );
                    roll.setSavedIndex( gameDiceRollPacket.getSavedIndex() );
                    roll.setRollsLeft( gameDiceRollPacket.getRollsLeft() );

                    // Set roll
                    game.setCurrentRoll( roll );

                    // Call handler for roll in fx thread
                    Platform.runLater( () -> game.getGameHandlers().forEach( handler -> handler.handleDiceRoll( game.getCurrentEntity(), roll ) ) );
                } );
            }

            // Packet for game dice save
            if ( packet instanceof GameDiceSavePacket ) {
                GameDiceSavePacket gameDiceSavePacket = (GameDiceSavePacket) packet;

                // Get game
                Attributes.getInstance().getGame().ifPresent( game -> {
                    // Get roll and save index in fx thread
                    Platform.runLater( () -> game.getCurrentRoll().saveDice( gameDiceSavePacket.getDiceIndex() ) );
                } );
            }

            // Packet for game dice remove
            if ( packet instanceof GameDiceRemovePacket ) {
                GameDiceRemovePacket gameDiceRemovePacket = (GameDiceRemovePacket) packet;

                // Get game
                Attributes.getInstance().getGame().ifPresent( game -> {
                    // Get roll and save index in fx thread
                    Platform.runLater( () -> game.getCurrentRoll().removeDice( gameDiceRemovePacket.getDiceIndex() ) );
                } );
            }

            // Packet for game quit
            if ( packet instanceof GamePlayerQuitPacket ) {
                GamePlayerQuitPacket gamePlayerQuitPacket = (GamePlayerQuitPacket) packet;

                // Get user
                Optional<User> optionalUser = Attributes.getInstance().getUser();

                // Check if exists
                if ( !optionalUser.isPresent() ) {
                    return;
                }

                // Check if the kicked user on this client
                if ( optionalUser.get().getUsername().equalsIgnoreCase( gamePlayerQuitPacket.getName() ) ) {
                    Platform.runLater( () -> {
                        switch ( Main.getInstance().getCurrentState() ) {
                            case GAME:
                                // Get game controller
                                GameController gameController = Attributes.getInstance().getCurrentController( GameController.class );

                                try {
                                    // Switch window
                                    WindowUtil.switchWindow( Attributes.getInstance().getMainMenuFXML(), gameController.getGrdMain() );

                                    // Run delay
                                    new TimelineUtil( 500L, e -> {
                                        // Get main menu controller
                                        MainMenuController mainMenuController = Attributes.getInstance().getCurrentController( MainMenuController.class );

                                        // Show notification
                                        NotificationUtil.show( gamePlayerQuitPacket.isAfk() ? "AFK" : "Verlassen", gamePlayerQuitPacket.isAfk() ? "Du wurdest aufgrund von Inaktivität gekickt!" : "Du hast das Spiel verlassen!",
                                                mainMenuController.getStckMenu(), mainMenuController.getAnchMenu() );
                                    } ).start();
                                } catch ( Exception e ) {
                                    e.printStackTrace();
                                }
                                break;
                            case MAIN_MENU:
                                // Get main menu controller
                                MainMenuController mainMenuController = Attributes.getInstance().getCurrentController( MainMenuController.class );

                                // Show notification
                                NotificationUtil.show( gamePlayerQuitPacket.isAfk() ? "AFK" : "Verlassen", gamePlayerQuitPacket.isAfk() ? "Du wurdest aufgrund von Inaktivität gekickt!" : "Du hast das Spiel verlassen!",
                                        mainMenuController.getStckMenu(), mainMenuController.getAnchMenu() );
                                break;
                            default:
                                break;
                        }
                    } );
                    return;
                }

                // Get game
                Attributes.getInstance().getGame().ifPresent( game -> game.getEntities().stream().filter( e ->
                        e.getName().equalsIgnoreCase( gamePlayerQuitPacket.getName() ) ).findFirst().ifPresent( entity -> {
                    // Remove player
                    game.getEntities().remove( entity );

                    // Call handler in fx thread
                    Platform.runLater( () -> game.getGameHandlers().forEach( handler -> handler.handlePlayerQuit( (GamePlayer) entity, gamePlayerQuitPacket.isAfk() ) ) );
                } ) );
                return;
            }

            // Packet for game end
            if ( packet instanceof GameEndPacket ) {
                GameEndPacket gameEndPacket = (GameEndPacket) packet;

                // Get game
                Attributes.getInstance().getGame().ifPresent( game -> {
                    List<AbstractEntity> entitiesSorted = gameEndPacket.getPlayers().stream().filter( s -> game.getEntities().stream().anyMatch( e ->
                            e.getName().equalsIgnoreCase( s ) ) ).map( s -> game.getEntities().stream().filter( e -> e.getName().equalsIgnoreCase( s ) ).findFirst().get() )
                            .collect( Collectors.toList() );

                    // Call handler in fx thread
                    Platform.runLater( () -> game.getGameHandlers().forEach( handler -> handler.handleEnd( entitiesSorted, gameEndPacket.getTimeOfGame(), true ) ) );
                } );
                return;
            }

            // Packet for lobby creation
            if ( packet instanceof LobbyCreatePacket ) {
                LobbyCreatePacket lobbyCreatePacket = (LobbyCreatePacket) packet;

                // Check if current state mp online
                if ( !Main.getInstance().getCurrentState().equals( State.MULTIPLAYER_ONLINE ) ) return;

                // Get mp online controller
                MpOnlineController mpOnlineController = Attributes.getInstance().getCurrentController( MpOnlineController.class );

                // Show lobby in fx  thread
                Platform.runLater( () -> mpOnlineController.showLobby( lobbyCreatePacket.getLobbyName(), lobbyCreatePacket.getUserName(), null ) );
                return;
            }

            // Packet for lobby update
            if ( packet instanceof LobbyUpdatePacket ) {
                LobbyUpdatePacket lobbyUpdatePacket = (LobbyUpdatePacket) packet;

                // Check if current state mp online
                if ( !Main.getInstance().getCurrentState().equals( State.MULTIPLAYER_ONLINE ) ) return;

                // Get mp online controller
                MpOnlineController mpOnlineController = Attributes.getInstance().getCurrentController( MpOnlineController.class );

                // Show lobby in fx  thread
                Platform.runLater( () -> mpOnlineController.showLobby( lobbyUpdatePacket.getLobbyName(), lobbyUpdatePacket.getLeader(), lobbyUpdatePacket.getPlayers() ) );
                return;
            }

            // Packet for lobby update
            if ( packet instanceof LobbyDeletePacket ) {
                // Check if current state mp online
                if ( !Main.getInstance().getCurrentState().equals( State.MULTIPLAYER_ONLINE ) ) return;

                // Get mp online controller
                MpOnlineController mpOnlineController = Attributes.getInstance().getCurrentController( MpOnlineController.class );

                // Close dialog
                if ( mpOnlineController.getCurrentLobbyDialog() != null ) {
                    mpOnlineController.setDeleteOnExit( false );
                    mpOnlineController.getCurrentLobbyDialog().close();
                    mpOnlineController.setCurrentLobbyDialog( null );
                }
                return;
            }
        } catch ( Exception e ) {
            e.printStackTrace();
        } finally {
            ReferenceCountUtil.release( object );
        }
    }

    @Override
    public void channelActive( ChannelHandlerContext ctx ) throws Exception {
        // Set channel
        this.nettyClient.setChannel( ctx.channel() );
    }

    @Override
    public void channelInactive( ChannelHandlerContext ctx ) throws Exception {
        // Remove channel
        this.nettyClient.setChannel( null );
    }

    @Override
    public void exceptionCaught( ChannelHandlerContext ctx, Throwable cause ) throws Exception {
        // Test only
        //System.out.println( cause.toString() );
    }
}
