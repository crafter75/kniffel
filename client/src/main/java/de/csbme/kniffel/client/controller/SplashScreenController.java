package de.csbme.kniffel.client.controller;

import com.jfoenix.controls.JFXProgressBar;
import de.csbme.kniffel.client.Attributes;
import de.csbme.kniffel.client.Main;
import de.csbme.kniffel.client.util.*;
import eu.hansolo.medusa.Gauge;
import eu.hansolo.medusa.GaugeBuilder;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ResourceBundle;

public class SplashScreenController implements Initializable {

    @FXML
    GridPane grdMain;

    @FXML
    AnchorPane anchScreen;

    @FXML
    JFXProgressBar pbLoading;

    @FXML
    Label lblPercent;

    Gauge gaugeCircle;

    // Delay in seconds
    private final int DELAY = 2;

    private void style() {
        GaugeBuilder builder = GaugeBuilder.create().skinType(Gauge.SkinType.SLIM);

        gaugeCircle = builder.decimals(0).value(0).maxValue(100).build();
        gaugeCircle.setPrefWidth(210);
        gaugeCircle.setPrefHeight(210);

        gaugeCircle.setTranslateY(65);
        gaugeCircle.setTranslateX(77);

        gaugeCircle.setValueColor(Color.TRANSPARENT);
        gaugeCircle.setBarColor(Color.rgb(0, 0, 0, 0.25));
        gaugeCircle.setBarBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Create TimelineUtil object
        TimelineUtil timeline = new TimelineUtil();

        // Add frame to show title bar
        timeline.addFrame(1L, e -> TitleBarUtil.show(grdMain, Main.getInstance().getStage(), false, false, true, false));

        // Add frame to switch window
        timeline.addFrame((this.DELAY * 1000L) + 1000L, e -> {
            try {
                WindowUtil.switchWindow( Attributes.getInstance().getLoginFXML(), grdMain );
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        });

        // Start TimelineUtil
        timeline.start();

        // Add style to gauge builder
        style();

        // Set bar and gauge counter
        CounterUtil.barCounter(pbLoading, this.DELAY * 1000, lblPercent);
        CounterUtil.gaugeCounter(gaugeCircle, this.DELAY * 10);

        // Add children
        anchScreen.getChildren().addAll(gaugeCircle);
    }

}
