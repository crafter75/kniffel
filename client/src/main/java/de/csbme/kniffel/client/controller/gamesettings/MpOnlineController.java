package de.csbme.kniffel.client.controller.gamesettings;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.JFXTextField;
import de.csbme.kniffel.client.Attributes;
import de.csbme.kniffel.client.Main;
import de.csbme.kniffel.client.user.User;
import de.csbme.kniffel.client.util.FooterUtil;
import de.csbme.kniffel.client.util.TimelineUtil;
import de.csbme.kniffel.client.util.TitleBarUtil;
import de.csbme.kniffel.client.util.WebUtil;
import de.csbme.kniffel.lib.game.util.GameSettings;
import de.csbme.kniffel.lib.network.packet.lobby.*;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.effect.BoxBlur;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebView;
import lombok.Getter;
import lombok.Setter;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

@Getter
public class MpOnlineController implements Initializable {

    @FXML
    GridPane grdMain, grdSettings;

    @FXML
    WebView wvBackground;


    @FXML
    StackPane stckSettings;

    @FXML
    Label lblTitle;

    @FXML
    JFXButton btnCreateLobby, btnJoinLobby;

    @FXML
    FontAwesomeIconView icoTitle;

    @FXML
    AnchorPane anchMpOnline;

    @FXML
    JFXTextField txtLobbyCreateName, txtLobbyJoinName;

    @Setter
    private JFXDialog currentLobbyDialog;

    @Setter
    private boolean deleteOnExit = true;

    private Label headerLabel, playerLabel;

    private JFXButton startButton = null;

    @Override
    public void initialize( URL location, ResourceBundle resources ) {
        WebUtil.customBackground( wvBackground );

        Optional<User> optionalUser = Attributes.getInstance().getUser();

        if ( !optionalUser.isPresent() ) return;

        User user = optionalUser.get();

        btnCreateLobby.setOnMousePressed( e -> Main.getInstance().getNettyClient().sendPacket(
                new LobbyCreatePacket( txtLobbyCreateName.getText(), user.getUsername() ) ) );
        //this.showLobby( stckSettings, grdSettings, txtLobbyCreateName.getText() ); );

        btnJoinLobby.setOnMousePressed( e -> Main.getInstance().getNettyClient().sendPacket(
                new LobbyJoinPacket( txtLobbyJoinName.getText(), user.getUsername() ) ) );


        new TimelineUtil( 1L,
                e -> TitleBarUtil.show( grdMain, Main.getInstance().getStage(), true, true, true, true ) ).start();

        // Show footer
        FooterUtil.show( grdSettings, Attributes.getInstance().getMainMenuFXML(), Attributes.getInstance().getMainMenuFXML(), "Zurück", Attributes.getInstance().getBACK_ICON(), "Spielen", Attributes.getInstance().getPLAY_ICON() );

    }

    /**
     * Show lobby notification with lobby name, leader and players
     *
     * @param lobbyName of the lobby
     * @param leader of the lobby
     * @param players of the lobby
     */
    public void showLobby( String lobbyName, String leader, List<String> players ) {
        // Check if user logged in
        if ( !Attributes.getInstance().getUser().isPresent() ) return;

        // Get user
        User user = Attributes.getInstance().getUser().get();

        // Get lobby size
        int size = 1 + ( players != null ? players.size() : 0 );

        // Update label on old dialog
        if ( this.currentLobbyDialog != null ) {
            // Update labels
            this.headerLabel.setText( this.getLobbyNameWithSize( lobbyName, size ) );
            this.playerLabel.setText( this.getLeadeAndPlayers( leader, players ) );

            // Update button if player is leader
            if(this.startButton != null) {
                this.startButton.setDisable( size < GameSettings.MIN_PLAYERS );
            }
            return;
        }

        // Enable delete on exit
        this.deleteOnExit = true;

        // Create dialog layout
        JFXDialogLayout dialogLayout = new JFXDialogLayout();

        // Check if leader
        if ( user.getUsername().equalsIgnoreCase( leader ) ) {
            this.startButton = new JFXButton( "Starten" );
            JFXButton closeButton = new JFXButton( "Lobby schließen" );

            // Disable button if player size lower then min players
            this.startButton.setDisable( size < GameSettings.MIN_PLAYERS );

            // Event for start button
            this.startButton.setOnAction( e -> {
                // Send start packet to server
                Main.getInstance().getNettyClient().sendPacket( new LobbyStartPacket( lobbyName ) );

                // Close dialog
                this.currentLobbyDialog.close();

                // Reset dialog
                this.currentLobbyDialog = null;

                // Reset button
                this.startButton = null;
            } );

            // Event for close button
            closeButton.setOnAction( e -> {
                // Close dialog
                this.currentLobbyDialog.close();

                // Reset dialog
                this.currentLobbyDialog = null;

                // Reset button
                this.startButton = null;
            } );

            // Add to dialog layout
            dialogLayout.setActions( this.startButton, closeButton );
        } else {
            JFXButton leaveButton = new JFXButton( "Lobby verlassen" );

            // Event for leave button
            leaveButton.setOnAction( e -> {
                // Close dialog
                this.currentLobbyDialog.close();

                // Reset dialog
                this.currentLobbyDialog = null;
            } );

            // Add to dialog layout
            dialogLayout.setActions( leaveButton );
        }

        // Create dialog
        this.currentLobbyDialog = new JFXDialog( stckSettings, dialogLayout, JFXDialog.DialogTransition.TOP );

        // Set lobby name with size
        this.headerLabel = new Label( this.getLobbyNameWithSize( lobbyName, size ) );
        dialogLayout.setHeading( this.headerLabel );

        // Set body from dialog layout
        this.playerLabel = new Label( this.getLeadeAndPlayers( leader, players ) );
        dialogLayout.setBody( this.playerLabel );

        // Add event for dialog
        this.currentLobbyDialog.setOnDialogClosed( event -> {
            // Reset effect
            grdSettings.setEffect( null );

            // Reset dialog
            this.currentLobbyDialog = null;

            // Reset button
            this.startButton = null;

            // Check if delete on exit enabled
            if ( !this.deleteOnExit ) return;

            // Check if leader
            if ( user.getUsername().equalsIgnoreCase( leader ) ) {
                // Delete lobby
                Main.getInstance().getNettyClient().sendPacket( new LobbyDeletePacket( lobbyName ) );
            } else {
                // Leave lobby
                Main.getInstance().getNettyClient().sendPacket( new LobbyQuitPacket( lobbyName, user.getUsername() ) );
            }
        } );

        // Set effect
        grdSettings.setEffect( new BoxBlur( 5, 5, 5 ) );

        // Show dialog
        this.currentLobbyDialog.show();
    }

    /**
     * Get the header text for the label with lobby name and player size
     *
     * @param lobbyName of the lobby
     * @param size      of the players in the lobby
     * @return the text
     */
    private String getLobbyNameWithSize( String lobbyName, int size ) {
        return lobbyName + " (" + size + "/" + GameSettings.MAX_PLAYERS + ")";
    }

    /**
     * Get the content text for the label with leader and players
     *
     * @param leader  of the lobby
     * @param players of the lobby
     * @return the text
     */
    private String getLeadeAndPlayers( String leader, List<String> players ) {
        // Build string with lobby leader
        StringBuilder builder = new StringBuilder( "Lobby-Leiter: " + leader + "\n\n" );

        // Check if player exists and add them to string builder
        if ( players != null && players.size() > 0 ) {
            builder.append( "Spieler:\n" );

            players.forEach( player -> builder.append( "- " ).append( player ).append( "\n" ) );
        } else {
            builder.append( "Keine Spieler gefunden." );
        }
        return builder.toString();
    }
}
