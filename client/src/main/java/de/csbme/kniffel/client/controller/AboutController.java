package de.csbme.kniffel.client.controller;

import com.jfoenix.controls.JFXTextArea;
import de.csbme.kniffel.client.Attributes;
import de.csbme.kniffel.client.Main;
import de.csbme.kniffel.client.util.FooterUtil;
import de.csbme.kniffel.client.util.TimelineUtil;
import de.csbme.kniffel.client.util.TitleBarUtil;
import de.csbme.kniffel.client.util.WebUtil;
import javafx.beans.value.ChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.web.WebView;

import java.net.URL;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.Scanner;

public class AboutController implements Initializable {

    @FXML
    GridPane grdAbout;

    @FXML
    WebView wvBackground;

    @FXML
    Label lblAuthor;

    @FXML
    JFXTextArea taCredits;

    @Override
    public void initialize( URL location, ResourceBundle resources ) {
        WebUtil.customBackground( wvBackground );

        lblAuthor.setText( "Entwickelt von " + Attributes.getInstance().getAUTHOR() );

        taCredits.textProperty().addListener( (ChangeListener<Object>) ( observable, oldValue, newValue ) -> taCredits.setScrollTop( Double.MAX_VALUE ) );

        readFile();

        new TimelineUtil( 1L, e ->
                TitleBarUtil.show( grdAbout, Main.getInstance().getStage(), true, true, true, true ) )
                .start();

        // Show footer
        FooterUtil.show( grdAbout, Attributes.getInstance().getMainMenuFXML(), "Zurück", Attributes.getInstance().getBACK_ICON() );
    }

    /**
     * Read file
     */
    private void readFile() {
        try {
            Scanner scanner = new Scanner( Objects.requireNonNull( this.getClass().getResourceAsStream(
                    Attributes.getInstance().getRESOURCE_PATH_TXT() + "credits.txt" ) ) );

            StringBuilder builder = new StringBuilder();
            while ( scanner.hasNext() ) {
                builder.append( scanner.useDelimiter("\\A").next() ).append( System.lineSeparator() );
            }
            scanner.close();
            taCredits.appendText( builder.toString() );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

}
