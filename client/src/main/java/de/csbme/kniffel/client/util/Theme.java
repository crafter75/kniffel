package de.csbme.kniffel.client.util;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

public enum Theme {

    // Themes
    DARK( "Dark" ),
/*    LIGHT( "Light" ),
    TWITCH( "Twitch" ),
    SOLARIZED( "Solarized" ),
    MATERIAL( "Material" ),
    MR_VALUE( "Mr. Value" )*/;

    @Getter
    private String name;

    Theme( String name ) {
        this.name = name;
    }

    /**
     * Get a style by the name
     *
     * @param name of the style
     * @return the style
     */
    public static Optional<Theme> fromName( String name ) {
        return Arrays.stream( Theme.values() ).filter( theme -> theme.getName().equalsIgnoreCase( name ) ).findFirst();
    }
}
