package de.csbme.kniffel.client.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import com.sun.javafx.scene.control.skin.TableHeaderRow;
import de.csbme.kniffel.client.Attributes;
import de.csbme.kniffel.client.Main;
import de.csbme.kniffel.client.game.ClientGameHandler;
import de.csbme.kniffel.client.user.User;
import de.csbme.kniffel.client.util.*;
import de.csbme.kniffel.lib.game.Game;
import de.csbme.kniffel.lib.game.category.Category;
import de.csbme.kniffel.lib.game.player.AbstractEntity;
import de.csbme.kniffel.lib.game.util.Roll;
import de.csbme.kniffel.lib.game.util.TableCategory;
import de.csbme.kniffel.lib.network.Packet;
import de.csbme.kniffel.lib.network.packet.game.*;
import javafx.animation.KeyFrame;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.SVGPath;
import javafx.util.Duration;
import lombok.Getter;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

@Getter
public class GameController implements Initializable {

    @FXML
    GridPane grdMain, grdGame;

    @FXML
    StackPane stckGame;

    @FXML
    TableView<TableCategory> tvCard;

    @FXML
    ColumnConstraints colCard;

    @FXML
    Rectangle lnOne, lnTwo, lnThree, lnFour, lnHead;

    @FXML
    Label lblTurn;

    @FXML
    ImageView ivTopLeft, ivTopMiddle, ivTopRight, ivBottomLeft, ivBottomRight;

    @FXML
    CheckBox cbTopLeft, cbTopMiddle, cbTopRight, cbBottomLeft, cbBottomRight;

    @FXML
    JFXButton btnRoll;

    @FXML
    JFXDrawer drawer;

    @FXML
    JFXHamburger hamburger;

    @FXML
    Circle rollOne, rollTwo, rollThree;

    // Tip Stuff
    @FXML
    Pane pnTips, pnPlayerInfo;

    @FXML
    Label lblEinser, lblZweier, lblDreier, lblVierer, lblFuenfer, lblSechser, lblDreierpasch, lblViererpasch, lblFullHouse, lblKleineStr, lblGrosseStr, lblKniffel, lblChance;

    @FXML
    SVGPath tipEinser, tipZweier, tipDreier, tipVierer, tipFuenfer, tipSechser, tipDreierpasch, tipViererpasch, tipFullHouse, tipKleineStr, tipGrosseStr, tipKniffel, tipChance;

    @Getter
    private List<CheckBox> selectedDices;

    @Getter
    private List<ImageView> diceImages;

    @Getter
    private List<Label> tipLabels;

    @Getter
    private List<SVGPath> tips;

    private long lastClick = System.currentTimeMillis();

    @Override
    public void initialize( URL location, ResourceBundle resources ) {
        this.selectedDices = new ArrayList<>();
        this.diceImages = new ArrayList<>();
        this.tipLabels = new ArrayList<>();
        this.tips = new ArrayList<>();

        // Get all FXML fields
        Arrays.stream( this.getClass().getDeclaredFields() ).filter( f -> f.isAnnotationPresent( FXML.class )
                && !f.getName().equals( "lblTurn" ) ).forEach( f -> {
            Class<?> clazz = f.getType();

            // Check if specific class and add it to the list
            try {
                if ( clazz.isAssignableFrom( ImageView.class ) ) {
                    this.diceImages.add( (ImageView) f.get( this ) );
                } else if ( clazz.isAssignableFrom( CheckBox.class ) ) {
                    this.selectedDices.add( (CheckBox) f.get( this ) );
                } else if ( clazz.isAssignableFrom( Label.class ) ) {
                    this.tipLabels.add( (Label) f.get( this ) );
                } else if ( clazz.isAssignableFrom( SVGPath.class ) ) {
                    this.tips.add( (SVGPath) f.get( this ) );
                }
            } catch ( IllegalAccessException e ) {
                e.printStackTrace();
            }
        } );

        // Clear images
        this.diceImages.forEach( imageView -> imageView.setImage( null ) );

        // Get game
        Optional<Game> optionalGame = Attributes.getInstance().getGame();

        // Check if not present
        if ( !optionalGame.isPresent() ) {
            // Run after 1 second
            new TimelineUtil( 500L, e -> {
                try {
                    // Switch to main menu
                    WindowUtil.switchWindow( Attributes.getInstance().getMainMenuFXML(), grdGame );

                    // Show notification with possible errors
                    MainMenuController mainMenuController = Attributes.getInstance().getCurrentController( MainMenuController.class );
                    NotificationUtil.show( "Fehler", "Das Spiel konnte nicht gestartet werden!\n" +
                                    "\n" +
                                    "Mögliche Fehler:\n" +
                                    "- Spieler haben gleiche Namen\n" +
                                    "- Fehlerhafte Auswahl der Spieler",
                            mainMenuController.getStckMenu() );
                } catch ( Exception e2 ) {
                    e2.printStackTrace();
                }
            } ).start();
            return;
        }

        // Get Game
        Game game = optionalGame.get();

        // Set handler
        game.withHandler( new ClientGameHandler( game, this ) );

        // Add click event for player info
        pnPlayerInfo.setOnMousePressed( e -> NotificationUtil.show( "Spieler", this.getPlayerAsString( game ), stckGame, grdGame ) );

        // Set lines for dice
        this.setDiceBorderLines( ivTopLeft, false );
        this.setDiceBorderLines( ivTopMiddle, false );
        this.setDiceBorderLines( ivTopRight, false );
        this.setDiceBorderLines( ivBottomLeft, false );
        this.setDiceBorderLines( ivBottomRight, false );

        // Register events
        this.registerEvents( game );

        // Create key frame for title
        KeyFrame title = new KeyFrame( Duration.millis( 1L ), e ->
                TitleBarUtil.show( grdMain, Main.getInstance().getStage(), true, true, true, true ) );

        // Create key frame to fix and hide tips
        KeyFrame fixTips = new KeyFrame( Duration.millis( 50L ), e -> {
            // Fix position of tips and labels
            int move = 93;

            // Move label to next player and disable visibility
            this.tipLabels.forEach( label -> {
                label.setTranslateX( move );
                label.setVisible( false );
            } );

            // Move svg path to next player and disable visibility
            this.tips.forEach( svgPath -> {
                svgPath.setTranslateX( move );
                svgPath.setVisible( false );
            } );
        } );

        // Create key frame to start game
        KeyFrame startGame = new KeyFrame( Duration.millis( 75L ), e -> {
            // Check if online game
            if ( game.getType().equals( Game.Type.MULTIPLAYER_ONLINE ) ) {
                // Call handler manual
                game.getGameHandlers().forEach( handler -> handler.handleStart( game.getEntities() ) );
                return;
            }

            // Start game
            game.start();
        } );

        // Create timeline util and start
        new TimelineUtil( title, fixTips, startGame ).start();

        // EventHandler for leave
        EventHandler<MouseEvent> handler = event -> {
            // Check if multiplayer online game
            if ( game.getType().equals( Game.Type.MULTIPLAYER_ONLINE ) ) {
                // Send to server
                Attributes.getInstance().getUser().ifPresent( user -> Main.getInstance().getNettyClient().sendPacket(
                        new GamePlayerQuitPacket( user.getUsername(), false ) ) );

                // Reset game
                Attributes.getInstance().setGame( Optional.empty() );
                return;
            }
            // Stop game
            game.stop( false );

            // Reset game
            Attributes.getInstance().setGame( Optional.empty() );
        };

        // Add event for sidebar
        grdMain.addEventHandler( KeyEvent.KEY_RELEASED, ( KeyEvent event ) -> {
            if ( KeyCode.ESCAPE == event.getCode() ) {
                drawer.setPrefWidth( 0 );
                drawer.setMaxWidth( 0 );
                drawer.setMinWidth( 0 );
                drawer.setVisible( false );
                drawer.close();
            }
        } );

        // Show footer
        FooterUtil.show( grdGame, Attributes.getInstance().getMainMenuFXML(), "Zurück", Attributes.getInstance().getBACK_ICON(), handler );
    }

    /**
     * Load sidebar with rules
     */
    public void loadSidebar() {
        try {
            // Load rom fxml
            FXMLLoader loader = new FXMLLoader( getClass().getResource( "/fxml/sidebar.fxml" ) );
            VBox box = loader.load();
            drawer.setSidePane( box );

            // Create object
            HamburgerBackArrowBasicTransition transition = new HamburgerBackArrowBasicTransition( hamburger );

            // Set rate
            transition.setRate( -1 );

            // Add mouse event
            hamburger.setOnMousePressed( e -> {
                transition.setRate( transition.getRate() * -1 );
                transition.play();

                // Check if open
                if ( drawer.isOpened() ) {
                    // Set width
                    drawer.setPrefWidth( 0 );
                    drawer.setMaxWidth( 0 );
                    drawer.setMinWidth( 0 );

                    // Hide drawer
                    drawer.setVisible( false );

                } else if ( drawer.isClosed() ) {
                    // Set width
                    drawer.setPrefWidth( Control.USE_COMPUTED_SIZE );
                    drawer.setMaxWidth( Control.USE_COMPUTED_SIZE );
                    drawer.setMinWidth( Control.USE_COMPUTED_SIZE );

                    // Show drawer
                    drawer.setVisible( true );

                }

                // Check if sidebar open
                if ( drawer.isOpened() ) {
                    // Close sidebar
                    drawer.close();
                } else {
                    // Open sidebar
                    drawer.open();
                }
            } );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Set the current player name of the roll
     *
     * @param name to set
     */
    public void setCurrentPlayerName( String name ) {
        // Set name
        lblTurn.setText( name + " " + ( name.equals( "Du" ) ? "b" : "" ) + "ist am Zug!" );
    }

    /**
     * Set a important information
     *
     * @param info to set
     */
    public void setInformation( String info ) {
        // Add information
        lblTurn.setText( lblTurn.getText() + "\n" + info );
    }

    /**
     * Set rolls left information
     *
     * @param rolls left
     */
    public void setRollsLeft( int rolls ) {
        // Remove style from all circles
        rollOne.getStyleClass().remove( "rollFill" );
        rollTwo.getStyleClass().remove( "rollFill" );
        rollThree.getStyleClass().remove( "rollFill" );

        // Check current roll and add style to fill circle
        if ( rolls == 3 ) {
            this.addStyle( rollOne, "rollFill" );
            this.addStyle( rollTwo, "rollFill" );
            this.addStyle( rollThree, "rollFill" );
        } else if ( rolls == 2 ) {
            this.addStyle( rollOne, "rollFill" );
            this.addStyle( rollTwo, "rollFill" );
        } else if ( rolls == 1 ) {
            this.addStyle( rollOne, "rollFill" );
        }
    }

    /**
     * Add style class if not exists
     *
     * @param node  to add
     * @param style to add
     */
    private void addStyle( Node node, String style ) {
        // Check if style already set
        if ( node.getStyleClass().contains( style ) ) return;

        // Add style
        node.getStyleClass().add( style );
    }

    /**
     * Reset border lines and selection from all dices
     */
    public void resetBorderLinesAndSelection() {
        // Reset dice selections
        this.selectedDices.forEach( checkBox -> checkBox.setSelected( false ) );

        // Remove dice border lines
        this.diceImages.forEach( imageView -> this.setDiceBorderLines( imageView, false ) );
    }

    /**
     * Register all events for the game controller
     */
    private void registerEvents( Game game ) {
        // Register events for dice click
        cbTopLeft.setOnAction( e -> this.handleClickOnDice( game, ivTopLeft, cbTopLeft ) );
        cbTopMiddle.setOnAction( e -> this.handleClickOnDice( game, ivTopMiddle, cbTopMiddle ) );
        cbTopRight.setOnAction( e -> this.handleClickOnDice( game, ivTopRight, cbTopRight ) );
        cbBottomLeft.setOnAction( e -> this.handleClickOnDice( game, ivBottomLeft, cbBottomLeft ) );
        cbBottomRight.setOnAction( e -> this.handleClickOnDice( game, ivBottomRight, cbBottomRight ) );

        // Register events for tip click
        this.tipLabels.forEach( label -> label.setOnMouseClicked( e -> this.handleClickOnTip( game, label ) ) );

        // Register event for button click
        btnRoll.setOnAction( e -> {
            // Prevent spamming
            if ( System.currentTimeMillis() <= ( this.lastClick + 100 ) ) {
                return;
            }

            // Disable button
            btnRoll.setDisable( true );

            // Update time for last click
            this.lastClick = System.currentTimeMillis();

            // Check if multiplayer online game
            if ( game.getType().equals( Game.Type.MULTIPLAYER_ONLINE ) ) {
                // Get user and send packet to server
                Attributes.getInstance().getUser().ifPresent( user ->
                        Main.getInstance().getNettyClient().sendPacket( new GamePlayerRollAgainPacket( user.getUsername() ) ) );
                return;
            }

            // Get current roll
            Roll roll = game.getCurrentRoll();

            // Check if roll available
            if ( roll.getRollsLeft() == 0 ) {
                return;
            }
            // Roll again
            roll.roll();
        } );
    }

    /**
     * Handle click on dice
     *
     * @param game      of the dice
     * @param imageView of the dice
     * @param checkBox  of the dice
     */
    private void handleClickOnDice( Game game, ImageView imageView, CheckBox checkBox ) {
        // Prevent spamming
        if ( System.currentTimeMillis() <= ( this.lastClick + 100 ) ) {
            return;
        }

        // Update time for last click
        this.lastClick = System.currentTimeMillis();

        boolean ignoreClick;
        // Check if online multiplayer game
        if ( game.getType().equals( Game.Type.MULTIPLAYER_ONLINE ) ) {
            // Ignore click when entity is not player of the client
            ignoreClick = !this.isEntityPlayerOfClient( game.getCurrentEntity() );
        } else {
            // Ignore click when current entity is a bot
            ignoreClick = game.getCurrentEntity().isBot();
        }

        // Check if the click should be ignored
        if ( ignoreClick ) return;

        // Check if dice selected
        if ( checkBox.isSelected() ) {
            // Save dice
            game.getCurrentRoll().saveDice( this.diceImages.indexOf( imageView ) );
        } else {
            // Remove dice
            game.getCurrentRoll().removeDice( this.diceImages.indexOf( imageView ) );
        }

        // Check if online game
        if ( game.getType().equals( Game.Type.MULTIPLAYER_ONLINE ) ) {
            // Get user
            Attributes.getInstance().getUser().ifPresent( user -> {
                // Get index from clicked image
                int index = this.diceImages.indexOf( imageView );

                Packet packet;

                // Check if dice selected or not
                if ( checkBox.isSelected() ) {
                    // Create dice save packet
                    packet = new GamePlayerSaveDicePacket( game.getCurrentEntity().getName(), game.getCurrentRoll().getDice().get( index ), index );
                } else {
                    // Create dice remove packet
                    packet = new GamePlayerRemoveDicePacket( game.getCurrentEntity().getName(), game.getCurrentRoll().getDice().get( index ), index );
                }
                // Send packet to serve
                Main.getInstance().getNettyClient().sendPacket( packet );
            } );
        }
    }

    /**
     * Handle click on tip
     *
     * @param game  of the tip
     * @param label of the tip
     */
    private void handleClickOnTip( Game game, Label label ) {
        // Prevent spamming
        if ( System.currentTimeMillis() <= ( this.lastClick + 100 ) ) {
            return;
        }

        // Update time for last click
        this.lastClick = System.currentTimeMillis();

        // Get Category from label
        Category category = Category.values()[this.tipLabels.indexOf( label )];

        // Set Category as result
        game.getCurrentRoll().setResult( category );

        // Send click on tip to server if online game
        if ( game.getType().equals( Game.Type.MULTIPLAYER_ONLINE ) ) {
            Attributes.getInstance().getUser().ifPresent( user -> Main.getInstance().getNettyClient().sendPacket(
                    new GamePlayerResultPacket( game.getCurrentEntity().getName(), category ) ) );
        }

        // Play sound
        SoundUtil.playSound( Attributes.getInstance().getTIP_SOUND() );
    }

    /**
     * Set or remove border lines of dice
     *
     * @param view   to set the border lines
     * @param enable or remove the lines
     */
    public void setDiceBorderLines( ImageView view, boolean enable ) {
        // Set border lines style
        /*if(enable) {
            view.getStyleClass().remove( "borderLineTestFalse" );
            view.getStyleClass().add( "borderLineTestTrue" );
        } else {
            view.getStyleClass().remove( "borderLineTestTrue" );
            view.getStyleClass().add( "borderLineTestFalse" );
        }*/
        // TODO Geht nur wenn z.B. ein Border Pane um das ImageView ist
        view.setStyle( "-fx-effect: " + ( enable ? "innershadow(three-pass-box, #6F55F2, 6, 1, 0, 0)" : "innershadow(three-pass-box, #262B32, 5, 1, 0, 0)" ) );
        //view.getStyleClass().add( enable ? "borderLineTestTrue" : "borderLineTestFalse" );

    }

    /**
     * Create a new stats table
     *
     * @param entities of the game to set
     */
    public void createStatsTable( List<AbstractEntity> entities ) {
        // Clear old columns
        tvCard.getColumns().clear();

        // Column for categories
        TableColumn<TableCategory, String> categoryColumn = new TableColumn<>( "Kategorie" );
        categoryColumn.setCellValueFactory( data -> new SimpleStringProperty( data.getValue().getName() ) );
        tvCard.getColumns().add( categoryColumn );

        // Disable moving column
        categoryColumn.setSortable( false );

        // Change width of column
        categoryColumn.setMaxWidth( 153 );
        categoryColumn.setPrefWidth( 153 );
        categoryColumn.setMinWidth( 153 );

        // Add listener to disable column moving
        tvCard.widthProperty().addListener( ( source, oldWidth, newWidth ) -> {
            TableHeaderRow header = (TableHeaderRow) tvCard.lookup( "TableHeaderRow" );
            header.reorderingProperty().addListener( ( observable, oldValue, newValue ) -> header.setReordering( false ) );
        } );

        // Create column for every entity
        entities.forEach( entity -> {
            // Create column with entity name
            TableColumn<TableCategory, String> column = new TableColumn<>( "" + ( entities.indexOf( entity ) + 1 ) );

            // Set value for column
            column.setCellValueFactory( data -> new SimpleStringProperty( data.getValue().getPoints( entity ) ) );

            // Add to list
            tvCard.getColumns().add( column );

            // Disable moving column
            column.setSortable( false );

            // Change width of column
            column.setMaxWidth( 42 );
            column.setPrefWidth( 42 );
            column.setMinWidth( 42 );
        } );

        // 0 = 2 Spieler
        // 1 = 3 Spieler
        // 2 = 4 Spieler
        // 3 = 5 Spieler
        // 4 = 6 Spieler
        // 5 = 7 Spieler
        // 6 = 8 Spieler
        int entitySize = entities.size() - 2;

        // Fix width for higher player size
        colCard.setMaxWidth( 239 + ( 42 * entitySize ) );
        colCard.setPrefWidth( 239 + ( 42 * entitySize ) );
        colCard.setMinWidth( 239 + ( 42 * entitySize ) );

        // Fix width for higher player size
        lnOne.setWidth( 239 + ( 42 * entitySize ) - 2 );
        lnTwo.setWidth( 239 + ( 42 * entitySize ) - 2 );
        lnThree.setWidth( 239 + ( 42 * entitySize ) - 2 );
        lnFour.setWidth( 239 + ( 42 * entitySize ) - 2 );
        lnHead.setWidth( 239 + ( 42 * entitySize ) - 2 );

        // Get all regular categories
        ObservableList<TableCategory> tableCategories = Arrays.stream( Category.values() ).map( category ->
                new TableCategory( category.getName() ) ).collect( Collectors.toCollection( FXCollections::observableArrayList ) );

        // Add extra table categories
        tableCategories.add( 6, new TableCategory( "Gesamt" ) );
        tableCategories.add( 7, new TableCategory( "Bonus" ) );
        tableCategories.add( new TableCategory( "Endsumme" ) );

        // Set categories
        tvCard.setItems( tableCategories );
    }

    /**
     * Refresh stats table
     */
    public void refreshStatsTable() {
        // Refresh table
        tvCard.refresh();
    }

    /**
     * Reset stats table
     */
    public void resetStatsTable() {
        // Clear old columns
        tvCard.getColumns().clear();
    }

    /**
     * Get all entities as string with name and position
     *
     * @param game of the entities
     * @return the entities as string
     */
    private String getPlayerAsString( Game game ) {
        // Player Info
        StringBuilder players = new StringBuilder();

        // Add all names of the entities
        game.getEntities().forEach( entity ->
                players.append( "Spieler " ).append( ( game.getEntities().indexOf( entity ) + 1 ) ).append( ": " )
                        .append( entity.getName() ).append( "\n" ) );

        return players.toString();
    }

    /**
     * Check if the entity is the player of the client
     *
     * @param entity to check
     * @return if the entity is the player of the client
     */
    public boolean isEntityPlayerOfClient( AbstractEntity entity ) {
        return entity.getName().equalsIgnoreCase( Attributes.getInstance().getUser().map( User::getUsername ).orElse( "Gast" ) );
    }
}
