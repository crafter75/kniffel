package de.csbme.kniffel.client.util;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hasher {

    /**
     * Get the hashed password with SHA-512
     *
     * @param password to hash
     * @return the result
     */
    public static String getHashedPassword( String password ) {
        if ( password.length() == 128 ) {
            return password;
        }
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance( "SHA-512" );
            byte[] bytes = md.digest( password.getBytes( StandardCharsets.UTF_8 ) );
            StringBuilder sb = new StringBuilder();
            for ( int i = 0; i < bytes.length; i++ ) {
                sb.append( Integer.toString( ( bytes[i] & 0xff ) + 0x100, 16 ).substring( 1 ) );
            }
            generatedPassword = sb.toString();
        } catch ( NoSuchAlgorithmException e ) {
            e.printStackTrace();
        }
        return generatedPassword;
    }
}
