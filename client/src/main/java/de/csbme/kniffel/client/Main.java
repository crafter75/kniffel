package de.csbme.kniffel.client;

import de.csbme.kniffel.client.network.NettyClient;
import de.csbme.kniffel.client.util.LocalDataUtil;
import de.csbme.kniffel.client.util.State;
import de.csbme.kniffel.client.util.WindowUtil;
import javafx.application.Application;
import javafx.stage.Stage;
import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

public class Main extends Application {

    @Getter
    private static Main instance;

    @Getter
    @Setter
    private Stage stage;

    @Getter
    @Setter
    private State currentState;

    @Getter
    private NettyClient nettyClient;

    @Getter
    @Setter
    private boolean firstLaunch;

    @Override
    public void start(Stage stage) throws Exception {
        instance = this;

        // Check if the first launch
        this.firstLaunch = !LocalDataUtil.existsFolder();

        // Set start State
        this.currentState = State.SPLASH_SCREEN;

        WindowUtil.createWindow(Attributes.getInstance().getSplashScreenFXML());

        this.nettyClient = new NettyClient();
    }

    @Override
    public void stop() throws Exception {
        this.nettyClient.disconnect();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
