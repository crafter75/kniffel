package de.csbme.kniffel.client.util;

import de.csbme.kniffel.client.Attributes;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.SVGPath;

import java.net.URL;

public class FooterUtil {

    private static StackPane stckFooter;

    private static SVGPath icoLeft, icoRight;

    private static Label lblTextLeft, lblTextRight;

    /**
     * Style the left area
     *
     * @param icon to style
     */
    private static void leftAreaStyle( String icon ) {
        // Set content
        icoLeft.setContent( icon );

        // Add style class
        icoLeft.getStyleClass().add( "footerIcon" );
        lblTextLeft.getStyleClass().add( "footerText" );

        // Set alignment
        StackPane.setAlignment( icoLeft, Pos.CENTER_LEFT );
        StackPane.setAlignment( lblTextLeft, Pos.CENTER_LEFT );

        // Set margin
        StackPane.setMargin( icoLeft, new Insets( 0, 0, 0, 26 ) );
        StackPane.setMargin( lblTextLeft, new Insets( 0, 0, 0, 56 ) );
    }

    /**
     * Add & style left area
     *
     * @param grd     to add
     * @param fxml    to load
     * @param text    to set for the left area
     * @param icon    to set for the left area
     * @param handler for click event
     */
    private static void leftArea( GridPane grd, URL fxml, String text, String icon, EventHandler<MouseEvent> handler ) {
       // Create object
        icoLeft = new SVGPath();
        lblTextLeft = new Label( text );

        // Style left area
        leftAreaStyle( icon );

        // Add click event if exists
        if ( handler != null ) {
            icoLeft.addEventHandler( MouseEvent.MOUSE_PRESSED, handler );
            lblTextLeft.addEventHandler( MouseEvent.MOUSE_PRESSED, handler );
        }

        // Add default click event
        icoLeft.setOnMousePressed( event -> onAreaClickEvent( grd, fxml ) );
        lblTextLeft.setOnMousePressed( event -> onAreaClickEvent( grd, fxml ) );
    }

    /**
     * Style the right area
     *
     * @param icon to style
     */
    private static void rightAreaStyle( String icon ) {
        // Set content
        icoRight.setContent( icon );

        // Add style class
        icoRight.getStyleClass().add( "footerIcon" );
        lblTextRight.getStyleClass().add( "footerText" );

        // Set alignment
        StackPane.setAlignment( icoRight, Pos.CENTER_RIGHT );
        StackPane.setAlignment( lblTextRight, Pos.CENTER_RIGHT );

        // Set margin
        StackPane.setMargin( icoRight, new Insets( 0, 26, 0, 0 ) );
        StackPane.setMargin( lblTextRight, new Insets( 0, 56, 0, 0 ) );
    }

    /**
     * Add & style right area
     *
     * @param grd     to add
     * @param fxml    to load
     * @param text    to set for the right area
     * @param icon    to set for the right area
     * @param handler for click event
     */
    private static void rightArea( GridPane grd, URL fxml, String text, String icon, EventHandler<MouseEvent> handler ) {
        // Create object
        icoRight = new SVGPath();
        lblTextRight = new Label( text );

        // Style right area
        rightAreaStyle( icon );

        // Add click event if exists
        if ( handler != null ) {
            icoRight.addEventHandler( MouseEvent.MOUSE_PRESSED, handler );
            lblTextRight.addEventHandler( MouseEvent.MOUSE_PRESSED, handler );
        }

        // Add default click event
        icoRight.setOnMousePressed( event -> onAreaClickEvent( grd, fxml ) );
        lblTextRight.setOnMousePressed( event -> onAreaClickEvent( grd, fxml ) );
    }

    /**
     * Handle click for the left & right text or icon
     *
     * @param grd  to close
     * @param fxml to open
     */
    private static void onAreaClickEvent( GridPane grd, URL fxml ) {
        try {
            // Switch window
            WindowUtil.switchWindow( fxml, grd );

            // Play sound
            SoundUtil.playSound( Attributes.getInstance().getBUTTON_SOUND() );
        } catch ( Exception ex ) {
            ex.printStackTrace();
        }
    }

    /**
     * Show the footer only with the left area
     *
     * @param grd      of the footer
     * @param fxml     to load
     * @param leftIcon of the footer
     * @param leftText of the footer
     */
    public static void show( GridPane grd, URL fxml, String leftIcon, String leftText ) {
        show( grd, fxml, null, leftIcon, leftText, null, null );
    }

    /**
     * Show the footer only with the left area & event handler
     *
     * @param grd      of the footer
     * @param fxml     to load
     * @param leftIcon of the footer
     * @param leftText of the footer
     * @param handler  for click event
     */
    public static void show( GridPane grd, URL fxml, String leftIcon, String leftText, EventHandler<MouseEvent> handler ) {
        show( grd, fxml, null, leftIcon, leftText, null, null, handler, null );
    }

    /**
     * Show the footer with both areas
     *
     * @param grd       of the footer
     * @param fxmlLeft  to load
     * @param fxmlRight to load
     * @param leftIcon  of the left area
     * @param leftText  of the left area
     * @param rightIcon of the right area
     * @param rightText of the right area
     */
    public static void show( GridPane grd, URL fxmlLeft, URL fxmlRight, String leftIcon, String leftText, String rightIcon, String rightText ) {
        show( grd, fxmlLeft, fxmlRight, leftIcon, leftText, rightIcon, rightText, null, null );
    }

    /**
     * Show the footer with both areas with click events
     *
     * @param grd               of the footer
     * @param fxmlLeft          to load
     * @param fxmlRight         to load
     * @param leftIcon          of the left area
     * @param leftText          of the left area
     * @param rightIcon         of the right area
     * @param rightText         of the right area
     * @param leftEventHandler  for the left area
     * @param rightEventHandler for the right area
     */
    public static void show( GridPane grd, URL fxmlLeft, URL fxmlRight, String leftIcon, String leftText,
                             String rightIcon, String rightText, EventHandler<MouseEvent> leftEventHandler, EventHandler<MouseEvent> rightEventHandler ) {
        // Creat stack pane
        stckFooter = new StackPane();

        // Add style class
        stckFooter.getStyleClass().add( "mainColorBackground" );

        // Creat left area
        leftArea( grd, fxmlLeft, leftIcon, leftText, leftEventHandler );

        // Check if right area exists
        if ( fxmlRight != null && rightIcon != null && rightText != null ) {
            // Create right area
            rightArea( grd, fxmlRight, rightIcon, rightText, rightEventHandler );

            // Add children to footer
            stckFooter.getChildren().addAll( icoLeft, lblTextLeft, icoRight, lblTextRight );
        } else {
            // Add children to footer
            stckFooter.getChildren().addAll( icoLeft, lblTextLeft );
        }

        // Add to grid pane
        grd.add( stckFooter, 0, 2 );
    }

}