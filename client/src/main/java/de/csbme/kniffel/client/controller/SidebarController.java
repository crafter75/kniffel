package de.csbme.kniffel.client.controller;

import com.sun.javafx.scene.control.skin.TableHeaderRow;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.net.URL;
import java.util.ResourceBundle;

public class SidebarController implements Initializable {

    @FXML
    TableView<Data> tvSidebar;

    @FXML
    TableColumn<Data, String> tcCategory, tcRule, tcPoints;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // Set value
        tcCategory.setCellValueFactory( new PropertyValueFactory<>( "category" ));
        tcRule.setCellValueFactory( new PropertyValueFactory<>( "rule" ));
        tcPoints.setCellValueFactory( new PropertyValueFactory<>( "points" ));

        // Disable table sorting
        tcCategory.setSortable( false );
        tcRule.setSortable( false );
        tcPoints.setSortable( false );

        // Add listener to disable sorting
        tvSidebar.widthProperty().addListener( ( source, oldWidth, newWidth ) -> {
            TableHeaderRow header = (TableHeaderRow) tvSidebar.lookup( "TableHeaderRow" );
            header.reorderingProperty().addListener( ( observable, oldValue, newValue ) -> header.setReordering( false ) );
        } );

        // Set items
        tvSidebar.setItems(getData());
    }

    /**
     * Get data for rule sidebar
     *
     * @return the data as list
     */
    private ObservableList<Data> getData() {
        // Create list
        ObservableList<Data> data = FXCollections.observableArrayList();

        // Add to list
        data.add(new Data("Einser", "Alle Einser werden gezählt", "1-4"));
        data.add(new Data("Zweier", "Alle Zweier werden gezählt", "2-10"));
        data.add(new Data("Dreier", "Alle Dreier werden gezählt", "3-15"));
        data.add(new Data("Vierer", "Alle Vierer werden gezählt", "4-20"));
        data.add(new Data("Fünfer", "Alle Fünfer werden gezählt", "5-25"));
        data.add(new Data("Sechser", "Alle Sechser werden gezählt", "6-30"));
        data.add(new Data("Bonus", "Mindestens 63 Punkte", "35"));
        data.add(new Data("Dreierpasch", "3 gleiche Würfel – Summe aller Augenzahlen", "5-30"));
        data.add(new Data("Viererpasch", "4 gleiche Würfel – Summe aller Augenzahlen", "5-30"));
        data.add(new Data("Full House", "3 gleiche und 2 gleiche Würfel", "25"));
        data.add(new Data("Kleine Straße", "[1-2-3-4]; [2-3-4-5], oder [3-4-5-6]", "30"));
        data.add(new Data("Große Straße", "[1-2-3-4-5] oder [2-3-4-5-6]", "40"));
        data.add(new Data("Kniffel", "5 gleiche Würfel", "50"));
        data.add(new Data("Chance", "Summe aller Augenzahlen", "5-30"));
        data.add(new Data("", "Der Spieler mit den meisten Punkten gewinnt", ""));

        return data;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    public class Data {
        private String category, rule, points;
    }
}

