package de.csbme.kniffel.client.game;

import de.csbme.kniffel.client.Attributes;
import de.csbme.kniffel.client.Main;
import de.csbme.kniffel.client.controller.GameController;
import de.csbme.kniffel.client.controller.MainMenuController;
import de.csbme.kniffel.client.user.User;
import de.csbme.kniffel.client.util.NotificationUtil;
import de.csbme.kniffel.client.util.SoundUtil;
import de.csbme.kniffel.client.util.TimelineUtil;
import de.csbme.kniffel.client.util.WindowUtil;
import de.csbme.kniffel.lib.game.Game;
import de.csbme.kniffel.lib.game.category.Category;
import de.csbme.kniffel.lib.game.handler.AbstractGameHandler;
import de.csbme.kniffel.lib.game.player.AbstractEntity;
import de.csbme.kniffel.lib.game.player.GamePlayer;
import de.csbme.kniffel.lib.game.util.Dice;
import de.csbme.kniffel.lib.game.util.Roll;
import de.csbme.kniffel.lib.network.packet.UserManipulateStatsPacket;
import de.csbme.kniffel.lib.time.TimeUtil;
import de.csbme.kniffel.lib.user.Stats;

import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

public class ClientGameHandler extends AbstractGameHandler {

    private GameController gameController;

    public ClientGameHandler( Game game, GameController gameController ) {
        super( game );
        this.gameController = gameController;
    }

    @Override
    public void handleStart( List<AbstractEntity> entities ) {
        // Create stats table with all entities
        this.gameController.createStatsTable( entities );

        // Load sidebar with rules
        this.gameController.loadSidebar();

        // Check if not online game and client connected to the server
        if ( !this.getGame().getType().equals( Game.Type.MULTIPLAYER_ONLINE ) && Main.getInstance().getNettyClient().isConnected() ) {
            // Get user
            Attributes.getInstance().getUser().ifPresent( user -> {
                // Send stats to server
                Main.getInstance().getNettyClient().sendPacket( new UserManipulateStatsPacket( user.getId(), Stats.Key.GAMES.ordinal(), true, 1 ) );
                Main.getInstance().getNettyClient().sendPacket( new UserManipulateStatsPacket( user.getId(), Stats.Key.LAST_PLAYED.ordinal(), false,
                        System.currentTimeMillis() ) );
            } );
        }
    }

    @Override
    public void handleNextEntity( AbstractEntity entity ) {
        // Set current player name
        this.gameController.setCurrentPlayerName( this.getName( entity ) );

        // Reset border lines and reset selections
        this.gameController.resetBorderLinesAndSelection();

        // Check if current entity a bot or current entity is not the player of the client
        boolean disableButton = this.isABotOrNotClientPlayer( entity );

        // Disable button for roll
        this.gameController.getBtnRoll().setDisable( disableButton );

        if ( disableButton ) return;

        // Move tip for current player
        this.moveTip( this.getGame().getEntities().indexOf( entity ) );
    }

    @Override
    public void handleDiceRoll( AbstractEntity entity, Roll roll ) {
        // Play sound
        SoundUtil.playSound( Attributes.getInstance().getROLL_SOUND() );

        // Set rolls left
        this.gameController.setRollsLeft( roll.getRollsLeft() );

        // Set dice images if the dice not saved
        IntStream.range( 0, roll.getDice().size() ).filter( i -> !roll.getSavedIndex().contains( i ) ).forEach( i ->
                this.gameController.getDiceImages().get( i ).setImage( Attributes.getInstance().getImageForDice( roll.getDice().get( i ) ) ) );

        // Check if tips and button disabled
        if ( this.isABotOrNotClientPlayer( entity ) ) return;

        // Hide tips
        this.gameController.getTipLabels().forEach( label -> label.setVisible( false ) );
        this.gameController.getTips().forEach( svgPath -> svgPath.setVisible( false ) );

        // Create object
        TimelineUtil timelineUtil = new TimelineUtil();

        // Check if all rolls used
        if ( roll.getRollsLeft() == 0 ) {
            // Disable button
            this.gameController.getBtnRoll().setDisable( true );

            // Show tips with all available categories after 800 milli seconds
            timelineUtil.addFrame( 800L, e ->
                    entity.getAvailableCategories().forEach( category -> this.showTip( category, roll.getDice() ) ) );
        } else {
            timelineUtil.addFrame( 800L, e -> {
                // Enable button
                this.gameController.getBtnRoll().setDisable( false );

                // Show tips with all possible categories after 800 milli seconds (possible = points > 0)
                roll.getPossibleCategories().forEach( category -> this.showTip( category, roll.getDice() ) );
            } );
        }

        // Start
        timelineUtil.start();
    }

    @Override
    public void handleDiceSave( AbstractEntity entity, Dice dice, int diceIndex ) {
        // Set dice border lines
        this.gameController.setDiceBorderLines( this.gameController.getDiceImages().get( diceIndex ), true );
    }

    @Override
    public void handleDiceRemove( AbstractEntity entity, Dice dice, int diceIndex ) {
        // Set dice border lines
        this.gameController.setDiceBorderLines( this.gameController.getDiceImages().get( diceIndex ), false );
    }

    @Override
    public void handleDiceRollEnd( AbstractEntity entity, Category category, int value ) {
        // Refresh stats table
        this.gameController.refreshStatsTable();

        // Hide all tips
        this.gameController.getTipLabels().forEach( label -> label.setVisible( false ) );
        this.gameController.getTips().forEach( svgPath -> svgPath.setVisible( false ) );
    }

    @Override
    public void handlePlayerQuit( GamePlayer player, boolean afk ) {
        // Show player information
        String reason = afk ? player.getName() + " wurde gekickt! [AFK]" : player.getName() + " hat das Spiel verlassen";
        this.gameController.setInformation( reason );

        // Rebuild stats table
        this.gameController.resetStatsTable();
        this.gameController.createStatsTable( this.getGame().getEntities() );
    }

    @Override
    public void handleEnd( List<AbstractEntity> entities, long timeOfGame, boolean finish ) {
        if ( !finish ) {
            switch ( Main.getInstance().getCurrentState() ) {
                case GAME:
                    // Get game controller
                    GameController gameController = Attributes.getInstance().getCurrentController( GameController.class );

                    try {
                        // Switch window
                        WindowUtil.switchWindow( Attributes.getInstance().getMainMenuFXML(), gameController.getGrdMain() );

                        // Run delayed notification
                        new TimelineUtil( 1000L, e -> {
                            // Get main menu controller
                            MainMenuController mainMenuController = Attributes.getInstance().getCurrentController( MainMenuController.class );

                            // Show notification
                            NotificationUtil.show( "Spielabbruch", "Das Spiel wurde abgebrochen!",
                                    mainMenuController.getStckMenu(), mainMenuController.getAnchMenu() );
                        } ).start();
                    } catch ( Exception e ) {
                        e.printStackTrace();
                    }
                    break;
                case MAIN_MENU:
                    // Get main menu controller
                    MainMenuController mainMenuController = Attributes.getInstance().getCurrentController( MainMenuController.class );

                    // Run delayed notification
                    new TimelineUtil( 1000L, e -> NotificationUtil.show( "Spielabbruch", "Das Spiel wurde abgebrochen!",
                            mainMenuController.getStckMenu(), mainMenuController.getAnchMenu() ) ).start();
                    break;
                default:
                    break;
            }
            return;
        }

        // Build message
        StringBuilder builder = new StringBuilder();

        // Message with the winner
        if ( entities.size() > 0 ) {
            String name = this.getName( entities.get( 0 ) );
            builder.append( name ).append( name.equals( "Du" ) ? " hast" : " hat" ).append( " das Spiel gewonnen!\n\n" );
        }

        // Message with ranking
        IntStream.range( 0, entities.size() ).forEach( i -> builder.append( "#" ).append( ( i + 1 ) ).append( " | " )
                .append( entities.get( i ).getName() ).append( ": " ).append( entities.get( i ).getAllPoints() )
                .append( " Punkte" ).append( "\n" ) );

        // Add time of the game
        builder.append( "\n" ).append( "Diese Runde dauerte: " ).append( TimeUtil.getDurationFromMilliseconds( timeOfGame ) );

        // Show notification with information
        NotificationUtil.show( "Spielende", builder.toString(), this.gameController.getStckGame(), e -> {
            try {
                WindowUtil.switchWindow( Attributes.getInstance().getMainMenuFXML(), gameController.getBtnRoll() );
            } catch ( Exception e2 ) {
                e2.printStackTrace();
            }
        } );

        // Check if not online game and client connected to the server
        if ( !this.getGame().getType().equals( Game.Type.MULTIPLAYER_ONLINE ) && Main.getInstance().getNettyClient().isConnected() ) {
            // Get user
            Attributes.getInstance().getUser().ifPresent( user -> {
                // Send stats to server
                Main.getInstance().getNettyClient().sendPacket( new UserManipulateStatsPacket( user.getId(), Stats.Key.PLAY_TIME.ordinal(), true, timeOfGame ) );
                Main.getInstance().getNettyClient().sendPacket( new UserManipulateStatsPacket( user.getId(), Stats.Key.LAST_PLAYED.ordinal(), false, System.currentTimeMillis() ) );

                Optional<AbstractEntity> optionalEntity = entities.stream().filter( e -> e.getName().equalsIgnoreCase( user.getUsername() ) ).findFirst();
                optionalEntity.ifPresent( entity -> Main.getInstance().getNettyClient().sendPacket(
                        new UserManipulateStatsPacket( user.getId(), Stats.Key.POINTS.ordinal(), true, entity.getAllPoints() ) ) );

                if ( entities.size() > 0 && entities.get( 0 ).getName().equalsIgnoreCase( user.getUsername() ) ) {
                    Main.getInstance().getNettyClient().sendPacket( new UserManipulateStatsPacket( user.getId(), Stats.Key.WINS.ordinal(), true, 1 ) );
                }
            } );
        }
    }

    /**
     * Show tip for player
     *
     * @param category of the tip
     * @param dice     to calculate the points
     */
    private void showTip( Category category, List<Dice> dice ) {
        // Get index from category
        int index = category.ordinal();

        // Show tip with the amount of points for the category
        this.gameController.getTipLabels().get( index ).setText( category.getPoints( dice ) + "" );
        this.gameController.getTips().get( index ).setVisible( true );
        this.gameController.getTipLabels().get( index ).setVisible( true );
    }

    /**
     * Move tip for player
     *
     * @param index of the player
     */
    private void moveTip( int index ) {
        // Calculate the amount with player index
        int move = 93 + ( index * 40 );

        // Move label to next player and disable visibility
        this.gameController.getTipLabels().forEach( label -> {
            label.setTranslateX( move );
            label.setVisible( false );
        } );

        // Move svg path to next player and disable visibility
        this.gameController.getTips().forEach( svgPath -> {
            svgPath.setTranslateX( move );
            svgPath.setVisible( false );
        } );
    }

    /**
     * Get name for the player
     *
     * @return the name
     */
    private String getName( AbstractEntity entity ) {
        // Check game type
        if ( this.getGame().getType().equals( Game.Type.SINGLEPLAYER ) && !entity.isBot() ) {
            return "Du";
        } else if ( this.getGame().getType().equals( Game.Type.MULTIPLAYER_ONLINE ) ) {
            // Get user
            Optional<User> optionalUser = Attributes.getInstance().getUser();

            // Check if exists and name equals
            if ( optionalUser.isPresent() && entity.getName().equals( optionalUser.get().getUsername() ) ) {
                return "Du";
            }
        }
        return entity.getName();
    }

    /**
     * Check if the entity is a bot or not the player of this client
     *
     * @param entity to check
     * @return the result
     */
    private boolean isABotOrNotClientPlayer( AbstractEntity entity ) {
        // Check if online multiplayer game
        if ( this.getGame().getType().equals( Game.Type.MULTIPLAYER_ONLINE ) ) {
            // Disable tips if the entity is not the player of the client
            return !this.gameController.isEntityPlayerOfClient( entity );
        } else {
            // Disable tips if the entity is a bot
            return entity.isBot();
        }
    }
}
