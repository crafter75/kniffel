package de.csbme.kniffel.client.util;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

public enum Style {

    // Styles
    NONE( "None" ),
    BOX( "Box" ),
    PARALLAX( "Parallax" ),
    BUBBLE( "Bubble" ),
    ABSTRACT( "Abstract" );

    @Getter
    private String name;

    Style( String name ) {
        this.name = name;
    }

    /**
     * Get a style by the name
     *
     * @param name of the style
     * @return the style
     */
    public static Optional<Style> fromName( String name ) {
        return Arrays.stream( Style.values() ).filter( style -> style.getName().equalsIgnoreCase( name ) ).findFirst();
    }
}
