package de.csbme.kniffel.client.controller;

import com.jfoenix.controls.JFXButton;
import de.csbme.kniffel.client.Attributes;
import de.csbme.kniffel.client.Main;
import de.csbme.kniffel.client.util.*;
import de.csbme.kniffel.lib.network.packet.UserStatsRequestPacket;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.web.WebView;
import lombok.Getter;

import java.awt.*;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ResourceBundle;

@Getter
public class MainMenuController implements Initializable {

    @FXML
    @Getter
    GridPane grdMain;

    @FXML
    Pane pnFirstLaunch, pnPage;

    @FXML
    WebView wvBackground;

    @FXML
    @Getter
    StackPane stckMenu;

    @FXML
    AnchorPane anchMenu;

    @FXML
    Rectangle lnTitle;

    @FXML
    ImageView ivDice;

    @FXML
    Label lblTitle;

    @FXML
    JFXButton btnSp, btnStatistics, btnMpLocale, btnMpOnline, btnSettings, btnAbout, btnExit, btnServerStatus, btnPlay;

    /**
     * Handle action event for exit button
     *
     * @param event to handle
     */
    private void onExitActionEvent( ActionEvent event ) {
        // Play sound
        SoundUtil.playSound( Attributes.getInstance().getBUTTON_SOUND() );

        // Exit
        System.exit( 0 );
    }

    /**
     * Handle action event for settings button
     *
     * @param event to handle
     */
    private void onSettingsActionEvent( ActionEvent event ) {
        try {
            if ( !Attributes.getInstance().getUser().isPresent() ) {
                NotificationUtil.show( "Fehler", "Für die Einstellungen musst du dich anmelden!",
                        stckMenu, anchMenu );
                return;
            }
            // Reset web engine
            resetWebEngine();

            // Update current state
            Main.getInstance().setCurrentState( State.SETTINGS );

            // Switch to settings
            WindowUtil.switchWindow( Attributes.getInstance().getSettingsFXML(), grdMain );

            // Play sound
            SoundUtil.playSound( Attributes.getInstance().getBUTTON_SOUND() );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Handle action event for singleplayer button
     *
     * @param event to handle
     */
    private void onSpActionEvent( ActionEvent event ) {
        try {
            // Reset web engine
            resetWebEngine();

            // Update current state
            Main.getInstance().setCurrentState( State.SINGLEPLAYER );

            // Switch to singleplayer
            WindowUtil.switchWindow( Attributes.getInstance().getSpFXML(), grdMain );

            // Play sound
            SoundUtil.playSound( Attributes.getInstance().getBUTTON_SOUND() );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Handle action event for multiplayer local button
     *
     * @param event to handle
     */
    private void onMpLocaleActionEvent( ActionEvent event ) {
        try {
            // Reset web engine
            resetWebEngine();

            // Update current state
            Main.getInstance().setCurrentState( State.MULTIPLAYER_LOCAL );

            // Switch to mp local
            WindowUtil.switchWindow( Attributes.getInstance().getMpLocaleFXML(), grdMain );

            // Play sound
            SoundUtil.playSound( Attributes.getInstance().getBUTTON_SOUND() );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Handle action event for multiplayer online button
     *
     * @param event to handle
     */
    private void onMpOnlineActionEvent( ActionEvent event ) {
        try {
            // Check if server online
            if(!Main.getInstance().getNettyClient().isConnected()) {
                NotificationUtil.show( "Fehler", "Der Online-Server ist offline!",
                        stckMenu, anchMenu );
                return;
            }

            // Check if user logged in
            if ( !Attributes.getInstance().getUser().isPresent() ) {
                NotificationUtil.show( "Fehler", "Um Online spielen zu können musst du dich anmelden!",
                        stckMenu, anchMenu );
                return;
            }
            // Reset web engine
            resetWebEngine();

            // Update current state
            Main.getInstance().setCurrentState( State.MULTIPLAYER_ONLINE );

            // Switch to mp online
            WindowUtil.switchWindow( Attributes.getInstance().getMpOnlineFXML(), grdMain );

            // Play sound
            SoundUtil.playSound( Attributes.getInstance().getBUTTON_SOUND() );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Handle action event for about button
     *
     * @param event to handle
     */
    private void onAboutActionEvent( ActionEvent event ) {
        try {
            // Reset web engine
            resetWebEngine();

            // Update current state
            Main.getInstance().setCurrentState( State.ABOUT );

            // Switch to about
            WindowUtil.switchWindow( Attributes.getInstance().getAboutFXML(), grdMain );

            // Play sound
            SoundUtil.playSound( Attributes.getInstance().getBUTTON_SOUND() );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Handle action event for statistics button
     *
     * @param event to handle
     */
    private void onStatisticsActionEvent( ActionEvent event ) {
        try {
            // Check if user not logged in
            if ( !Attributes.getInstance().getUser().isPresent() ) {
                NotificationUtil.show( "Fehler", "Um die Statistiken abrufen zu können musst du dich anmelden!",
                        stckMenu, anchMenu );
                return;
            }
            // Reset web engine
            resetWebEngine();

            // Update current state
            Main.getInstance().setCurrentState( State.STATISTICS );

            // Switch to statistics
            WindowUtil.switchWindow( Attributes.getInstance().getStatisticsFXML(), grdMain );

            // Play sound
            SoundUtil.playSound( Attributes.getInstance().getBUTTON_SOUND() );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Handle action event for firstlaunch button
     *
     * @param event to handle
     */
    private void onFirstLaunchActionEvent( ActionEvent event ) {
        try {
            // Disable first launch screen
            pnFirstLaunch.setVisible( false );
            pnFirstLaunch.setDisable( true );
            SoundUtil.playSound( Attributes.getInstance().getBUTTON_SOUND() );
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    @Override
    public void initialize( URL location, ResourceBundle resources ) {
        // Update state
        Main.getInstance().setCurrentState( State.MAIN_MENU );

        // Open Website
        pnPage.setOnMousePressed(e -> {
            try {
                Desktop.getDesktop().browse(URI.create("http://www.kniffel.kuqs.pro"));
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        // Check if the first launch
        if ( !Main.getInstance().isFirstLaunch() ) {
            pnFirstLaunch.setVisible( false );
            pnFirstLaunch.setDisable( true );
        } else {
            Main.getInstance().setFirstLaunch( false );
        }

        // Check if netty connected
        if ( Main.getInstance().getNettyClient().isConnected() ) {
            btnServerStatus.setText( "Online" );
            btnServerStatus.getStyleClass().add( "statusOnlineTest" );
        }

        // Register events
        this.registerEvents();

        // Create timeline for title
        new TimelineUtil( 1L,
                e -> TitleBarUtil.show( grdMain, Main.getInstance().getStage(), true, true, true, true ) ).start();

        // Show custom background
        WebUtil.customBackground( wvBackground );

    }

    /**
     * Reset WebEngine to prevent lagging
     */
    private void resetWebEngine() {
        wvBackground.getEngine().loadContent( "" );
    }

    /**
     * Register all main menu events
     */
    private void registerEvents() {
        this.btnSp.setOnAction( this::onSpActionEvent );
        this.btnMpLocale.setOnAction( this::onMpLocaleActionEvent );
        this.btnMpOnline.setOnAction( this::onMpOnlineActionEvent );
        this.btnExit.setOnAction( this::onExitActionEvent );
        this.btnSettings.setOnAction( this::onSettingsActionEvent );
        this.btnAbout.setOnAction( this::onAboutActionEvent );
        this.btnStatistics.setOnAction( this::onStatisticsActionEvent );
        this.btnPlay.setOnAction( this::onFirstLaunchActionEvent );
    }

}
