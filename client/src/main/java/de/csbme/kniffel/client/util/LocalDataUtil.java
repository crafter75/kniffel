package de.csbme.kniffel.client.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;

public class LocalDataUtil {

    private static final String TEMP_PATH = System.getProperty( "java.io.tmpdir" );

    private static final File FOLDER = new File( TEMP_PATH + "/Kniffel" ),
            PROPERTIES_FILE = new File( FOLDER, "data.properties" );

    private static Properties properties = null;

    /**
     * Check if the folder exists
     *
     * @return if the folder exists
     */
    public static boolean existsFolder() {
        return FOLDER.exists();
    }

    /**
     * Get property from file
     *
     * @param key of the property
     * @return the property as optional
     */
    public static Optional<String> getProperty( String key ) {
        // Init properties
        initProperties();

        // Check if key exists
        if ( !properties.containsKey( key ) ) {
            return Optional.empty();
        }
        // Return value of key
        return Optional.of( properties.getProperty( key ) );
    }

    /**
     * Set a property to file
     *
     * @param key   to set
     * @param value to set
     */
    public static void setProperty( String key, String value ) {
        // Init properties
        initProperties();

        // Set property
        properties.setProperty( key, value );

        try {
            // Save properties
            properties.store( new FileOutputStream( PROPERTIES_FILE ), "Do not change anything here!" );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Remove a property from file
     *
     * @param key to remove
     */
    public static void removeProperty( String key ) {
        // Init properties
        initProperties();

        // Remove key
        properties.remove( key );

        try {
            // Save properties
            properties.store( new FileOutputStream( PROPERTIES_FILE ), "Do not change anything here!" );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }

    /**
     * Init the properties file
     */
    private static void initProperties() {
        // Check if already loaded
        if ( properties != null ) return;

        // Create empty properties
        properties = new Properties();

        // Check if folder exists
        if ( !existsFolder() ) {
            FOLDER.mkdir();
        }

        // Check if file exists
        if ( !PROPERTIES_FILE.exists() ) {
            try {
                // No, create empty properties file
                properties.store( new FileOutputStream( PROPERTIES_FILE ), "Do not change anything here!" );
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        }

        try {
            // Load properties file
            properties.load( new FileInputStream( PROPERTIES_FILE ) );
        } catch ( IOException e ) {
            e.printStackTrace();
        }
    }
}
