package de.csbme.kniffel.client.controller.gamesettings;

import com.jfoenix.controls.JFXSlider;
import de.csbme.kniffel.client.Attributes;
import de.csbme.kniffel.client.Main;
import de.csbme.kniffel.client.user.User;
import de.csbme.kniffel.client.util.*;
import de.csbme.kniffel.lib.game.Game;
import de.csbme.kniffel.lib.game.GameManager;
import de.csbme.kniffel.lib.game.player.GamePlayer;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;

import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Optional;
import java.util.ResourceBundle;

public class SpController implements Initializable {

    @FXML
    GridPane grdSettings;

    @FXML
    WebView wvBackground;

    @FXML
    Label lblTitle;

    @FXML
    FontAwesomeIconView icoTitle;

    @FXML
    Pane pnSp;

    @FXML
    JFXSlider sldBots;

    @Override
    public void initialize( URL location, ResourceBundle resources ) {
        WebUtil.customBackground( wvBackground );

        new TimelineUtil( 1L,
                e -> TitleBarUtil.show( grdSettings, Main.getInstance().getStage(), true, true, true, true ) ).start();

        // Add event handler for click on play
        EventHandler<MouseEvent> handler = event -> {
            Main.getInstance().setCurrentState( State.GAME );

            // Create name attribute
            String playerName;
            try {
                // Get name from player or computer name
                playerName = Attributes.getInstance().getUser().map( User::getUsername ).orElse( InetAddress.getLocalHost().getHostName() );
            } catch( UnknownHostException e ) {
                // Set default name
                playerName = "Lokaler Benutzer";
            }

            // Create game
            Game game = GameManager.getInstance().createGame( Game.Type.SINGLEPLAYER ).withPlayers( new GamePlayer( playerName ) ).withBots( (int) sldBots.getValue() );

            // Set game
            Attributes.getInstance().setGame( Optional.of( game ) );
        };

        // Show footer
        FooterUtil.show( grdSettings, Attributes.getInstance().getMainMenuFXML(), Attributes.getInstance().getGameFXML(),
                "Zurück", Attributes.getInstance().getBACK_ICON(), "Spielen",
                Attributes.getInstance().getPLAY_ICON(), null, handler );

    }

}
