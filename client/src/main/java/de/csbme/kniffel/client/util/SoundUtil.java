package de.csbme.kniffel.client.util;

import de.csbme.kniffel.client.Attributes;
import de.csbme.kniffel.client.user.User;
import de.csbme.kniffel.lib.user.Settings;
import javafx.scene.media.AudioClip;

import java.util.Optional;

public class SoundUtil {

    /**
     * Play sound
     *
     * @param soundFile to play
     */
    public static void playSound( String soundFile ) {
        // Get User
        Optional<User> optionalUser = Attributes.getInstance().getUser();

        // Check if exists and has played sound
        if ( !optionalUser.isPresent() || optionalUser.get().getSettings().get( Settings.Key.PLAY_SOUND, Boolean.class ) ) {
            // Create audio clip from file
            AudioClip sound = new AudioClip( soundFile );

            // Set specific volume or default if user not logged in
            sound.setVolume( optionalUser.isPresent() ?
                    optionalUser.get().getSettings().get( Settings.Key.SOUND_VOLUME, Integer.class ) : 50 );

            // Play sound
            sound.play();
        }
    }
}
