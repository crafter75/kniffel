package de.csbme.kniffel.client.controller;

import com.jfoenix.controls.*;
import de.csbme.kniffel.client.Attributes;
import de.csbme.kniffel.client.Main;
import de.csbme.kniffel.client.user.User;
import de.csbme.kniffel.client.util.*;
import de.csbme.kniffel.lib.network.packet.UserManipulateSettingPacket;
import de.csbme.kniffel.lib.time.TimeUtil;
import de.csbme.kniffel.lib.user.Settings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebView;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SettingsController implements Initializable {

    @FXML
    GridPane grdSettings;

    @FXML
    StackPane stckMenu;

    @FXML
    WebView wvBackground;


    @FXML
    AnchorPane anchPane;

    @FXML
    Label lblUsername, lblRegistered;

    @FXML
    JFXTextField txtChangeUsername;

    @FXML
    JFXPasswordField pwdChange;

    @FXML
    JFXButton btnLogOut;

    @FXML
    JFXComboBox<String> cbxTheme, cbxStyle;

    @FXML
    JFXToggleButton tbAnimation, tbSound;

    @FXML
    Pane pnAnimation, pnSound;

    @FXML
    JFXSlider sldVolume;

    @Override
    public void initialize( URL location, ResourceBundle resources ) {
        WebUtil.customBackground( wvBackground );

        // Update state
        Main.getInstance().setCurrentState( State.SETTINGS );

        new TimelineUtil( 1L,
                e -> TitleBarUtil.show( grdSettings, Main.getInstance().getStage(), true, true, true, true ) ).start();

        // Show footer
        FooterUtil.show( grdSettings, Attributes.getInstance().getMainMenuFXML(), Attributes.getInstance().getMainMenuFXML(), "Zurück",
                Attributes.getInstance().getBACK_ICON(), "Speichern", Attributes.getInstance().getSAVE_ICON() );

        Settings settings;
        if ( Attributes.getInstance().getUser().isPresent() ) {
            settings = Attributes.getInstance().getUser().get().getSettings();
        } else {
            settings = new Settings();
        }

        // Get all themes and add it to the combo box
        ObservableList<String> themeList = FXCollections.observableArrayList(
                Stream.of( Theme.values() ).map( Theme::getName ).collect( Collectors.toList() ) );
        cbxTheme.setItems( themeList );
        cbxTheme.setValue( themeList.get( settings.get( Settings.Key.THEME_STYLE, Integer.class ) ) );

        // Get all styles and add it to the combo box
        ObservableList<String> styleList = FXCollections.observableArrayList(
                Stream.of( Style.values() ).map( Style::getName ).collect( Collectors.toList() ) );
        cbxStyle.setItems( styleList );
        cbxStyle.setValue( styleList.get( settings.get( Settings.Key.BACKGROUND_STYLE, Integer.class ) ) );

        // Add Event for change theme
        cbxTheme.setOnAction( e -> Theme.fromName( cbxTheme.getSelectionModel().getSelectedItem() )
                .ifPresent( theme -> {
                    Attributes.getInstance().setCurrentTheme( theme );

                    // Save setting to database
                    this.sendSettingToServer( Settings.Key.THEME_STYLE, theme.ordinal() );
                } ) );

        // Add Event for change style
        cbxStyle.setOnAction( e -> Style.fromName( cbxStyle.getSelectionModel().getSelectedItem() )
                .ifPresent( style -> {
                    Attributes.getInstance().setCurrentStyle( style );

                    // Save setting to database
                    this.sendSettingToServer( Settings.Key.BACKGROUND_STYLE, style.ordinal() );
                } ) );

        // Set default for toggle buttons
        tbAnimation.setSelected( settings.get( Settings.Key.ANIMATED_BACKGROUND, Boolean.class ) );
        tbSound.setSelected( settings.get( Settings.Key.PLAY_SOUND, Boolean.class ) );

        // Fix style
        this.stylePane( tbAnimation, pnAnimation );
        this.stylePane( tbSound, pnSound );

        // Add event for animation toggle button
        tbAnimation.setOnAction( event -> {
            this.stylePane( tbAnimation, pnAnimation );

            // Save setting to database
            this.sendSettingToServer( Settings.Key.ANIMATED_BACKGROUND, tbAnimation.isSelected() );
        } );

        // Add event for sound toggle button
        tbSound.setOnAction( event -> {
            this.stylePane( tbSound, pnSound );

            // Save setting to database
            this.sendSettingToServer( Settings.Key.PLAY_SOUND, tbSound.isSelected() );
        } );

        // Set default for sound volume
        sldVolume.setValue( settings.get( Settings.Key.SOUND_VOLUME, Integer.class ) );

        // Add event for sound volume
        sldVolume.setOnMouseReleased( event ->
                this.sendSettingToServer( Settings.Key.SOUND_VOLUME, (int) sldVolume.getValue() ) );
        sldVolume.setOnKeyReleased( event ->
                this.sendSettingToServer( Settings.Key.SOUND_VOLUME, (int) sldVolume.getValue() ) );

        // Set username & registration date
        if ( Attributes.getInstance().getUser().isPresent() ) {
            User user = Attributes.getInstance().getUser().get();

            // TODO change username and password
            // TODO add user id (user.getId())
            lblUsername.setText( user.getUsername() );
            lblRegistered.setText( TimeUtil.getDateFromMilliseconds( user.getRegistrationDate(), false ) );
        }
    }

    /**
     * Send the setting to the server to save it into the database
     *
     * @param settingsKey of the setting
     * @param value       of the setting
     */
    private void sendSettingToServer( Settings.Key settingsKey, Object value ) {
        // Check if value can cast to stats class
        if ( !settingsKey.getClassValue().isInstance( value ) ) {
            throw new IllegalArgumentException( "Can't cast " + value.getClass().getSimpleName() + " to " + settingsKey.getClassValue().getSimpleName() );
        }

        // Check if user exists
        Attributes.getInstance().getUser().ifPresent( user -> {
            // Send setting to server
            Main.getInstance().getNettyClient().sendPacket(
                    new UserManipulateSettingPacket( user.getId(), settingsKey.ordinal(), value ) );

            // Manipulate local settings
            user.getSettings().manipulate( settingsKey, value );
        } );
    }

    /**
     * Format toggle button
     *
     * @para
     */
    private void stylePane(JFXToggleButton button, Pane pane) {
        if ( button.isSelected() ) {
            pane.getStyleClass().remove( "backgroundTestBackground" );
            pane.getStyleClass().add( "mainTestBackground" );
        } else {
            pane.getStyleClass().remove( "mainTestBackground" );
            pane.getStyleClass().add( "backgroundTestBackground" );
        }
    }

}
