package de.csbme.kniffel.client.controller;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextField;
import com.sun.javafx.scene.control.skin.TableHeaderRow;
import de.csbme.kniffel.client.Attributes;
import de.csbme.kniffel.client.Main;
import de.csbme.kniffel.client.user.User;
import de.csbme.kniffel.client.util.FooterUtil;
import de.csbme.kniffel.client.util.TimelineUtil;
import de.csbme.kniffel.client.util.TitleBarUtil;
import de.csbme.kniffel.client.util.WebUtil;
import de.csbme.kniffel.lib.network.packet.UserRankingRequestPacket;
import de.csbme.kniffel.lib.network.packet.UserStatsRequestPacket;
import de.csbme.kniffel.lib.time.TimeUtil;
import de.csbme.kniffel.lib.user.RankingEntry;
import de.csbme.kniffel.lib.user.Stats;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;
import javafx.scene.web.WebView;
import lombok.Getter;

import java.net.URL;
import java.text.NumberFormat;
import java.util.*;
import java.util.stream.Collectors;

@Getter
public class StatisticsController implements Initializable {

    private static final NumberFormat NUMBER_FORMAT = NumberFormat.getInstance();

    @FXML
    GridPane grdMain, grdStatistics;

    @FXML
    WebView wvBackground;

    @FXML
    TableView<String> tvMyStats, tvSearchPlayer;

    @FXML
    TableView<RankingEntry> tvTop10;

    @FXML
    JFXTextField txtSearchPlayer;

    @FXML
    JFXButton btnSearchPlayer;

    @FXML
    JFXTabPane tpStatistics;

    private String currentPlayerStats = null;

    @Override
    public void initialize( URL location, ResourceBundle resources ) {
        // Change maximum faction digits
        NUMBER_FORMAT.setMaximumFractionDigits( 2 );

        // Set custom background
        WebUtil.customBackground( wvBackground );

        // Show title bar delayed
        new TimelineUtil( 1L,
                e -> TitleBarUtil.show( grdStatistics, Main.getInstance().getStage(), true, true, true, true ) ).start();

        // Show footer
        FooterUtil.show( grdStatistics, Attributes.getInstance().getMainMenuFXML(), "Zurück", Attributes.getInstance().getBACK_ICON() );

        // Load statistics for player
        Attributes.getInstance().getUser().ifPresent( user -> Main.getInstance().getNettyClient().sendPacket(
                new UserStatsRequestPacket( user.getUsername() ) ) );

        // Add change listener for tab pane
        tpStatistics.getSelectionModel().selectedIndexProperty().addListener( ( observable, oldValue, newValue ) -> {
            // Check if change to ranking
            if ( newValue.intValue() == 1 ) {
                // Check if ranking loaded
                if ( !tvTop10.getColumns().isEmpty() ) return;

                // Get ranking from server
                Main.getInstance().getNettyClient().sendPacket( new UserRankingRequestPacket() );
                return;
            }
        } );

        // Set button name
        btnSearchPlayer.setText( "Spieler suchen" );

        // Add event for search player button
        btnSearchPlayer.setOnAction( event -> {
            // Check if current stats loaded
            if ( this.currentPlayerStats != null && this.currentPlayerStats.equalsIgnoreCase( txtSearchPlayer.getText() ) ) {
                return;
            }
            // Send packet to server
            Main.getInstance().getNettyClient().sendPacket( new UserStatsRequestPacket( txtSearchPlayer.getText() ) );
        } );
    }

    /**
     * Show user stats
     *
     * @param name  of the User
     * @param stats of the User
     */
    public void showStats( String name, Stats stats, int ranking ) {
        TableView<String> table;

        // Check if tab pane in own stats
        switch ( tpStatistics.getSelectionModel().getSelectedIndex() ) {
            case 0:
                // Get user
                Optional<User> optionalUser = Attributes.getInstance().getUser();

                // Check if user exists
                if ( optionalUser.isPresent() && optionalUser.get().getUsername().equalsIgnoreCase( name ) ) {
                    // Own stats
                    table = tvMyStats;
                    break;
                }
                return;
            case 2:
                // Other stats
                table = tvSearchPlayer;
                this.currentPlayerStats = name;
                break;
            default:
                return;
        }

        // Clear old columns
        table.getColumns().clear();

        // Column for stats key
        TableColumn<String, String> statsKeyColumn = new TableColumn<>( "Statistik" );
        statsKeyColumn.setCellValueFactory( data -> new SimpleStringProperty( data.getValue() ) );
        table.getColumns().add( statsKeyColumn );

        // Column for stats value
        TableColumn<String, String> statsValueColumn = new TableColumn<>( "Wert" );
        statsValueColumn.setCellValueFactory( data -> {
            switch ( data.getValue().toLowerCase() ) {
                case "name":
                    return new SimpleStringProperty( name );
                case "position im ranking":
                    return new SimpleStringProperty( ranking + ". Platz" );
                case "sieg wahrscheinlichkeit":
                    return new SimpleStringProperty( this.getWinChance( stats ) );
                default:
                    return Stats.Key.fromDisplayName( data.getValue() ).map( key -> {
                        switch ( key ) {
                            case LAST_PLAYED:
                                return new SimpleStringProperty( TimeUtil.getDateFromMilliseconds(
                                        stats.get( key, Long.class ), true ) );
                            case PLAY_TIME:
                                return new SimpleStringProperty( TimeUtil.getDurationFromMilliseconds(
                                        stats.get( key, Long.class ) ) );
                            default:
                                return new SimpleStringProperty( String.valueOf(
                                        stats.get( key, key.getClassValue() ) ) );
                        }
                    } ).orElseGet( () -> new SimpleStringProperty( "Unbekannt" ) );
            }
        } );
        table.getColumns().add( statsValueColumn );

        // Get all stats keys
        ObservableList<String> statsKeys = Arrays.stream( Stats.Key.values() ).map( Stats.Key::getDisplayName )
                .collect( Collectors.toCollection( FXCollections::observableArrayList ) );

        // Add extra stats keys
        statsKeys.add( 0, "Name" );
        statsKeys.add( 2, "Position im Ranking" );
        statsKeys.add( 6, "Sieg Wahrscheinlichkeit" );

        // Set stats keys
        table.setItems( statsKeys );
    }

    /**
     * Show user ranking
     *
     * @param rankingEntries to set
     */
    public void showRanking( LinkedList<RankingEntry> rankingEntries ) {
        // Clear old columns
        tvTop10.getColumns().clear();

        // Get all stats keys
        List<String> statKeys = Arrays.stream( Stats.Key.values() ).filter( key ->
                !key.equals( Stats.Key.LAST_PLAYED ) && !key.equals( Stats.Key.HIGHEST_POINTS_PER_GAME ) )
                .map( Stats.Key::getDisplayName ).collect( Collectors.toList() );

        // Add extra stats keys
        statKeys.add( 0, "Platz" );
        statKeys.add( 1, "Name" );

        // Iterate and create columns
        statKeys.forEach( key -> {
            // Create column with name
            TableColumn<RankingEntry, String> column = new TableColumn<>( key );

            // Set value factory
            column.setCellValueFactory( data -> {
                switch ( key.toLowerCase() ) {
                    case "platz":
                        return new SimpleStringProperty( ( rankingEntries.indexOf( data.getValue() ) + 1 ) + "" );
                    case "name":
                        return new SimpleStringProperty( data.getValue().getUsername() );
                    default:
                        return Stats.Key.fromDisplayName( key ).map( k -> {
                            if ( k == Stats.Key.PLAY_TIME ) {
                                return new SimpleStringProperty( TimeUtil.getDurationFromMilliseconds(
                                        data.getValue().getStats().get( k, Long.class ) ) );
                            }
                            return new SimpleStringProperty( String.valueOf(
                                    data.getValue().getStats().get( k, k.getClassValue() ) ) );
                        } ).orElseGet( () -> new SimpleStringProperty( "Unbekannt" ) );
                }
            } );

            // Disable moving column
            column.setSortable( false );

            // Add to table
            tvTop10.getColumns().add( column );
        } );

        // Add listener to disable column moving
        tvTop10.widthProperty().addListener( ( source, oldWidth, newWidth ) -> {
            TableHeaderRow header = (TableHeaderRow) tvTop10.lookup( "TableHeaderRow" );
            header.reorderingProperty().addListener( ( observable, oldValue, newValue ) -> header.setReordering( false ) );
        } );

        // Set ranking entries
        tvTop10.setItems( rankingEntries.stream().collect( Collectors.toCollection( FXCollections::observableArrayList ) ) );
    }

    /**
     * Get win chance from stats
     *
     * @param stats to calculate
     */
    private String getWinChance( Stats stats ) {
        // Get stats for calculation
        int wins = stats.get( Stats.Key.WINS, Integer.class );
        int games = stats.get( Stats.Key.GAMES, Integer.class );

        // Calculate
        double chance = ( wins != 0 ? ( wins * 100.0D ) : 0 ) / games;
        return NUMBER_FORMAT.format( chance ) + "%";
    }
}
