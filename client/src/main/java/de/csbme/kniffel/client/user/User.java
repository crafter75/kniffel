package de.csbme.kniffel.client.user;

import de.csbme.kniffel.lib.user.Settings;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class User {

    private final int id;

    private String username;

    private final long registrationDate;

    private final Settings settings;
}
