package de.csbme.kniffel.client.util;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import com.jfoenix.controls.events.JFXDialogEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.effect.BoxBlur;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;

public class NotificationUtil {

    /**
     * Notification with pane and close event handler
     *
     * @param header       to show
     * @param message      to show
     * @param stck         to place on
     * @param pane         to place on
     * @param closeHandler to handle when the dialog is closed
     */
    public static void show(String header, String message, StackPane stck, Pane pane,
                            EventHandler<JFXDialogEvent> closeHandler) {
        // Create dialog layout
        JFXDialogLayout dialogLayout = new JFXDialogLayout();

        // Create back button
        JFXButton button = new JFXButton("Zurück");

        // Create dialog
        JFXDialog dialog = new JFXDialog(stck, dialogLayout, JFXDialog.DialogTransition.TOP);

        // Set action event for button
        button.setOnAction(e -> dialog.close());

        // Add labels and button to dialog layout
        dialogLayout.setHeading(new Label(header));
        dialogLayout.setBody(new Label(message));
        dialogLayout.setActions(button);

        // Show dialog
        dialog.show();

        // Check if close handler exists
        if (closeHandler != null) {
            // Add close event handler
            dialog.addEventHandler(JFXDialogEvent.CLOSED, closeHandler);
        }

        // Check if pane exists
        if (pane != null) {
            // Add effect to pane
            pane.setEffect(new BoxBlur(5, 5, 5));

            // Set close event for pane to reset effect
            dialog.setOnDialogClosed(event -> pane.setEffect(null));
        }
    }

    /**
     * Notification with close event handler
     *
     * @param header       to show
     * @param message      to show
     * @param stck         to place on
     * @param closeHandler to handle when the dialog is closed
     */
    public static void show(String header, String message, StackPane stck, EventHandler<JFXDialogEvent> closeHandler) {
        show(header, message, stck, null, closeHandler);
    }

    /**
     * Notification with pane
     *
     * @param header  to show
     * @param message to show
     * @param stck    to place on
     * @param pane    to place on
     */
    public static void show(String header, String message, StackPane stck, Pane pane) {
        show(header, message, stck, pane, null);
    }

    /**
     * Notification
     *
     * @param header  to show
     * @param message to show
     * @param stck    to place on
     */
    public static void show(String header, String message, StackPane stck) {
        show(header, message, stck, null, null);
    }

}
