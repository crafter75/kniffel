package de.csbme.kniffel.client.controller.gamesettings;

import com.jfoenix.controls.JFXTextField;
import de.csbme.kniffel.client.Attributes;
import de.csbme.kniffel.client.Main;
import de.csbme.kniffel.client.user.User;
import de.csbme.kniffel.client.util.*;
import de.csbme.kniffel.lib.game.Game;
import de.csbme.kniffel.lib.game.GameManager;
import de.csbme.kniffel.lib.game.player.GamePlayer;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class MpLocaleController implements Initializable {

    @FXML
    GridPane grdSettings;

    @FXML
    WebView wvBackground;


    @FXML
    Label lblTitle;

    @FXML
    FontAwesomeIconView icoTitle;

    @FXML
    Pane pnMpLocale;

    @FXML
    JFXTextField txtPlayer1, txtPlayer2, txtPlayer3, txtPlayer4, txtPlayer5, txtPlayer6, txtPlayer7, txtPlayer8;

    private List<JFXTextField> textFields;

    @Override
    public void initialize( URL location, ResourceBundle resources ) {
        WebUtil.customBackground( wvBackground );

        this.textFields = new ArrayList<>();

        this.textFields = Arrays.stream( this.getClass().getDeclaredFields() ).filter( f -> f.isAnnotationPresent( FXML.class )
                && f.getType().isAssignableFrom( JFXTextField.class ) ).map( f -> {
            try {
                return (JFXTextField) f.get( this );
            } catch ( IllegalAccessException e ) {
                e.printStackTrace();
                return null;
            }
        } ).filter( Objects::nonNull ).collect( Collectors.toList() );

        // Create title
        new TimelineUtil( 1L,
                e -> TitleBarUtil.show( grdSettings, Main.getInstance().getStage(), true, true, true, true ) ).start();

        // Get User
        Optional<User> optionalUser = Attributes.getInstance().getUser();

        optionalUser.ifPresent( user -> {
            txtPlayer1.setDisable( true );
            txtPlayer1.setText( user.getUsername() );
        } );

        // Add event handler for click on play
        EventHandler<MouseEvent> handler = event -> {
            Main.getInstance().setCurrentState( State.GAME );

            List<GamePlayer> players = this.textFields.stream().filter( txtField -> !txtField.getText().isEmpty() )
                    .map( txtField -> new GamePlayer( txtField.getText() ) ).collect( Collectors.toList() );

            // Check if two players exists
            if ( players.size() < 2 ) {
                return;
            }

            // Check if names unique
            if ( players.stream().anyMatch( p -> players.stream().filter( p2 -> p2.getName().equalsIgnoreCase( p.getName() ) ).count() > 1 ) ) {
                return;
            }

            Game game = GameManager.getInstance().createGame( Game.Type.MULTIPLAYER_LOCAL ).withPlayers( players );
            Attributes.getInstance().setGame( Optional.of( game ) );
        };

        // Show footer
        FooterUtil.show( grdSettings, Attributes.getInstance().getMainMenuFXML(), Attributes.getInstance().getGameFXML(),
                "Zurück", Attributes.getInstance().getBACK_ICON(), "Spielen",
                Attributes.getInstance().getPLAY_ICON(), null, handler );

    }

}
