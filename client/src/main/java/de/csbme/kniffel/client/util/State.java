package de.csbme.kniffel.client.util;

public enum State {

    FIRST_LAUNCH,
    SPLASH_SCREEN,
    LOGIN,
    REGISTER,
    MAIN_MENU,
    SETTINGS,
    STATISTICS,
    ABOUT,
    SINGLEPLAYER,
    MULTIPLAYER_LOCAL,
    MULTIPLAYER_ONLINE,
    GAME;
}
