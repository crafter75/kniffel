package de.csbme.kniffel.client.util;

import de.csbme.kniffel.client.Attributes;
import javafx.scene.web.WebView;

public class WebUtil {

    /**
     * Custom background
     *
     * @param wv to set
     */
    public static void customBackground( WebView wv ) {
        // Load page
        wv.getEngine().load( Attributes.getInstance().getSite( Attributes.getInstance().getCurrentStyle() ) );
    }

}
