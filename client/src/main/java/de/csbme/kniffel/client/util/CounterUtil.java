package de.csbme.kniffel.client.util;

import com.jfoenix.controls.JFXProgressBar;
import com.jfoenix.controls.JFXToggleButton;
import de.csbme.kniffel.client.Attributes;
import eu.hansolo.medusa.Gauge;
import javafx.animation.KeyValue;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.web.WebView;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicInteger;

public class CounterUtil {

    /**
     * Create gauge counter
     *
     * @param gauge  for the counter
     * @param period of the timer to run
     */
    public static void gaugeCounter( Gauge gauge, long period ) {
        // Create atomic integer to count
        AtomicInteger integer = new AtomicInteger( 0 );

        // Create timer
        new Timer().scheduleAtFixedRate( new TimerTask() {
            @Override
            public void run() {
                // Set value and increment
                Platform.runLater( () -> gauge.setValue( integer.getAndIncrement() ) );

                // Check if integer equals or greater then 100
                if ( integer.get() >= 100 ) {
                    // Cancel timer
                    this.cancel();
                }
            }
        }, 0L, period );
    }

    /**
     * Set bar counter
     *
     * @param pb         to set
     * @param millis     to set
     * @param lblPercent to display percent
     */
    public static void barCounter( JFXProgressBar pb, long millis, Label lblPercent ) {
        // Create TimelineUtil with value for progress bar
        new TimelineUtil().addFrame( 0L, new KeyValue( pb.progressProperty(), 0 ) )
                .addFrame( millis, new KeyValue( pb.progressProperty(), 1 ) )
                .start();

        // Bind status in percent
        lblPercent.textProperty().bind( pb.progressProperty().multiply( 100 ).asString( "%.0f" ).concat( "%" ) );
    }

}
