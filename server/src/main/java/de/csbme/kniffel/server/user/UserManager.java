package de.csbme.kniffel.server.user;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.kniffel.lib.error.HTTPCodes;
import de.csbme.kniffel.lib.network.packet.ErrorPacket;
import de.csbme.kniffel.lib.network.packet.UserLoginCallbackPacket;
import de.csbme.kniffel.lib.network.packet.UserRegisterCompletePacket;
import de.csbme.kniffel.lib.network.packet.lobby.LobbyDeletePacket;
import de.csbme.kniffel.server.Main;
import de.csbme.kniffel.server.mysql.row.Row;
import io.netty.channel.Channel;
import lombok.Getter;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;

public class UserManager {

    private Main main;

    @Getter
    private List<User> users = new CopyOnWriteArrayList<>();

    public UserManager( Main main ) {
        this.main = main;

        // Start time for timeout
        new Timer().scheduleAtFixedRate( new TimerTask() {
            @Override
            public void run() {
                long current = System.currentTimeMillis();

                // Get iterator from list
                Iterator<User> userIterator = UserManager.this.users.iterator();

                // Iterate if has next user
                while ( userIterator.hasNext() ) {
                    // Get user
                    User user = userIterator.next();

                    // Check timeout
                    if ( current > user.getTimeout() ) {
                        // Close channel
                        user.getChannel().close();

                        // Log out
                        UserManager.this.logOutUser( user );
                    }
                }
            }
        }, 3000L, 3000L ); // 3 seconds
    }

    /**
     * Register User
     *
     * @param username of the User
     * @param password of the User
     * @param channel  of the User
     */
    public void registerUser( String username, String password, Channel channel ) {
        Futures.addCallback( this.main.getUserRepository().insertUserAndGetId( username, password ), new FutureCallback<Integer>() {
            @Override
            public void onSuccess( Integer id ) {
                // Check if Id exists
                if ( id == null ) {
                    UserManager.this.main.getLogger().error( "User: Registered User-ID is null!" );
                    channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_503, "Fehler beim generieren der User-ID!" ) );
                    return;
                }

                // Id exists. Insert to other tables
                UserManager.this.main.getStatsRepository().insertUser( id );
                UserManager.this.main.getSettingRepository().insertUser( id );

                UserManager.this.main.getLogger().info( "Netty/User: '" + username + "' has been registered!" );

                // Send packet to client
                channel.writeAndFlush( new UserRegisterCompletePacket( id, username ) );
            }

            @Override
            public void onFailure( Throwable throwable ) {
                UserManager.this.main.getLogger().error( "User: Can't register User!", (Exception) throwable );
                channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_503, "Fehler beim registrieren!" ) );
            }
        }, MoreExecutors.directExecutor() );
    }

    /**
     * Login a User
     *
     * @param id      of the User
     * @param channel of the User
     */
    public void loginUser( int id, Channel channel ) {
        try {
            // Get user row
            Row row = this.main.getUserRepository().getUserRow( id ).get().first();

            // Create user
            User user = new User( id, row.getString( "name" ),
                    row.getLong( "registration_date" ), channel );

            // Add to list
            this.users.add( user );

            // Update address
            this.main.getUserRepository().updateAddress( id, user.getAddress() );

            this.main.getLogger().info( "Netty/User: '" + user.getUsername() + "' logged in! [" + user.getAddress() + "]" );

            // Send callback packet to client
            user.sendPacket( new UserLoginCallbackPacket( user.getId(), user.getUsername(),
                    user.getRegistrationDate(), this.main.getSettingRepository().getSettings( id ).get() ) );
        } catch ( InterruptedException | ExecutionException e ) {
            Main.getInstance().getLogger().error( "Error while loading settings & registration date", e );
            channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_502, "Fehler beim Laden der Daten!" ) );
        }
    }

    /**
     * Log out a User
     *
     * @param user to log out
     */
    public void logOutUser( User user ) {
        // Check if logged in
        if ( !this.users.contains( user ) ) return;

        // Remove lobby or user from lobby
        this.main.getLobbyManager().getLobbies().stream().filter( lobby -> lobby.getLeader().equals( user )
                || lobby.getPlayers().contains( user ) ).findFirst().ifPresent( lobby -> {
            // Check if lobby leader
            if ( lobby.getLeader().equals( user ) ) {
                // Create lobby packet
                LobbyDeletePacket lobbyDeletePacket = new LobbyDeletePacket( lobby.getName() );

                // Send to all players
                lobby.getPlayers().stream().filter( user2 -> user2.getId() != lobby.getLeader().getId() ).forEach( user2 -> user.sendPacket( lobbyDeletePacket ) );

                // Remove lobby
                this.main.getLobbyManager().getLobbies().remove( lobby );
                return;
            }
            lobby.removeUser( user );
        } );

        // Remove from game
        user.getGame().ifPresent( game -> {
            // Remove user
            game.removePlayer( user );

            // Reset game
            user.setGame( Optional.empty() );
        } );

        // Remove user
        this.main.getUserManager().getUsers().remove( user );
        this.main.getLogger().info( "Netty/User: '" + user.getUsername() + "' logged out!" );
    }

    /**
     * Get a User by the username
     *
     * @param username of the User
     * @return the User.
     */
    public Optional<User> getUser( String username ) {
        return this.users.stream().filter( user -> user.getUsername().equalsIgnoreCase( username ) ).findFirst();
    }

    /**
     * Get a User by the id
     *
     * @param id of the User
     * @return the User. the result can be null if the User not found
     */
    public Optional<User> getUser( int id ) {
        return this.users.stream().filter( user -> user.getId() == id ).findFirst();
    }

    /**
     * Get a User by the netty channel
     *
     * @param channel of the User
     * @return the User. the result can be null if the User not found
     */
    public Optional<User> getUser( Channel channel ) {
        return this.users.stream().filter( user -> user.getChannel().equals( channel ) ).findFirst();
    }
}
