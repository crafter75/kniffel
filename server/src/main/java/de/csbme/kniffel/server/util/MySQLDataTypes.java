package de.csbme.kniffel.server.util;

public class MySQLDataTypes {

    /**
     * Get the mysql data type for a java class
     *
     * @param clazz of the data type
     * @return the mysql data type as string
     */
    public static String getMySQLDataType( Class clazz ) {
        switch ( clazz.getSimpleName().toLowerCase() ) {
            case "integer":
                return "INT";
            case "long":
                return "BIGINT";
            case "string":
                return "VARCHAR(30)";
            default:
                return clazz.getSimpleName().toUpperCase();
        }
    }
}
