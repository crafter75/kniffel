package de.csbme.kniffel.server.mysql.repository;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.kniffel.lib.user.RankingEntry;
import de.csbme.kniffel.lib.user.Stats;
import de.csbme.kniffel.server.Main;
import de.csbme.kniffel.server.mysql.MySQL;
import de.csbme.kniffel.server.mysql.row.Rows;
import de.csbme.kniffel.server.util.MySQLDataTypes;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.concurrent.ExecutionException;

public class StatsRepository {

    private MySQL mySQL;

    public StatsRepository( MySQL mySQL ) {
        this.mySQL = mySQL;

        // Create stats table
        try {
            // Create builder for stats table
            StringBuilder queryBuilder = new StringBuilder( "CREATE TABLE IF NOT EXISTS kniffel_stats (user_id INT NOT NULL" );

            // Add rows for settings
            Arrays.stream( Stats.Key.values() ).forEach( key -> queryBuilder.append( "," ).append( key.toString() ).append( " " )
                    .append( MySQLDataTypes.getMySQLDataType( key.getClassValue() ) )
                    .append( " NOT NULL DEFAULT " ).append( key.getDefaultValue() ) );

            // Add foreign key and make user_id unique
            queryBuilder.append( ", FOREIGN KEY (user_id) REFERENCES kniffel_users (id), UNIQUE (user_id))" );

            mySQL.execute( queryBuilder.toString() ).get();
        } catch ( InterruptedException | ExecutionException e ) {
            Main.getInstance().getLogger().error( "StatsRepository: Error while creating table 'kniffel_stats'", e );
        }
    }

    /**
     * Insert a user
     *
     * @param userId to insert
     * @return future if the User has been inserted
     */
    public ListenableFuture<Boolean> insertUser( int userId ) {
        return this.mySQL.execute( "INSERT INTO kniffel_stats (user_id) VALUES (?)", userId );
    }

    /**
     * Get the Stats of a User
     *
     * @param userId of the User
     * @return future with the Stats. if no stats found it will return a empty Stats object
     */
    public ListenableFuture<Stats> getStats( int userId ) {
        // Get stats from user id
        ListenableFuture<Rows> futureRows = this.mySQL.query( "SELECT * FROM kniffel_stats WHERE user_id = ?", userId );

        // Load stats
        return Futures.transform( futureRows, rows -> {
            // Create stats object
            Stats stats = new Stats();

            // Check if stats exists
            if ( rows != null && rows.first() != null ) {
                // Add stats to object
                rows.first().getValues().entrySet().stream().filter( e -> !e.getKey().equals( "user_id" ) )
                        .forEach( e -> stats.manipulate( Stats.Key.valueOf( e.getKey().toUpperCase() ), e.getValue() ) );
            }
            return stats;
        }, MoreExecutors.directExecutor() );
    }

    /**
     * Manipulate or increase Stats of a User
     *
     * @param userId of the Stats
     * @param statKey of the Stats
     * @param value of the Stats
     */
    public void manipulateOrIncreaseStats(int userId, Stats.Key statKey, boolean increase, Object value) throws ExecutionException, InterruptedException {
        // Check if stat should be increase
        if ( increase ) {
            // Get stats
            Stats stats = this.getStats( userId ).get();

            // Check if stat key points
            if ( statKey.equals( Stats.Key.POINTS ) ) {
                // Get points from packet
                int points = (Integer) value;

                // Check if points higher than the old highest points per game
                if ( points > stats.get( Stats.Key.HIGHEST_POINTS_PER_GAME, Integer.class ) ) {
                    // Replace points in database
                    this.manipulateStats( userId, Stats.Key.HIGHEST_POINTS_PER_GAME, points );
                }
            }

            switch ( statKey.getClassValue().getSimpleName().toLowerCase() ) {
                case "integer":
                    // Cast old and new value to integer and increase the value
                    int iValue = stats.get( statKey, Integer.class ) + (Integer) value;
                    // Manipulate into database
                    this.manipulateStats( userId, statKey, iValue );
                    break;
                case "long":
                    // Cast old and new value to long and increase the value
                    long lValue = stats.get( statKey, Long.class ) + (Long) value;
                    // Manipulate into database
                    this.manipulateStats( userId, statKey, lValue );
                    break;
                default:
                    throw new UnsupportedOperationException( "Only integer & long can be increase!" );

            }
            return;
        }
        // Manipulate stat value in database
        this.manipulateStats( userId, statKey, value );
        return;
    }

    /**
     * Manipulate Stats of a User
     *
     * @param userId  of the Stats
     * @param statKey of the Stats
     * @param value   of the Stats
     */
    private ListenableFuture<Boolean> manipulateStats( int userId, Stats.Key statKey, Object value ) {
        // Check if value has the same class
        if ( !statKey.getClassValue().getSimpleName().equals( value.getClass().getSimpleName() ) ) {
            throw new IllegalArgumentException( "Can't cast " + value.getClass().getSimpleName() + " to " + statKey.getClassValue().getSimpleName() );
        }
        return this.mySQL.execute( "UPDATE kniffel_stats SET " + statKey.toString() + " = ? WHERE user_id = ?",
                value, userId );
    }

    /**
     * Get the top ten ranking
     *
     * @param rankingKey to sort
     * @return the top ten ranking
     */
    public ListenableFuture<LinkedList<RankingEntry>> getTopTen( Stats.Key rankingKey ) {
        // Get top ten
        ListenableFuture<Rows> futureRows = this.mySQL.query( "SELECT stats.user_id, stats." + rankingKey.toString()
                + ", user.name, user.id FROM kniffel_stats AS stats INNER JOIN kniffel_users AS user ON (stats.user_id = user.id)"
                + " ORDER BY stats." + rankingKey.toString()
                + " DESC LIMIT 10" );

        // Load top ten
        return Futures.transform( futureRows, rows -> {
            // Create empty list
            LinkedList<RankingEntry> linkedList = new LinkedList<>();

            // Check if rows exists
            if ( rows != null ) {
                // Iterate rows
                rows.all().forEach( row -> {
                    try {
                        // Add ranking with stats
                        linkedList.add( new RankingEntry( row.getString( "name" ),
                                this.getStats( row.getInt( "id" ) ).get() ) );
                    } catch ( InterruptedException | ExecutionException e ) {
                        e.printStackTrace();
                    }
                } );
            }
            return linkedList;
        }, MoreExecutors.directExecutor() );
    }

    /**
     * Get the ranking of a User by id
     *
     * @param userId     of the User
     * @param rankingKey to sort
     * @return future with the ranking number. the result can be null if the User not found
     */
    public ListenableFuture<Integer> getUserRanking( int userId, Stats.Key rankingKey ) {
        // Get user ranking position
        ListenableFuture<Rows> futureRows = this.mySQL.query( "SELECT * FROM (SELECT t.user_id, @rn := @rn + 1"
                + " AS rank FROM kniffel_stats t CROSS JOIN (SELECT @rn := 0) AS PARAMETERS ORDER BY t." + rankingKey.toString()
                + " DESC) t2 WHERE t2.user_id = ?", userId );

        // Load user ranking
        return Futures.transform( futureRows, rows -> {
            // Check if user found and id exists
            if ( rows != null && rows.first() != null ) {
                return rows.first().get( "rank", Double.class ).intValue();
            }
            return -1;
        }, MoreExecutors.directExecutor() );
    }
}