package de.csbme.kniffel.server.game;

import de.csbme.kniffel.lib.game.Game;
import de.csbme.kniffel.lib.game.GameManager;
import de.csbme.kniffel.lib.game.player.GamePlayer;
import de.csbme.kniffel.lib.network.packet.lobby.LobbyUpdatePacket;
import de.csbme.kniffel.server.Main;
import de.csbme.kniffel.server.user.User;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Getter
public class Lobby {

    private final String name;

    private final User leader;

    private final List<User> players;

    public Lobby( String name, User leader ) {
        this.name = name;
        this.leader = leader;
        this.players = new ArrayList<>();
        this.players.add( leader );
    }

    /**
     * Add User to lobby
     *
     * @param user to add
     */
    public void addUser( User user ) {
        // Check if inside list
        if ( this.players.contains( user ) ) return;

        // Add to list
        this.players.add( user );

        // Send update packet to all players
        this.sendUpdatePacket();
    }

    /**
     * Remove User from lobby
     *
     * @param user to remove
     */
    public void removeUser( User user ) {
        // Check if inside list
        if ( !this.players.contains( user ) ) return;

        // Remove from list
        this.players.remove( user );

        // Send update packet to all players
        this.sendUpdatePacket();
    }

    /**
     * Start lobby
     */
    public void start() {
        // Create game with players
        Game game = GameManager.getInstance().createGame( Game.Type.MULTIPLAYER_ONLINE ).withPlayers( this.players.stream().map( user ->
                (GamePlayer) user ).collect( Collectors.toList() ) );

        // Add handler for game
        game.withHandler( new ServerGameHandler( game ) );

        // Set game for users
        this.players.forEach( user -> user.setGame( Optional.of( game ) ) );

        // Start game
        game.start();

        // Delete lobby
        Main.getInstance().getLobbyManager().getLobbies().remove( this );
    }

    /**
     * Send update packet to all Users
     */
    private void sendUpdatePacket() {
        // Get player names (without leader)
        List<String> players = this.players.stream().filter( user -> user.getId() != this.leader.getId() ).map( User::getUsername ).collect( Collectors.toList() );

        // Create packet
        LobbyUpdatePacket lobbyUpdatePacket = new LobbyUpdatePacket( this.name, this.leader.getUsername(), players );

        // Send to all players
        this.players.forEach( user -> user.sendPacket( lobbyUpdatePacket ) );
    }
}
