package de.csbme.kniffel.server.user;

import de.csbme.kniffel.lib.game.Game;
import de.csbme.kniffel.lib.game.player.GamePlayer;
import de.csbme.kniffel.lib.network.Packet;
import de.csbme.kniffel.server.Main;
import io.netty.channel.Channel;
import lombok.Getter;
import lombok.Setter;

import java.util.Optional;

@Getter
public class User extends GamePlayer {

    private final int id;

    private String username;

    private final long registrationDate;

    @Setter
    private Channel channel;

    private long timeout;

    @Setter
    private Optional<Game> game;

    User( int id, String username, long registrationDate, Channel channel ) {
        super( username );
        this.id = id;
        this.username = username;
        this.registrationDate = registrationDate;
        this.channel = channel;
        this.game = Optional.empty();
        this.updateTimeout();
    }

    /**
     * Update timeout of the user
     */
    public void updateTimeout() {
        this.timeout = System.currentTimeMillis() + 30000; // 30 seconds
    }

    /**
     * Get Address of the User
     *
     * @return the Address
     */
    public String getAddress() {
        return this.channel.remoteAddress().toString().substring( 1 ).split( ":" )[0];
    }

    /**
     * Send a packet to the client
     *
     * @param packet to send
     */
    public void sendPacket( Packet packet ) {
        // Check if channel active and open
        if ( !this.channel.isActive() || !this.channel.isOpen() ) {
            Main.getInstance().getLogger().warning( "Netty/User: Sending packet to a closed channel!" );
            return;
        }
        // Send packet
        this.channel.writeAndFlush( packet );
    }
}
