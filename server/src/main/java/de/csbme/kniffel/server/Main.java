package de.csbme.kniffel.server;

import de.csbme.kniffel.server.command.*;
import de.csbme.kniffel.server.game.LobbyManager;
import de.csbme.kniffel.server.mysql.MySQL;
import de.csbme.kniffel.server.mysql.repository.SettingRepository;
import de.csbme.kniffel.server.mysql.repository.StatsRepository;
import de.csbme.kniffel.server.mysql.repository.UserRepository;
import de.csbme.kniffel.server.network.NettyServer;
import de.csbme.kniffel.server.stats.RankingManager;
import de.csbme.kniffel.server.user.UserManager;
import de.csbme.kniffel.server.util.Logger;
import de.csbme.kniffel.server.util.ServerProperties;
import de.csbme.kniffel.server.util.command.CommandHandler;
import lombok.Getter;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Getter
public class Main {

    @Getter
    private static Main instance;

    private final ExecutorService POOL = Executors.newCachedThreadPool();

    private Logger logger;

    private ServerProperties serverProperties;

    private MySQL mySQL;

    private UserRepository userRepository;

    private StatsRepository statsRepository;

    private SettingRepository settingRepository;

    private NettyServer nettyServer;

    private CommandHandler commandHandler;

    private UserManager userManager;

    private RankingManager rankingManager;

    private LobbyManager lobbyManager;

    private Main() {
        // Save instance
        instance = this;

        // Init logger
        this.logger = new Logger( new File( "logs" ) );
        this.logger.info( "Starting server..." );

        // Add shutdown hook
        Runtime.getRuntime().addShutdownHook( new Thread( () -> {
            this.logger.info( "Stopping..." );

            // Disconnect from mysql
            this.mySQL.disconnect();

            // Shutdown pool
            this.POOL.shutdown();

            this.logger.info( "Server successfully stopped!" );
        } ) );

        // Init and load properties
        this.serverProperties = new ServerProperties( new File( "server.properties" ) );

        // Init mysql connection
        this.mySQL = new MySQL( this.serverProperties );

        // Check connection
        if ( !this.mySQL.isConnected() ) {
            return;
        }

        // Init repositories
        this.userRepository = new UserRepository( this.mySQL );
        this.statsRepository = new StatsRepository( this.mySQL );
        this.settingRepository = new SettingRepository( this.mySQL );

        // Init netty server
        this.nettyServer = new NettyServer( this, this.serverProperties.getNettyPort() );
        this.nettyServer.startServer();

        // Init command handler
        this.commandHandler = new CommandHandler();

        // Register commands
        this.commandHandler.registerCommand( new HelpCommand( this ) );
        this.commandHandler.registerCommand( new StopCommand( this ) );
        this.commandHandler.registerCommand( new SettingsCommand( this ) );
        this.commandHandler.registerCommand( new StatsCommand( this ) );
        this.commandHandler.registerCommand( new UserCommand( this ) );

        // Init user & ranking manager
        this.userManager = new UserManager( this );
        this.rankingManager = new RankingManager( this );

        // Init lobby manager
        this.lobbyManager = new LobbyManager();

        // Init console input
        this.POOL.execute( () -> this.commandHandler.consoleInput( command ->
                this.logger.info( "[CommandHandler] The command '" + command + "' not exists." ) ) );
    }

    public static void main( String[] args ) {
        new Main();
    }
}
