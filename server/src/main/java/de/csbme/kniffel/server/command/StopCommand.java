package de.csbme.kniffel.server.command;

import de.csbme.kniffel.server.Main;
import de.csbme.kniffel.server.util.command.Command;

public class StopCommand extends Command {

    private Main main;

    public StopCommand( Main main ) {
        super( "stop" );
        this.getAliases().add( "shutdown" );
        this.getAliases().add( "end" );
        this.main = main;
    }

    @Override
    public void execute( String label, String[] args ) {
        this.main.getLogger().info( "Stopping server..." );
        System.exit( 0 );
    }
}
