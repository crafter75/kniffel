package de.csbme.kniffel.server.game;

import de.csbme.kniffel.lib.game.Game;
import de.csbme.kniffel.lib.game.category.Category;
import de.csbme.kniffel.lib.game.handler.AbstractGameHandler;
import de.csbme.kniffel.lib.game.player.AbstractEntity;
import de.csbme.kniffel.lib.game.player.GamePlayer;
import de.csbme.kniffel.lib.game.util.Dice;
import de.csbme.kniffel.lib.game.util.Roll;
import de.csbme.kniffel.lib.network.Packet;
import de.csbme.kniffel.lib.network.packet.game.*;
import de.csbme.kniffel.lib.user.Stats;
import de.csbme.kniffel.server.Main;
import de.csbme.kniffel.server.mysql.repository.StatsRepository;
import de.csbme.kniffel.server.user.User;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

public class ServerGameHandler extends AbstractGameHandler {

    public ServerGameHandler( Game game ) {
        super( game );
    }

    @Override
    public void handleStart( List<AbstractEntity> entities ) {
        // Send packet to all users (clients)
        this.sendPacketToAllPlayers( new GameStartPacket( this.getGame() ) );

        // Get stats repository
        StatsRepository statsRepository = Main.getInstance().getStatsRepository();

        // Iterate entities
        entities.forEach( entity -> {
            // Cast user from entity
            User user = (User) entity;

            try {
                // Manipulate or increase stats
                statsRepository.manipulateOrIncreaseStats( user.getId(), Stats.Key.GAMES, true, 1 );
                statsRepository.manipulateOrIncreaseStats( user.getId(), Stats.Key.LAST_PLAYED, false, System.currentTimeMillis() );
            } catch ( ExecutionException | InterruptedException e ) {
                Main.getInstance().getLogger().error( "Error while manipulating/increasing stats", e );
            }
        } );
    }

    @Override
    public void handleNextEntity( AbstractEntity entity ) {
        // Send packet to all users (clients)
        this.sendPacketToAllPlayers( new GameNextPlayerPacket( entity.getName() ) );
    }

    @Override
    public void handleDiceRoll( AbstractEntity entity, Roll roll ) {
        // Send packet to all users (clients)
        this.sendPacketToAllPlayers( new GameDiceRollPacket( roll.getDice(), roll.getSavedIndex(), roll.getRollsLeft() ) );
    }

    @Override
    public void handleDiceSave( AbstractEntity entity, Dice dice, int diceIndex ) {
        // Send packet to all users (clients)
        this.sendPacketToAllPlayers( new GameDiceSavePacket( dice, diceIndex ), entity );
    }

    @Override
    public void handleDiceRemove( AbstractEntity entity, Dice dice, int diceIndex ) {
        // Send packet to all users (clients)
        this.sendPacketToAllPlayers( new GameDiceRemovePacket( dice, diceIndex ), entity );
    }

    @Override
    public void handleDiceRollEnd( AbstractEntity entity, Category category, int value ) {
        // Send packet to all users (clients)
        this.sendPacketToAllPlayers( new GameDiceRollEndPacket( entity.getName(), category, value ) );
    }

    @Override
    public void handlePlayerQuit( GamePlayer player, boolean afk ) {
        // Send packet to all users (clients)
        this.sendPacketToAllPlayers( new GamePlayerQuitPacket( player.getName(), afk ) );

        // Get user from game player
        User user = (User) player;

        // Send info
        user.sendPacket( new GamePlayerQuitPacket( player.getName(), afk ) );

        // Reset game
        user.setGame( Optional.empty() );
    }

    @Override
    public void handleEnd( List<AbstractEntity> entities, long timeOfGame, boolean finish ) {
        // Send packet to all users (clients)
        this.sendPacketToAllPlayers( new GameEndPacket( entities.stream().map( AbstractEntity::getName ).collect( Collectors.toList() ), timeOfGame ) );

        // Get stats repository
        StatsRepository statsRepository = Main.getInstance().getStatsRepository();

        // Check if size greater than 0
        if ( entities.size() > 0 ) {
            try {
                // Add win for winner
                statsRepository.manipulateOrIncreaseStats( ( (User) entities.get( 0 ) ).getId(), Stats.Key.WINS, true, 1 );
            } catch ( ExecutionException | InterruptedException e ) {
                Main.getInstance().getLogger().error( "Error while manipulating/increasing stats", e );
            }
        }

        // Iterate entities
        entities.forEach( entity -> {
            // Cast user from entity
            User user = (User) entity;

            // Reset game
            user.setGame( Optional.empty() );

            try {
                // Manipulate or increase stats
                statsRepository.manipulateOrIncreaseStats( user.getId(), Stats.Key.PLAY_TIME, true, timeOfGame );
                statsRepository.manipulateOrIncreaseStats( user.getId(), Stats.Key.LAST_PLAYED, false, System.currentTimeMillis() );
                statsRepository.manipulateOrIncreaseStats( user.getId(), Stats.Key.POINTS, true, entity.getAllPoints() );
            } catch ( ExecutionException | InterruptedException e ) {
                Main.getInstance().getLogger().error( "Error while manipulating/increasing stats", e );
            }
        } );
    }

    /**
     * Send packet to all players of the game
     *
     * @param packet to send
     * @param ignore players
     */
    private void sendPacketToAllPlayers( Packet packet, AbstractEntity... ignore ) {
        // Ignore players as list
        List<AbstractEntity> entitiesIgnore = Arrays.asList( ignore );

        // Send packet to all users
        this.getGame().getEntities().stream().filter( e -> !entitiesIgnore.contains( e ) ).forEach( entity -> ( (User) entity ).sendPacket( packet ) );
    }
}
