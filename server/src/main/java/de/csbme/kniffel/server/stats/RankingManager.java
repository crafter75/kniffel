package de.csbme.kniffel.server.stats;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.kniffel.lib.network.packet.UserRankingResponsePacket;
import de.csbme.kniffel.lib.user.RankingEntry;
import de.csbme.kniffel.lib.user.Stats;
import de.csbme.kniffel.server.Main;
import lombok.Getter;

import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

public class RankingManager {

    @Getter
    private static final Stats.Key RANKING_KEY = Stats.Key.POINTS;

    private final Main main;

    private LinkedList<RankingEntry> rankingEntries = new LinkedList<>();

    public RankingManager( Main main ) {
        this.main = main;

        new Timer().scheduleAtFixedRate( new TimerTask() {
            @Override
            public void run() {
                // Load top ten
                Futures.addCallback( RankingManager.this.main.getStatsRepository().getTopTen( RANKING_KEY ), new FutureCallback<LinkedList<RankingEntry>>() {
                    @Override
                    public void onSuccess( LinkedList<RankingEntry> rankingEntries ) {
                        // Check if ranking entries exists
                        if ( rankingEntries == null ) {
                            RankingManager.this.main.getLogger().error( "Ranking: RankingEntries are null!" );
                            return;
                        }

                        // Update ranking entries
                        RankingManager.this.rankingEntries = rankingEntries;
                        RankingManager.this.main.getLogger().info( "Ranking: Loaded top ten successfully!" );

                        // Create ranking response packet
                        UserRankingResponsePacket userRankingResponsePacket = new UserRankingResponsePacket( rankingEntries );

                        // Send ranking to all online users
                        RankingManager.this.main.getUserManager().getUsers().forEach( user ->
                                user.sendPacket( userRankingResponsePacket ) );
                    }

                    @Override
                    public void onFailure( Throwable throwable ) {
                        RankingManager.this.main.getLogger().error( "Ranking: Error while loading top ten ranking!",
                                (Exception) throwable );
                    }
                }, MoreExecutors.directExecutor() );

            }
        }, 1000L, 600000L ); // delay: 1 second, period: 10 minutes
    }

    /**
     * Get the Top Ten Ranking
     *
     * @return the Ranking
     */
    public LinkedList<RankingEntry> getTopTenRanking() {
        return this.rankingEntries;
    }

    /**
     * Get the Ranking of a User
     *
     * @param id of the User
     * @return future with the Ranking number of the User
     */
    public ListenableFuture<Integer> getUserRanking( int id ) {
        return this.main.getStatsRepository().getUserRanking( id, RANKING_KEY );
    }
}
