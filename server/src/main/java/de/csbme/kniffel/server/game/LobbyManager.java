package de.csbme.kniffel.server.game;

import de.csbme.kniffel.server.user.User;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class LobbyManager {

    @Getter
    private final List<Lobby> lobbies = new ArrayList<>();

    /**
     * Create lobby
     *
     * @param name   of the lobby
     * @param leader of the lobby
     */
    public void createLobby( String name, User leader ) {
        this.lobbies.add( new Lobby( name, leader ) );
    }

    /**
     * Get Lobby from name
     *
     * @param name of the lobby
     * @return the lobby as optional
     */
    public Optional<Lobby> getLobby( String name ) {
        return this.lobbies.stream().filter( lobby -> lobby.getName().equalsIgnoreCase( name ) ).findFirst();
    }

    /**
     * Check if a user is in a lobby or has a lobby
     *
     * @param user to check
     * @return if the user has a lobby or is in a lobby
     */
    public boolean isInOrHasLobby( User user ) {
        return this.lobbies.stream().anyMatch( lobby -> lobby.getLeader().equals( user ) || lobby.getPlayers().contains( user ) );
    }
}
