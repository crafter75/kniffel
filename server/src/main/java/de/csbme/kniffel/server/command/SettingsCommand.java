package de.csbme.kniffel.server.command;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.kniffel.lib.user.Settings;
import de.csbme.kniffel.server.Main;
import de.csbme.kniffel.server.mysql.row.Row;
import de.csbme.kniffel.server.mysql.row.Rows;
import de.csbme.kniffel.server.util.Logger;
import de.csbme.kniffel.server.util.command.Command;

public class SettingsCommand extends Command {

    private Main main;

    public SettingsCommand( Main main ) {
        super( "settings" );
        this.main = main;
    }

    @Override
    public void execute( String label, String[] args ) {
        if ( args.length != 1 ) {
            this.main.getLogger().info( "Usage: /settings <ID|Username>" );
            return;
        }
        ListenableFuture<Rows> rows;
        try {
            // Get Stats by id
            int id = Integer.parseInt( args[0] );

            rows = this.main.getUserRepository().getUserRow( id );
        } catch ( NumberFormatException e ) {
            // Get Stats by name
            rows = this.main.getUserRepository().getUserRow( args[0] );
        }

        // Load user
        Futures.addCallback( rows, new FutureCallback<Rows>() {
            @Override
            public void onSuccess( Rows rows ) {
                // Check if user exists
                if ( rows == null || rows.first() == null ) {
                    SettingsCommand.this.main.getLogger().info( "User not found!" );
                    return;
                }
                // Get first row
                Row row = rows.first();
                // Get logger
                Logger logger = SettingsCommand.this.main.getLogger();
                // Get id from row
                int id = row.getInt( "id" );

                // Load settings
                Futures.addCallback( SettingsCommand.this.main.getSettingRepository().getSettings( id ), new FutureCallback<Settings>() {
                    @Override
                    public void onSuccess( Settings settings ) {
                        // Check if settings exists
                        if ( settings != null ) {
                            // Log settings
                            logger.info( "######### Settings #########" );
                            logger.info( "- ID / Name: " + id + " / " + row.getString( "name" ) );
                            logger.info( "- Einstellungen:" );
                            logger.info( "  - Animierter Hintergrund: " + settings.get( Settings.Key.ANIMATED_BACKGROUND, Boolean.class ) );
                            logger.info( "  - Sound abspielen: " + settings.get( Settings.Key.PLAY_SOUND, Boolean.class ) );
                            logger.info( "  - Theme Style: " + settings.get( Settings.Key.THEME_STYLE, Integer.class ) );
                            logger.info( "  - Hintergrund Style: " + settings.get( Settings.Key.BACKGROUND_STYLE, Integer.class ) );
                            logger.info( "  - Sound Volume: " + settings.get( Settings.Key.SOUND_VOLUME, Integer.class ) );
                            logger.info( "######### Settings #########" );
                            return;
                        }
                        logger.info( "No Settings found for the User!" );
                    }

                    @Override
                    public void onFailure( Throwable throwable ) {
                        SettingsCommand.this.main.getLogger().error( "Error while fetching Settings data", (Exception) throwable );
                    }
                }, MoreExecutors.directExecutor() );
            }

            @Override
            public void onFailure( Throwable throwable ) {
                SettingsCommand.this.main.getLogger().error( "Error while fetching User data", (Exception) throwable );
            }
        }, MoreExecutors.directExecutor() );
    }
}
