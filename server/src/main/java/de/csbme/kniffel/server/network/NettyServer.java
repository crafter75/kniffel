package de.csbme.kniffel.server.network;

import de.csbme.kniffel.lib.network.Packet;
import de.csbme.kniffel.server.Main;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;

public class NettyServer {

    private static final boolean EPOLL = Epoll.isAvailable();

    private final Main main;

    private final int port;

    public NettyServer( Main main, int port ) {
        this.main = main;
        this.port = port;
    }

    /**
     * Start netty server
     */
    public void startServer() {
        this.main.getLogger().info( "Netty: Starting server..." );

        // Start server in other thread
        this.main.getPOOL().execute( () -> {
            // Use Epoll if available
            EventLoopGroup eventLoopGroup = EPOLL ? new EpollEventLoopGroup() : new NioEventLoopGroup();

            try {
                // Create ServerBootstrap with Epoll if available and object encoder/decoder
                ServerBootstrap serverBootstrap = new ServerBootstrap()
                        .group( eventLoopGroup )
                        .channel( EPOLL ? EpollServerSocketChannel.class : NioServerSocketChannel.class )
                        .childHandler( new ChannelInitializer<SocketChannel>() {
                            @Override
                            protected void initChannel( SocketChannel socketChannel ) throws Exception {
                                socketChannel.pipeline().addLast( "encoder", new ObjectEncoder() );
                                socketChannel.pipeline().addLast( "decoder", new ObjectDecoder( Integer.MAX_VALUE, ClassResolvers.cacheDisabled( Packet.class.getClassLoader() ) ) );
                                socketChannel.pipeline().addLast( new ServerHandler( NettyServer.this.main ) );
                            }
                        } );
                // Bind server to port
                ChannelFuture channelFuture = serverBootstrap.bind( "0.0.0.0", this.port );

                // Sync server
                channelFuture.sync();

                this.main.getLogger().info( "Netty: Server on port '" + this.port + "' started!" );

                channelFuture.sync().channel().closeFuture().syncUninterruptibly();
            } catch ( Exception e ) {
                this.main.getLogger().error( "Netty: Can't start server on port '" + this.port + "'!", e );
                System.exit( 0 );
            } finally {
                eventLoopGroup.shutdownGracefully();
            }
        } );
    }
}
