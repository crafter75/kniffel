package de.csbme.kniffel.server.mysql.repository;

import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.kniffel.lib.user.Settings;
import de.csbme.kniffel.server.Main;
import de.csbme.kniffel.server.mysql.MySQL;
import de.csbme.kniffel.server.mysql.row.Rows;

import java.util.concurrent.ExecutionException;

public class UserRepository {

    private MySQL mySQL;

    public UserRepository( MySQL mySQL ) {
        this.mySQL = mySQL;

        // Create user table
        try {
            mySQL.execute( "CREATE TABLE IF NOT EXISTS kniffel_users (id INT NOT NULL AUTO_INCREMENT, " +
                    "name VARCHAR(12) NOT NULL, password CHAR(128) NOT NULL, " +
                    "registration_date BIGINT NOT NULL, " +
                    "address VARCHAR(15) DEFAULT NULL, PRIMARY KEY (id), UNIQUE (name))" ).get();
        } catch ( InterruptedException | ExecutionException e ) {
            Main.getInstance().getLogger().error( "UserRepository: Error while creating table 'kniffel_users'", e );
        }
    }

    /**
     * Insert a User
     *
     * @param name     of the user
     * @param password of the user
     * @return the generated User id. the result can be -1 if no id has been generated
     */
    public ListenableFuture<Integer> insertUserAndGetId( String name, String password ) {
        return this.mySQL.insertAndGetId( "INSERT INTO kniffel_users (name, password, registration_date) VALUES (?, ?, ?)",
                name, password, System.currentTimeMillis() );
    }

    /**
     * Get the ID of a User
     *
     * @param username of the User
     * @return future with the ID of the User
     */
    public ListenableFuture<Integer> getUserId( String username ) {
        // Get user id
        ListenableFuture<Rows> futureRows = this.mySQL.query( "SELECT id FROM kniffel_users WHERE name = ?", username );

        // Load user id
        return Futures.transform( futureRows, rows -> {
            // Check if user id found
            if ( rows != null && rows.first() != null ) {
                return rows.first().getInt( "id" );
            }
            return null;
        }, MoreExecutors.directExecutor() );
    }

    /**
     * Check password of a User
     *
     * @param username to check
     * @param password to check
     * @return future if the password is correct
     */
    public ListenableFuture<Boolean> isPasswordCorrect( String username, String password ) {
        // Get password
        ListenableFuture<Rows> futureRows = this.mySQL.query( "SELECT password FROM kniffel_users WHERE name = ?", username );

        // Load password
        return Futures.transform( futureRows, rows -> {
            // Check if password found
            if ( rows != null && rows.first() != null ) {
                return rows.first().getString( "password" ).equals( password );
            }
            return false;
        }, MoreExecutors.directExecutor() );
    }

    /**
     * Check if a User exists
     *
     * @param name to check
     * @return future if the name exists
     */
    public ListenableFuture<Boolean> existsUsername( String name ) {
        // Get username
        ListenableFuture<Rows> futureRows = this.mySQL.query( "SELECT name FROM kniffel_users WHERE name = ?", name );

        // Load username
        return Futures.transform( futureRows, rows -> {
            // Check if username exists
            if ( rows != null ) {
                return rows.first() != null;
            }
            return false;
        }, MoreExecutors.directExecutor() );
    }

    /**
     * Update address of a User
     *
     * @param userId  of the User
     * @param address to update
     * @return future if the update was successfully
     */
    public ListenableFuture<Boolean> updateAddress( int userId, String address ) {
        return this.mySQL.execute( "UPDATE kniffel_users SET address = ? WHERE id = ?", address, userId );
    }

    /**
     * Get Rows of a User
     *
     * @param id of the User
     * @return future with the raw Rows of a User
     */
    public ListenableFuture<Rows> getUserRow( int id ) {
        return this.mySQL.query( "SELECT * FROM kniffel_users WHERE id = ?", id );
    }

    /**
     * Get Rows of a User
     *
     * @param name of the User
     * @return future with the raw Rows of a User
     */
    public ListenableFuture<Rows> getUserRow( String name ) {
        return this.mySQL.query( "SELECT * FROM kniffel_users WHERE name = ?", name );
    }
}
