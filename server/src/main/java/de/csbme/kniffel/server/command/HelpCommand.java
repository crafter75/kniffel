package de.csbme.kniffel.server.command;

import de.csbme.kniffel.server.Main;
import de.csbme.kniffel.server.util.command.Command;

public class HelpCommand extends Command {

    private Main main;

    public HelpCommand( Main main ) {
        super( "help" );
        this.getAliases().add( "?" );
        this.getAliases().add( "commands" );
        this.main = main;
    }

    @Override
    public void execute( String label, String[] args ) {
        this.main.getLogger().info( "Commands:" );
        this.main.getCommandHandler().getCommands().forEach( command -> {
            if ( command.getAliases().size() > 0 ) {
                System.out.println( "- " + command.getName() + " (Aliases: " + command.getAliases().toString()
                        .replace( "[", "" ).replace( "]", "" ) + ")" );
            } else {
                System.out.println( "- " + command.getName() );
            }
        } );
    }
}
