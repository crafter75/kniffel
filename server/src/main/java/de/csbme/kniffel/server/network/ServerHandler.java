package de.csbme.kniffel.server.network;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.kniffel.lib.error.HTTPCodes;
import de.csbme.kniffel.lib.game.util.GameSettings;
import de.csbme.kniffel.lib.network.Packet;
import de.csbme.kniffel.lib.network.packet.*;
import de.csbme.kniffel.lib.network.packet.game.*;
import de.csbme.kniffel.lib.network.packet.lobby.*;
import de.csbme.kniffel.lib.user.Settings;
import de.csbme.kniffel.lib.user.Stats;
import de.csbme.kniffel.server.Main;
import de.csbme.kniffel.server.game.Lobby;
import de.csbme.kniffel.server.user.User;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.ReferenceCountUtil;

import java.util.Optional;
import java.util.concurrent.ExecutionException;

public class ServerHandler extends SimpleChannelInboundHandler<Object> {

    private final Main main;

    ServerHandler( Main main ) {
        this.main = main;
    }

    @Override
    protected void channelRead0( ChannelHandlerContext channelHandlerContext, Object object ) throws Exception {
        try {
            if ( !( object instanceof Packet ) ) {
                return;
            }
            Channel channel = channelHandlerContext.channel();
            Packet packet = (Packet) object;

            // Packet for user timeout
            if ( packet instanceof UserAlivePacket ) {
                UserAlivePacket userAlivePacket = (UserAlivePacket) packet;

                // Get user from name
                Optional<User> optionalNameUser = this.main.getUserManager().getUser( userAlivePacket.getUsername() );

                // Run only if user exists
                optionalNameUser.ifPresent( user -> {
                    // Update timeout
                    user.updateTimeout();

                    // Check if the channel are equal
                    if ( !user.getChannel().equals( channel ) ) {
                        // Replace channel
                        user.setChannel( channel );
                    }
                } );
                return;
            }

            // Packet for user registration
            if ( packet instanceof UserRegisterPacket ) {
                UserRegisterPacket userRegisterPacket = (UserRegisterPacket) packet;

                // Check if username exists
                ListenableFuture<Boolean> checkUsernameFuture = this.main.getUserRepository().existsUsername(
                        userRegisterPacket.getUsername() );
                Futures.addCallback( checkUsernameFuture, new FutureCallback<Boolean>() {
                    @Override
                    public void onSuccess( Boolean exists ) {
                        if ( exists ) {
                            // Send error to Client
                            channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_409, "Der Username wird bereits verwendet!" ) );
                            return;
                        }
                        // Username available. Insert into database
                        ServerHandler.this.main.getUserManager().registerUser( userRegisterPacket.getUsername(),
                                userRegisterPacket.getPassword(), channel );
                    }

                    @Override
                    public void onFailure( Throwable throwable ) {
                        // Send error to Client
                        channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_503, "Fehler bei der Username abfrage! Versuche es später erneut." ) );
                        ServerHandler.this.main.getLogger().error( "Error while checking username!", (Exception) throwable );
                    }
                }, MoreExecutors.directExecutor() );
                return;
            }

            // Packet for user login
            if ( packet instanceof UserLoginPacket ) {
                UserLoginPacket userLoginPacket = (UserLoginPacket) packet;

                // Check if password correct
                ListenableFuture<Boolean> checkPasswordFuture = this.main.getUserRepository().isPasswordCorrect(
                        userLoginPacket.getUsername(), userLoginPacket.getPassword() );
                Futures.addCallback( checkPasswordFuture, new FutureCallback<Boolean>() {
                    @Override
                    public void onSuccess( Boolean passwordCorrect ) {
                        if ( !passwordCorrect ) {
                            // Send error to client
                            channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_400, "Der Username oder das Passwort ist falsch!" ) );
                            return;
                        }

                        // Password correct. Log User in
                        try {
                            Integer userId = ServerHandler.this.main.getUserRepository().getUserId( userLoginPacket.getUsername() ).get();

                            if ( userId == null ) {
                                // Send error to client
                                channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_503, "User-ID konnte nicht gefunden werden! Wende dich an einen Admin!" ) );
                                return;
                            }

                            if ( ServerHandler.this.main.getUserManager().getUser( userId ).isPresent() ) {
                                // Send error to client
                                channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_400, "Du bist bereits eingeloggt!" ) );
                                return;
                            }

                            ServerHandler.this.main.getUserManager().loginUser( userId, channel );
                        } catch ( InterruptedException | ExecutionException e ) {
                            // Send error to client
                            channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_503, "Fehler beim abfragen der User-ID!" ) );
                            ServerHandler.this.main.getLogger().error( "Error while searching for User-ID!", e );
                        }
                    }

                    @Override
                    public void onFailure( Throwable throwable ) {
                        // Send error to client
                        channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_503, "Fehler beim einloggen!" ) );
                        ServerHandler.this.main.getLogger().error( "Error while login in!", (Exception) throwable );
                    }
                }, MoreExecutors.directExecutor() );
                return;
            }

            // Packet for manipulating user settings
            if ( packet instanceof UserManipulateSettingPacket ) {
                UserManipulateSettingPacket userManipulateSettingPacket = (UserManipulateSettingPacket) packet;

                // Manipulate setting
                this.main.getSettingRepository().manipulateSetting( userManipulateSettingPacket.getUserId(),
                        Settings.Key.values()[userManipulateSettingPacket.getSettingsKey()],
                        userManipulateSettingPacket.getSettingValue() );
                return;
            }

            // Packet for manipulating user stats
            if ( packet instanceof UserManipulateStatsPacket ) {
                UserManipulateStatsPacket userManipulateStatsPacket = (UserManipulateStatsPacket) packet;

                // Get stats key from packet
                Stats.Key key = Stats.Key.values()[userManipulateStatsPacket.getStatsKey()];

                // Manipulate or increase stat
                this.main.getStatsRepository().manipulateOrIncreaseStats( userManipulateStatsPacket.getUserId(), key, userManipulateStatsPacket.isIncrease(),
                        userManipulateStatsPacket.getStatsValue() );
                return;
            }

            // Packet for requesting user stats
            if ( packet instanceof UserStatsRequestPacket ) {
                UserStatsRequestPacket userStatsRequestPacket = (UserStatsRequestPacket) packet;

                // Get user
                Optional<User> userOptional = this.main.getUserManager().getUser( userStatsRequestPacket.getUsername() );

                int userId;
                // Check if user exists
                if ( userOptional.isPresent() ) {
                    // Get id from user
                    userId = userOptional.get().getId();
                } else {
                    // Get id from name
                    Integer id = this.main.getUserRepository().getUserId( userStatsRequestPacket.getUsername() ).get();

                    // Check if player found
                    if ( id == null ) {
                        channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_404, "Dieser Spieler wurde nicht gefunden." ) );
                        return;
                    }
                    userId = id;
                }

                // Get ranking from user id
                int ranking = this.main.getRankingManager().getUserRanking( userId ).get();

                // Get stats from user id
                Futures.addCallback( this.main.getStatsRepository().getStats( userId ), new FutureCallback<Stats>() {
                    @Override
                    public void onSuccess( Stats stats ) {
                        // Send stats with ranking to client
                        channel.writeAndFlush( new UserStatsResponsePacket( userStatsRequestPacket.getUsername(), stats, ranking ) );
                    }

                    @Override
                    public void onFailure( Throwable throwable ) {
                        // Send error to client
                        channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_503, "Fehler beim abrufen der Stats!" ) );
                        ServerHandler.this.main.getLogger().error( "Error while fetching stats!", (Exception) throwable );
                    }
                }, MoreExecutors.directExecutor() );
                return;
            }

            // Packet for requesting user top ten ranking
            if ( packet instanceof UserRankingRequestPacket ) {
                // Send top ten ranking to client
                channel.writeAndFlush( new UserRankingResponsePacket( this.main.getRankingManager().getTopTenRanking() ) );
                return;
            }

            // Packet for saving dice in multiplayer online game
            if ( packet instanceof GamePlayerSaveDicePacket ) {
                GamePlayerSaveDicePacket gamePlayerSaveDicePacket = (GamePlayerSaveDicePacket) packet;

                // Save dice index in game of the user
                this.main.getUserManager().getUser( gamePlayerSaveDicePacket.getName() ).ifPresent( user -> user.getGame().ifPresent( game ->
                        game.getCurrentRoll().saveDice( gamePlayerSaveDicePacket.getDiceIndex() ) ) );
                return;
            }

            // Packet for removing dice in multiplayer online game
            if ( packet instanceof GamePlayerRemoveDicePacket ) {
                GamePlayerRemoveDicePacket gamePlayerRemoveDicePacket = (GamePlayerRemoveDicePacket) packet;

                // Remove dice index from game of the user
                this.main.getUserManager().getUser( gamePlayerRemoveDicePacket.getName() ).ifPresent( user -> user.getGame().ifPresent( game ->
                        game.getCurrentRoll().removeDice( gamePlayerRemoveDicePacket.getDiceIndex() ) ) );
                return;
            }

            // Packet for roll again
            if ( packet instanceof GamePlayerRollAgainPacket ) {
                GamePlayerRollAgainPacket gamePlayerRollAgainPacket = (GamePlayerRollAgainPacket) packet;

                // Roll again
                this.main.getUserManager().getUser( gamePlayerRollAgainPacket.getName() ).ifPresent( user -> user.getGame().ifPresent( game ->
                        game.getCurrentRoll().roll() ) );
                return;
            }

            // Packet for player leaving
            if ( packet instanceof GamePlayerQuitPacket ) {
                GamePlayerQuitPacket gamePlayerQuitPacket = (GamePlayerQuitPacket) packet;

                this.main.getUserManager().getUser( gamePlayerQuitPacket.getName() ).ifPresent( user -> user.getGame().ifPresent( game -> {
                    game.removePlayer( user );

                    user.setGame( Optional.empty() );
                } ) );
                return;
            }

            // Packet for set category result in multiplayer online game
            if ( packet instanceof GamePlayerResultPacket ) {
                GamePlayerResultPacket gamePlayerResultPacket = (GamePlayerResultPacket) packet;

                // Set category result of user game
                this.main.getUserManager().getUser( gamePlayerResultPacket.getName() ).ifPresent( user -> user.getGame().ifPresent( game ->
                        game.getCurrentRoll().setResult( gamePlayerResultPacket.getCategory() ) ) );
                return;
            }

            // Packet for lobby creation
            if ( packet instanceof LobbyCreatePacket ) {
                LobbyCreatePacket lobbyCreatePacket = (LobbyCreatePacket) packet;

                // Get lobby
                Optional<Lobby> optionalLobby = this.main.getLobbyManager().getLobby( lobbyCreatePacket.getLobbyName() );

                // Check if exists
                if ( optionalLobby.isPresent() ) {
                    channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_406, "Eine Lobby mit diesem Namen existiert bererits!" ) );
                    return;
                }

                // Get user
                Optional<User> optionalUser = this.main.getUserManager().getUser( lobbyCreatePacket.getUserName() );

                // Check if exists
                if ( !optionalUser.isPresent() ) {
                    channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_404, "Du wurdest nicht auf dem Server gefunden! Bitte neu verbinden." ) );
                    return;
                }

                // Check if user already in a lobby or has a lobby
                if ( this.main.getLobbyManager().isInOrHasLobby( optionalUser.get() ) ) {
                    return;
                }

                // Create lobby
                this.main.getLobbyManager().createLobby( lobbyCreatePacket.getLobbyName(), optionalUser.get() );

                // Send packet back
                channel.writeAndFlush( lobbyCreatePacket );
                return;
            }

            // Packet for lobby deleting
            if ( packet instanceof LobbyDeletePacket ) {
                LobbyDeletePacket lobbyDeletePacket = (LobbyDeletePacket) packet;

                // Get lobby optional
                Optional<Lobby> optionalLobby = this.main.getLobbyManager().getLobby( lobbyDeletePacket.getLobbyName() );

                // Check if lobby exists
                if ( !optionalLobby.isPresent() ) return;

                // Get lobby
                Lobby lobby = optionalLobby.get();

                // Send to all players
                lobby.getPlayers().stream().filter( user -> user.getId() != lobby.getLeader().getId() ).forEach( user -> user.sendPacket( lobbyDeletePacket ) );

                // Remove lobby
                this.main.getLobbyManager().getLobbies().remove( lobby );
                return;
            }

            // Packet for lobby joining
            if ( packet instanceof LobbyJoinPacket ) {
                LobbyJoinPacket lobbyJoinPacket = (LobbyJoinPacket) packet;

                // Get user
                Optional<User> optionalUser = this.main.getUserManager().getUser( lobbyJoinPacket.getUserName() );

                // Check if exists
                if ( !optionalUser.isPresent() ) {
                    channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_404, "Du wurdest nicht auf dem Server gefunden! Bitte neu verbinden." ) );
                    return;
                }

                // Check if user already in a lobby or has a lobby
                if ( this.main.getLobbyManager().isInOrHasLobby( optionalUser.get() ) ) {
                    return;
                }

                // Get lobby
                Optional<Lobby> optionalLobby = this.main.getLobbyManager().getLobby( lobbyJoinPacket.getLobbyName() );

                // Check if exists
                if ( !optionalLobby.isPresent() ) {
                    channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_404, "Eine Lobby mit diesem Namen wurde nicht gefunden." ) );
                    return;
                }

                // Check if players lower than max players of a game
                if ( optionalLobby.get().getPlayers().size() >= GameSettings.MAX_PLAYERS ) {
                    channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_403, "Diese Lobby ist bereits voll." ) );
                    return;
                }

                // Add user to lobby
                optionalLobby.get().addUser( optionalUser.get() );
                return;
            }

            // Packet for lobby quitting
            if ( packet instanceof LobbyQuitPacket ) {
                LobbyQuitPacket lobbyQuitPacket = (LobbyQuitPacket) packet;

                // Get user
                Optional<User> optionalUser = this.main.getUserManager().getUser( lobbyQuitPacket.getUserName() );

                // Check if exists
                if ( !optionalUser.isPresent() ) {
                    channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_404, "Du wurdest nicht auf dem Server gefunden! Bitte neu verbinden." ) );
                    return;
                }

                // Get lobby
                Optional<Lobby> optionalLobby = this.main.getLobbyManager().getLobby( lobbyQuitPacket.getLobbyName() );

                // Check if exists
                if ( !optionalLobby.isPresent() ) {
                    channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_404, "Eine Lobby mit diesem Namen wurde nicht gefunden." ) );
                    return;
                }

                // Remove user from lobby
                optionalLobby.get().removeUser( optionalUser.get() );
                return;
            }

            // Packet for lobby starting
            if ( packet instanceof LobbyStartPacket ) {
                LobbyStartPacket lobbyStartPacket = (LobbyStartPacket) packet;

                // Get lobby
                Optional<Lobby> optionalLobby = this.main.getLobbyManager().getLobby( lobbyStartPacket.getLobbyName() );

                // Check if exists
                if ( !optionalLobby.isPresent() ) {
                    channel.writeAndFlush( new ErrorPacket( HTTPCodes.ERROR_404, "Eine Lobby mit diesem Namen wurde nicht gefunden." ) );
                    return;
                }

                // Start lobby
                optionalLobby.get().start();
                return;
            }
        } catch ( Exception e ) {
            Main.getInstance().getLogger().error( "Error while reading packet!", e );
        } finally {
            ReferenceCountUtil.release( object );
        }
    }

    @Override
    public void channelActive( ChannelHandlerContext ctx ) throws Exception {
        // Get address
        String address = ctx.channel().remoteAddress().toString().substring( 1 );

        this.main.getLogger().info( "Netty/User: '" + address + "' connected to the server!" );
    }

    @Override
    public void channelInactive( ChannelHandlerContext ctx ) throws Exception {
        // Get address
        String address = ctx.channel().remoteAddress().toString().substring( 1 );

        // Get User
        Optional<User> optionalUser = Main.getInstance().getUserManager().getUser( ctx.channel() );

        // Check if exists
        if ( !optionalUser.isPresent() ) {
            this.main.getLogger().info( "Netty/User: '" + address + "' disconnected from the server!" );
            return;
        }

        // Log out
        this.main.getUserManager().logOutUser( optionalUser.get() );
    }

    @Override
    public void exceptionCaught( ChannelHandlerContext ctx, Throwable cause ) throws Exception {
        // Only for test
        this.main.getLogger().error( "Netty-Error", (Exception) cause );
    }
}
