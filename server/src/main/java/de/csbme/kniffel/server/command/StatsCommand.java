package de.csbme.kniffel.server.command;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.MoreExecutors;
import de.csbme.kniffel.lib.time.TimeUtil;
import de.csbme.kniffel.lib.user.Stats;
import de.csbme.kniffel.server.Main;
import de.csbme.kniffel.server.mysql.row.Row;
import de.csbme.kniffel.server.mysql.row.Rows;
import de.csbme.kniffel.server.util.Logger;
import de.csbme.kniffel.server.util.command.Command;

public class StatsCommand extends Command {

    private Main main;

    public StatsCommand( Main main ) {
        super( "stats" );
        this.main = main;
    }

    @Override
    public void execute( String label, String[] args ) {
        if ( args.length != 1 ) {
            this.main.getLogger().info( "Usage: /stats <ID|Username>" );
            return;
        }
        ListenableFuture<Rows> rows;
        try {
            // Get Stats by id
            int id = Integer.parseInt( args[0] );

            rows = this.main.getUserRepository().getUserRow( id );
        } catch ( NumberFormatException e ) {
            // Get Stats by name
            rows = this.main.getUserRepository().getUserRow( args[0] );
        }

        Futures.addCallback( rows, new FutureCallback<Rows>() {
            @Override
            public void onSuccess( Rows rows ) {
                if ( rows == null || rows.first() == null ) {
                    StatsCommand.this.main.getLogger().info( "User not found!" );
                    return;
                }
                Row row = rows.first();
                Logger logger = StatsCommand.this.main.getLogger();
                int id = row.getInt( "id" );

                Futures.addCallback( StatsCommand.this.main.getStatsRepository().getStats( id ), new FutureCallback<Stats>() {
                    @Override
                    public void onSuccess( Stats stats ) {
                        if ( stats != null ) {
                            logger.info( "######### Stats #########" );
                            logger.info( "- ID / Name: " + id + " / " + row.getString( "name" ) );
                            logger.info( "- Statistiken:" );
                            logger.info( "  - Punkte: " + stats.get( Stats.Key.POINTS, Integer.class ) );
                            logger.info( "  - Höchste Punkte im Spiel: " + stats.get( Stats.Key.HIGHEST_POINTS_PER_GAME, Integer.class ) );
                            logger.info( "  - Gespielte Spiele: " + stats.get( Stats.Key.GAMES, Integer.class ) );
                            logger.info( "  - Gewonnene Spiele: " + stats.get( Stats.Key.WINS, Integer.class ) );
                            logger.info( "  - Zuletzt gespielt: " + TimeUtil.getDateFromMilliseconds(
                                    stats.get( Stats.Key.LAST_PLAYED, Long.class ), true ) );
                            logger.info( "  - Spielzeit: " + TimeUtil.getDurationFromMilliseconds(
                                    stats.get( Stats.Key.PLAY_TIME, Long.class ) ) );
                            logger.info( "######### Stats #########" );
                            return;
                        }
                        logger.info( "No Stats found for the User!" );
                    }

                    @Override
                    public void onFailure( Throwable throwable ) {
                        StatsCommand.this.main.getLogger().error( "Error while fetching Stats data", (Exception) throwable );
                    }
                }, MoreExecutors.directExecutor() );
            }

            @Override
            public void onFailure( Throwable throwable ) {
                StatsCommand.this.main.getLogger().error( "Error while fetching User data", (Exception) throwable );
            }
        }, MoreExecutors.directExecutor() );
    }
}
