package de.csbme.kniffel.lib.network.packet;

import de.csbme.kniffel.lib.network.Packet;
import de.csbme.kniffel.lib.user.RankingEntry;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.LinkedList;

@Getter
@AllArgsConstructor
public class UserRankingResponsePacket extends Packet {

    private LinkedList<RankingEntry> rankingEntries;
}
