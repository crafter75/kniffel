package de.csbme.kniffel.lib.network.packet.game;

import de.csbme.kniffel.lib.network.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class GamePlayerRollAgainPacket extends Packet {

    private String name;
}
