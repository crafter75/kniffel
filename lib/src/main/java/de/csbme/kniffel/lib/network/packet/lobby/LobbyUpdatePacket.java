package de.csbme.kniffel.lib.network.packet.lobby;

import de.csbme.kniffel.lib.network.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class LobbyUpdatePacket extends Packet {

    private String lobbyName;

    private String leader;

    private List<String> players;
}
