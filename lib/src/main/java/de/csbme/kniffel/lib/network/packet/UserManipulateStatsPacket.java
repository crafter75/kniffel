package de.csbme.kniffel.lib.network.packet;

import de.csbme.kniffel.lib.network.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserManipulateStatsPacket extends Packet {

    private int userId, statsKey;

    private boolean increase;

    private Object statsValue;
}
