package de.csbme.kniffel.lib.game.util;

import de.csbme.kniffel.lib.game.category.Category;
import de.csbme.kniffel.lib.game.player.AbstractEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public final class TableCategory {

    private String name;

    /**
     * Get the points of the table category
     *
     * @param entity to get the points
     * @return the value. if the field is not selected return an empty string
     */
    public String getPoints( AbstractEntity entity ) {
        switch ( this.name.toLowerCase() ) {
            case "gesamt":
                // Get upper block points
                int upperBlock = entity.getUpperBlockPoints();

                // Check if higher than 0
                if ( upperBlock > 0 ) {
                    return String.valueOf( upperBlock );
                }
                return "";
            case "bonus":
                // Check if bonus reached
                if ( entity.hasReachedBonusPoints() ) {
                    return String.valueOf( GameSettings.BONUS_POINTS );
                }
                return "";
            case "endsumme":
                // Get all points
                int points = entity.getAllPoints();

                // Check if higher than 0
                if ( points > 0 ) {
                    return String.valueOf( points );
                }
                return "";
            default:
                // Get category from name if exists
                return Category.fromName( this.name ).map( category -> {
                    // Check if category set
                    if ( entity.hasCategory( category ) ) {
                        return String.valueOf( entity.getPoints( category ) );
                    }
                    return "";
                } ).orElse( "" );
        }
    }
}
