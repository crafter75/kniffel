package de.csbme.kniffel.lib.network.packet.game;

import de.csbme.kniffel.lib.game.category.Category;
import de.csbme.kniffel.lib.network.Packet;
import lombok.Getter;

public class GameDiceRollEndPacket extends Packet {

    @Getter
    private String name;

    private int category;

    @Getter
    private int points;

    public GameDiceRollEndPacket( String name, Category category, int points ) {
        this.name = name;
        this.category = category != null ? category.ordinal() : -1;
        this.points = points;
    }

    public Category getCategory() {
        if ( this.category == -1 ) {
            return null;
        }
        return Category.values()[this.category];
    }
}
