package de.csbme.kniffel.lib.game.category;

import de.csbme.kniffel.lib.game.util.Dice;

import java.util.List;
import java.util.function.Function;

final class CategoryResults {

    static final Function<List<Dice>, Integer> FULL_HOUSE = dice -> 25, SMALL_STREET = dice -> 30,
            BIG_STREET = dice -> 40, KNIFFEL = dice -> 50;

    /**
     * Summarize every dice
     *
     * @return the summarize result in a Function
     */
    static Function<List<Dice>, Integer> summarizeEveryDice() {
        return dice -> dice.stream().mapToInt( Dice::getValue ).sum();
    }

    /**
     * Summarize specific dice
     *
     * @return the summarize result in a Function
     */
    static Function<List<Dice>, Integer> summarizeDice( Dice die ) {
        return dice -> dice.stream().filter( d -> d.equals( die ) ).mapToInt( Dice::getValue ).sum();
    }
}
