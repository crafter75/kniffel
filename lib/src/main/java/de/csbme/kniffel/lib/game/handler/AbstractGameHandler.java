package de.csbme.kniffel.lib.game.handler;

import de.csbme.kniffel.lib.game.Game;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public abstract class AbstractGameHandler implements GameHandler {

    private Game game;
}
