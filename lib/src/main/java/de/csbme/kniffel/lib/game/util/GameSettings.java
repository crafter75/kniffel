package de.csbme.kniffel.lib.game.util;

public final class GameSettings {

    public static final int DICE_AMOUNT = 5;

    public static final int MIN_PLAYERS = 2;

    public static final int MAX_PLAYERS = 8;

    public static final long AFK_TIMEOUT = 120000L; // 2 minutes

    public static final int UPPER_POINTS_FOR_BONUS = 63;

    public static final int BONUS_POINTS = 35;
}
