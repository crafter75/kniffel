package de.csbme.kniffel.lib.game.handler;

import de.csbme.kniffel.lib.game.category.Category;
import de.csbme.kniffel.lib.game.player.AbstractEntity;
import de.csbme.kniffel.lib.game.player.GamePlayer;
import de.csbme.kniffel.lib.game.util.Dice;
import de.csbme.kniffel.lib.game.util.Roll;

import java.util.List;
import java.util.Optional;

public interface GameHandler {

    /**
     * Called when the game starts
     *
     * @param entities to handle
     */
    void handleStart( List<AbstractEntity> entities );

    /**
     * Called when an entity is the next in the line
     *
     * @param entity to handle
     */
    void handleNextEntity( AbstractEntity entity );

    /**
     * Called when an entity has rolled the dice
     *
     * @param entity of the roll
     * @param roll   of the entity
     */
    void handleDiceRoll( AbstractEntity entity, Roll roll );

    /**
     * Called when an entity has saved a dice
     *
     * @param entity    of the roll
     * @param dice      to save
     * @param diceIndex of the dice to save
     */
    void handleDiceSave( AbstractEntity entity, Dice dice, int diceIndex );

    /**
     * Called when an entity has removed a dice from save list
     *
     * @param entity    of the roll
     * @param dice      to save
     * @param diceIndex of the dice to save
     */
    void handleDiceRemove( AbstractEntity entity, Dice dice, int diceIndex );

    /**
     * Called when an entity end the roll with a category and value
     *
     * @param entity   to set the category
     * @param category to set
     * @param value    of the category
     */
    void handleDiceRollEnd( AbstractEntity entity, Category category, int value );

    /**
     * Called when a player left the game
     *
     * @param player where left the game
     * @param afk    reason of the kick?
     */
    void handlePlayerQuit( GamePlayer player, boolean afk );

    /**
     * Called when the game is over
     *
     * @param entities of the game. the entities are sorted to the highest points
     * @param timeOfGame to display
     * @param finish game
     */
    void handleEnd( List<AbstractEntity> entities, long timeOfGame, boolean finish );
}
