package de.csbme.kniffel.lib.game.category;

import de.csbme.kniffel.lib.game.util.Dice;
import lombok.Getter;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

public enum Category {

    ONES( "Einser", CategoryRules.hasAtLeastOne( Dice.ONE ), CategoryResults.summarizeDice( Dice.ONE ) ),

    TWOS( "Zweier", CategoryRules.hasAtLeastOne( Dice.TWO ), CategoryResults.summarizeDice( Dice.TWO ) ),

    THREES( "Dreier", CategoryRules.hasAtLeastOne( Dice.THREE ), CategoryResults.summarizeDice( Dice.THREE ) ),

    FOURS( "Vierer", CategoryRules.hasAtLeastOne( Dice.FOUR ), CategoryResults.summarizeDice( Dice.FOUR ) ),

    FIVES( "Fünfer", CategoryRules.hasAtLeastOne( Dice.FIVE ), CategoryResults.summarizeDice( Dice.FIVE ) ),

    SIXES( "Sechser", CategoryRules.hasAtLeastOne( Dice.SIX ), CategoryResults.summarizeDice( Dice.SIX ) ),

    THREE_OF_A_KIND( "Dreierpasch", CategoryRules.oneDieOccursAtLeastXTimes( 3 ), CategoryResults.summarizeEveryDice() ),

    FOUR_OF_A_KIND( "Viererpasch", CategoryRules.oneDieOccursAtLeastXTimes( 4 ), CategoryResults.summarizeEveryDice() ),

    FULL_HOUSE( "Full House", CategoryRules.oneDieOccursAtLeastXTimes( 3 ).and( CategoryRules.anotherDieOccursAtLeastXTimes( 2 ) ),
            CategoryResults.FULL_HOUSE ),

    SMALL_STREET( "Kleine Straße", CategoryRules.diceInARow( 4 ), CategoryResults.SMALL_STREET ),

    BIG_STREET( "Große Straße", CategoryRules.diceInARow( 5 ), CategoryResults.BIG_STREET ),

    KNIFFEL( "Kniffel", CategoryRules.allDiceAreEqual(), CategoryResults.KNIFFEL ),

    CHANCE( "Chance", CategoryRules.ALLOW_EVERY_DICE, CategoryResults.summarizeEveryDice() );

    @Getter
    private String name;

    private Predicate<List<Dice>> rule;

    private Function<List<Dice>, Integer> result;

    Category( String name, Predicate<List<Dice>> rule, Function<List<Dice>, Integer> result ) {
        this.name = name;
        this.rule = rule;
        this.result = result;
    }

    /**
     * Check if the dice are possible for this category
     *
     * @param dice to check
     * @return if the dice are possible for this category
     */
    public boolean isPossibleCategoryFor( List<Dice> dice ) {
        return this.rule.test( dice );
    }

    /**
     * Get the points of this category for the dice list
     *
     * @param dice to calculate the points
     * @return the points of the dice list. the result can be 0 if the category is not possible
     */
    public int getPoints( List<Dice> dice ) {
        // Check if category possible for dices
        if ( !this.isPossibleCategoryFor( dice ) ) {
            return 0;
        }
        // Yes return points
        return this.result.apply( dice );
    }

    /**
     * Get Category by name
     *
     * @param name of the Category
     * @return the Category optional
     */
    public static Optional<Category> fromName( String name ) {
        return Arrays.stream(Category.values()).filter( category -> category.getName().equalsIgnoreCase( name ) ).findFirst();
    }
}
