package de.csbme.kniffel.lib.network.packet.lobby;

import de.csbme.kniffel.lib.network.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class LobbyCreatePacket extends Packet {

    private String lobbyName, userName;
}
