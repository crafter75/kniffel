package de.csbme.kniffel.lib.game;

import de.csbme.kniffel.lib.game.player.AbstractEntity;
import lombok.Getter;

import java.security.SecureRandom;
import java.util.*;

public final class GameManager {

    @Getter
    private static final GameManager instance = new GameManager();

    @Getter
    private final List<Game> games = new ArrayList<>();

    @Getter
    private final List<String> botNames = new ArrayList<>();

    private final SecureRandom random = new SecureRandom();

    private GameManager() {
        // Clear bot names
        this.botNames.clear();

        try {
            // Read bot names
            Scanner scanner = new Scanner( Objects.requireNonNull( this.getClass().getClassLoader().getResourceAsStream(
                    "txt/bot-names.txt" ) ) );

            // Check if next bot name exists
            while ( scanner.hasNext() ) {
                // Add bot name to list
                this.botNames.add( scanner.next() );
            }

            // Close scanner
            scanner.close();
        } catch ( Exception e ) {
            e.printStackTrace();
        }
    }

    /**
     * Get Game by player name
     *
     * @param playerName of the player
     * @return the Game as optional
     */
    public Optional<Game> getGameByPlayerName( String playerName ) {
        return this.games.stream().filter( game -> game.getEntities().stream()
                .anyMatch( e -> !e.isBot() && e.getName().equalsIgnoreCase( playerName ) ) ).findFirst();
    }

    /**
     * Get Game by entity
     *
     * @param entity who plays the game
     * @return the Game as optional
     */
    public Optional<Game> getGameByEntity( AbstractEntity entity ) {
        return this.games.stream().filter( game -> game.getEntities().contains( entity ) ).findFirst();
    }

    /**
     * Create a new game
     *
     * @return the game
     */
    public Game createGame( Game.Type type ) {
        Game game = new Game( this, this.getRandomGameId(), type );
        this.games.add( game );
        return game;
    }

    /**
     * Remove a game from the list
     *
     * @param game to remove
     */
    public void removeGame( Game game ) {
        games.remove( game );
    }

    /**
     * Create a game id that not used in other games
     *
     * @return a game id that is not used
     */
    private UUID getRandomGameId() {
        while ( true ) {
            // Get random id
            UUID id = UUID.randomUUID();

            // Check if game id already exists
            if ( this.games.stream().anyMatch( game -> game.getGameId().equals( id ) ) ) {
                // Yes continue loop
                continue;
            }
            // No return game id
            return id;
        }
    }

    /**
     * Get random bot name
     *
     * @return the name
     */
    public String getRandomBotName() {
        return this.botNames.get( this.random.nextInt( this.botNames.size() ) );
    }
}
