package de.csbme.kniffel.lib.network.packet.game;

import de.csbme.kniffel.lib.game.util.Dice;
import de.csbme.kniffel.lib.network.Packet;
import lombok.Getter;

public class GamePlayerSaveDicePacket extends Packet {

    @Getter
    private String name;

    private int dice;

    @Getter
    private int diceIndex;

    public GamePlayerSaveDicePacket( String name, Dice dice, int diceIndex ) {
        this.name = name;
        this.dice = dice.ordinal();
        this.diceIndex = diceIndex;
    }

    public Dice getDice() {
        return Dice.values()[this.dice];
    }
}
