package de.csbme.kniffel.lib.user;

import lombok.Getter;

import java.util.Arrays;
import java.util.Optional;

public class Stats extends AbstractData<Stats.Key> {

    @Override
    public <T> T get( Key key, Class<T> clazz ) {
        // Check if value can be cast to class
        if ( !key.getClassValue().getName().equals( clazz.getName() ) ) {
            throw new ClassCastException( "Can't cast " + key.getClassValue().getSimpleName() + " to " + clazz.getSimpleName() );
        }
        // Cast value to class
        return clazz.cast( this.getValues().getOrDefault( key.ordinal(), key.getDefaultValue() ) );
    }

    @Override
    public <V> void manipulate( Key key, V value ) {
        // Check if value is from class
        if ( !key.getClassValue().isInstance( value ) ) {
            throw new IllegalArgumentException( "Can't convert " + value.getClass().getSimpleName() + " to " + key.getClassValue().getSimpleName() );
        }
        // Store key with value
        this.getValues().put( key.ordinal(), value );
    }

    public enum Key {
        POINTS( "Gesamte Punktzahl", Integer.class, 0 ),
        HIGHEST_POINTS_PER_GAME( "Höchste Punktzahl pro Spiel", Integer.class, 0 ),
        GAMES( "Gespielte Spiele", Integer.class, 0 ),
        WINS( "Gewonnene Spiele", Integer.class, 0 ),
        LAST_PLAYED( "Zuletzt gespielt", Long.class, 0L ),
        PLAY_TIME( "Gespielte Zeit", Long.class, 0L );

        @Getter
        private String displayName;

        @Getter
        private Class<?> classValue;

        @Getter
        private Object defaultValue;

        Key( String displayName, Class<?> classValue, Object defaultValue ) {
            this.displayName = displayName;
            this.classValue = classValue;
            this.defaultValue = defaultValue;
        }

        @Override
        public String toString() {
            return this.name().toLowerCase();
        }

        public static Optional<Key> fromDisplayName( String displayName ) {
            return Arrays.stream( Key.values() ).filter( k -> k.getDisplayName().equalsIgnoreCase( displayName ) ).findFirst();
        }
    }
}
