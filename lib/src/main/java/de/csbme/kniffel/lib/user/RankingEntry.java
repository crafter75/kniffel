package de.csbme.kniffel.lib.user;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public class RankingEntry implements Serializable {

    private String username;

    private Stats stats;
}
