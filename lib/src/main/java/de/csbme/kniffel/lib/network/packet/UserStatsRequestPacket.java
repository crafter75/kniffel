package de.csbme.kniffel.lib.network.packet;

import de.csbme.kniffel.lib.network.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserStatsRequestPacket extends Packet {

    private String username;
}
