package de.csbme.kniffel.lib.game.util;

import de.csbme.kniffel.lib.game.Game;
import de.csbme.kniffel.lib.game.category.Category;
import de.csbme.kniffel.lib.game.player.AbstractEntity;
import lombok.Getter;
import lombok.Setter;

import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class Roll {

    @Getter
    private final Game game;

    private final AbstractEntity entity;

    private final SecureRandom random;

    @Getter
    private final long rollStartTime;

    @Getter
    @Setter
    private List<Dice> dice;

    @Getter
    @Setter
    private List<Integer> savedIndex;

    @Getter
    @Setter
    private int rollsLeft = 3;

    private Optional<Category> result;

    public Roll( Game game, AbstractEntity entity ) {
        this.game = game;
        this.entity = entity;
        this.random = new SecureRandom();
        this.random.setSeed( System.nanoTime() );
        this.rollStartTime = System.currentTimeMillis();
        this.dice = new ArrayList<>( GameSettings.DICE_AMOUNT );
        this.savedIndex = new ArrayList<>( GameSettings.DICE_AMOUNT );
        this.result = Optional.empty();
    }

    /**
     * Save a specific dice with the index
     *
     * @param index of the dice
     */
    public void saveDice( int index ) {
        // Check if index exists
        if ( index < 0 || index >= this.dice.size() ) {
            throw new IndexOutOfBoundsException( "Dice index out of bounds! (" + index + ")" );
        }

        // Check if already saved
        if ( this.savedIndex.contains( index ) ) return;

        // Add index
        this.savedIndex.add( index );

        // Call handler
        this.game.callGameHander( handler -> handler.handleDiceSave( this.entity, this.dice.get( index ), index ) );
    }

    /**
     * Remove specific dice with the index
     *
     * @param index of the dice to remove
     */
    public void removeDice( int index ) {
        // Check if index exists
        if ( index < 0 || index >= this.dice.size() ) {
            throw new IndexOutOfBoundsException( "Dice index out of bounds! (" + index + ")" );
        }

        // Check if index exists
        if ( !this.savedIndex.contains( index ) ) return;

        // Remove index
        this.savedIndex.remove( (Integer) index );

        // Call handler
        this.game.callGameHander( handler -> handler.handleDiceRemove( this.entity, this.dice.get( index ), index ) );
    }

    /**
     * Roll the dice again
     */
    public void roll() {
        // Check if roll available
        if ( this.rollsLeft == 0 ) {
            throw new UnsupportedOperationException( "Can't roll again!" );
        }

        // Decrement rolls
        this.rollsLeft--;

        // Iterate not saved dice index
        IntStream.range( 0, GameSettings.DICE_AMOUNT ).filter( i -> !this.savedIndex.contains( i ) ).forEach( i -> {
            // Get random dice
            Dice dice = this.getRandomDice();

            // Check if index exists in list
            if ( i >= this.dice.size() ) {
                // No, add to list
                this.dice.add( dice );
            } else {
                // Yes, replace index with new dice
                this.dice.set( i, dice );
            }
        } );

        // Call handler
        this.game.callGameHander( handler -> handler.handleDiceRoll( this.entity, this ) );
    }

    /**
     * Get all possible categories for current dice list
     *
     * @return the possioble categories as set
     */
    public Set<Category> getPossibleCategories() {
        return Arrays.stream( Category.values() ).filter( category ->
                !this.entity.hasCategory( category ) && category.isPossibleCategoryFor( this.dice ) ).collect( Collectors.toSet() );
    }

    /**
     * Set a new result
     *
     * @param result to set
     */
    public synchronized void setResult( Category result ) {
        // Set result
        this.result = Optional.ofNullable( result );
    }

    /**
     * Get the category as optional
     *
     * @return the result as optional
     */
    public synchronized Optional<Category> getResult() {
        return this.result;
    }

    /**
     * Roll the dice and generate random dice
     *
     * @return a random dice
     */
    private Dice getRandomDice() {
        return Dice.values()[this.random.nextInt( Dice.values().length )];
    }
}
