package de.csbme.kniffel.lib.network.packet;

import de.csbme.kniffel.lib.network.Packet;
import de.csbme.kniffel.lib.user.Settings;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserLoginCallbackPacket extends Packet {

    private int id;

    private String username;

    private long registrationDate;

    private Settings settings;
}
