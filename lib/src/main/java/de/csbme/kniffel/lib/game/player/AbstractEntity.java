package de.csbme.kniffel.lib.game.player;

import de.csbme.kniffel.lib.game.category.Category;
import de.csbme.kniffel.lib.game.util.GameSettings;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
public abstract class AbstractEntity {

    private final String name;

    private final boolean bot;

    private final Map<Category, Integer> points;

    AbstractEntity( String name, boolean bot ) {
        this.name = name;
        this.bot = bot;
        this.points = new HashMap<>();
    }

    /**
     * Check if a category exists
     *
     * @param category to check
     * @return if the category exists
     */
    public boolean hasCategory( Category category ) {
        return this.points.containsKey( category );
    }

    /**
     * Get the points of a category
     *
     * @param category of the points
     * @return the points. the result can be 0 if the category is not set
     */
    public int getPoints( Category category ) {
        return this.points.getOrDefault( category, 0 );
    }

    /**
     * Set points to a category
     *
     * @param category to set
     * @param points   to set
     */
    public void setPoints( Category category, int points ) {
        // Check if category already set
        if ( this.hasCategory( category ) ) {
            return;
        }
        // Set category with points
        this.points.put( category, points );
    }

    /**
     * Get available categories
     *
     * @return all available categories for use
     */
    public List<Category> getAvailableCategories() {
        return Arrays.stream( Category.values() ).filter( category -> !this.hasCategory( category ) ).collect( Collectors.toList() );
    }

    /**
     * Get all points from the upper block ( ones - sixes )
     *
     * @return the points
     */
    public int getUpperBlockPoints() {
        return Arrays.stream( Category.values(), 0, 6 ).mapToInt( this::getPoints ).sum();
    }

    /**
     * Check if the bonus points for the upper block reached
     *
     * @return if the entity has reached the bonus points
     */
    public boolean hasReachedBonusPoints() {
        return this.getUpperBlockPoints() >= GameSettings.UPPER_POINTS_FOR_BONUS;
    }

    /**
     * Get all points
     *
     * @return all points of the player
     */
    public int getAllPoints() {
        // Count points
        int points = this.points.values().stream().mapToInt( Integer::intValue ).sum();

        // Check if bonus reached
        if ( this.hasReachedBonusPoints() ) {
            // Add bonus points
            points += GameSettings.BONUS_POINTS;
        }
        return points;
    }
}
