package de.csbme.kniffel.lib.network.packet.game;

import de.csbme.kniffel.lib.network.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class GameEndPacket extends Packet {

    private List<String> players;

    private long timeOfGame;
}
