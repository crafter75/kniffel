package de.csbme.kniffel.lib.network.packet.game;

import de.csbme.kniffel.lib.game.category.Category;
import de.csbme.kniffel.lib.network.Packet;
import lombok.Getter;

public class GamePlayerResultPacket extends Packet {

    @Getter
    private String name;

    private int category;

    public GamePlayerResultPacket( String name, Category category ) {
        this.name = name;
        this.category = category.ordinal();
    }

    public Category getCategory() {
        return Category.values()[this.category];
    }
}
