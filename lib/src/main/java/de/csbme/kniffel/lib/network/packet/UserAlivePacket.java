package de.csbme.kniffel.lib.network.packet;

import de.csbme.kniffel.lib.network.Packet;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserAlivePacket extends Packet {

    private String username;
}
