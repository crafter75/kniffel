package de.csbme.kniffel.lib.game;

import de.csbme.kniffel.lib.game.category.Category;
import de.csbme.kniffel.lib.game.handler.GameHandler;
import de.csbme.kniffel.lib.game.player.AbstractEntity;
import de.csbme.kniffel.lib.game.player.GameBot;
import de.csbme.kniffel.lib.game.player.GamePlayer;
import de.csbme.kniffel.lib.game.util.GameSettings;
import de.csbme.kniffel.lib.game.util.Roll;
import javafx.application.Platform;
import lombok.Getter;
import lombok.Setter;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Getter
public final class Game {

    private final GameManager gameManager;

    private final UUID gameId;

    private final Type type;

    private final List<GameHandler> gameHandlers;

    private final List<AbstractEntity> entities;

    private boolean running;

    private long gameStart;

    @Setter
    private int currentEntityIndex;

    @Setter
    private Roll currentRoll;

    private boolean waitForResult;

    public Game( GameManager gameManager, UUID gameId, Type type ) {
        this.gameManager = gameManager;
        this.gameId = gameId;
        this.type = type;
        this.running = false;
        this.gameHandlers = new ArrayList<>();
        this.entities = new LinkedList<>();
        this.currentEntityIndex = 0;
        this.waitForResult = false;
    }

    /**
     * Add players to the game
     *
     * @param players to add
     * @return this game
     */
    public Game withPlayers( List<GamePlayer> players ) {
        // Check if game running
        if ( this.running ) {
            throw new IllegalStateException( "Game already running! Can't add players!" );
        }

        // Add players to game
        this.entities.addAll( players );
        return this;
    }

    /**
     * Add players to the game
     *
     * @param players to add
     * @return this game
     */
    public Game withPlayers( GamePlayer... players ) {
        return this.withPlayers( Arrays.asList( players ) );
    }

    /**
     * Add bots to the game
     *
     * @param amount of bots
     * @return this game
     */
    public Game withBots( int amount ) {
        // Check if game running
        if ( this.running ) {
            throw new IllegalStateException( "Game already running! Can't add bots!" );
        }

        // Add bots to game
        for ( int i = 0; i < amount; i++ ) {
            while ( true ) {
                String name = this.gameManager.getRandomBotName();

                if ( IntStream.range( 0, this.entities.size() ).filter( id -> this.entities.get( id ).isBot() ).boxed()
                        .map( id -> this.entities.get( id ).getName() ).anyMatch( otherName -> otherName.equalsIgnoreCase( name ) ) ) {
                    continue;
                }
                this.entities.add( new GameBot( name + " (Bot)" ) );
                break;
            }
        }
        return this;
    }

    /**
     * Add game handler to the game
     *
     * @param handlers to add
     * @return this game
     */
    public Game withHandler( GameHandler... handlers ) {
        this.gameHandlers.addAll( Arrays.asList( handlers ) );
        return this;
    }

    /**
     * Start the game
     */
    public void start() {
        new Thread( () -> {
            // Check if the new size is not outside the size of the settings
            if ( this.entities.size() < GameSettings.MIN_PLAYERS || this.entities.size() > GameSettings.MAX_PLAYERS ) {
                throw new IllegalArgumentException( "Too many players! Can't start game! (min: " + GameSettings.MIN_PLAYERS + ", max: " + GameSettings.MAX_PLAYERS + ")" );
            }

            // Check if at least one game handler exists
            if ( this.gameHandlers.isEmpty() ) {
                throw new IllegalArgumentException( "No game handler found! Can't start game!" );
            }
            this.running = true;
            this.gameStart = System.currentTimeMillis();

            this.callGameHander( handler -> handler.handleStart( this.entities ) );

            while ( true ) {
                // Check if only one entity left
                if ( this.entities.size() <= 1 ) {
                    this.stop( true );
                    return;
                }
                // Get current entity
                AbstractEntity entity = this.getCurrentEntity();

                // Call handler
                this.callGameHander( handler -> handler.handleNextEntity( entity ) );

                // Create new roll
                this.currentRoll = new Roll( this, entity );
                this.currentRoll.roll();

                if ( entity.isBot() ) {
                    ( (GameBot) entity ).calculate( this.currentRoll );
                }

                // Wait until category result available and game running
                while ( !this.currentRoll.getResult().isPresent() && this.running && this.entities.contains( entity ) ) {
                    // Wait (fix cpu problem)
                    try {
                        Thread.sleep( 500L );
                    } catch ( InterruptedException e ) {
                        // Stop game on error
                        this.stop( false );
                    }
                    // Check if player afk
                    if ( ( this.currentRoll.getRollStartTime() + GameSettings.AFK_TIMEOUT ) < System.currentTimeMillis() ) {
                        // Call handler
                        this.callGameHander( handler -> handler.handleDiceRollEnd( entity, null, -1 ) );

                        // Remove entity
                        this.entities.remove( this.currentEntityIndex );

                        // Call handler
                        this.callGameHander( handler -> handler.handlePlayerQuit( (GamePlayer) entity, true ) );
                        break;
                    }
                }

                // Check if game stopped
                if ( !this.running ) break;

                // Check if the entity is playing and not kicked
                if ( !this.entities.contains( entity ) ) {
                    continue;
                }

                // Get result from roll
                Category category = this.currentRoll.getResult().get();

                // Save points
                entity.setPoints( category, category.getPoints( this.currentRoll.getDice() ) );

                // Call handler
                this.callGameHander( handler -> handler.handleDiceRollEnd( entity, category, entity.getPoints( category ) ) );

                try {
                    // Wait 2 seconds (when bot) or 1 second
                    Thread.sleep( entity.isBot() ? 2000L : 1000L );
                } catch ( InterruptedException e ) {
                    // Stop game on error
                    this.stop( false );
                }

                // Check if game stopped
                if ( !this.running ) break;

                // Increment player
                this.currentEntityIndex++;

                // Check if player exists
                if ( this.currentEntityIndex >= this.entities.size() ) {
                    // Player not exists. Start with first one
                    this.currentEntityIndex = 0;

                    // Check if all categories has a value
                    if ( this.getCurrentEntity().getPoints().size() == Category.values().length ) {
                        this.stop( true );
                        return;
                    }
                }
            }
        } ).start();
    }

    /**
     * Stop the game
     *
     * @param finish game?
     */
    public void stop( boolean finish ) {
        if ( !this.running ) return;

        // Call handler
        this.callGameHander( handler -> handler.handleEnd( this.getRanking(), ( System.currentTimeMillis() - this.gameStart ), finish ) );

        // Remove from game list
        this.gameManager.removeGame( this );

        // Set running to false
        this.running = false;
    }

    /**
     * Called when the handler has a new action
     *
     * @param consumer with the handler
     */
    public void callGameHander( Consumer<GameHandler> consumer ) {
        // Check if multiplayer online game
        if ( this.type.equals( Type.MULTIPLAYER_ONLINE ) ) {
            // Yes, call handler in same thread
            this.gameHandlers.forEach( consumer );
            return;
        }
        // No, call handler in fx thread
        Platform.runLater( () -> this.gameHandlers.forEach( consumer ) );
    }

    /**
     * Remove player from game
     *
     * @param player to remove
     */
    public void removePlayer( GamePlayer player ) {
        // Check if player in list
        if ( !this.entities.contains( player ) ) return;

        // Get index from player object
        int index = this.entities.indexOf( player );

        // Remove player
        this.entities.remove( index );

        if ( this.currentEntityIndex == index ) {
            // Call handler
            this.callGameHander( handler -> handler.handleDiceRollEnd( player, null, -1 ) );

            // Check if currently waiting for result
            if ( this.waitForResult ) {
                // Set result to null and wake up thread
                this.currentRoll.setResult( null );
            }
        }

        // Call handler
        this.callGameHander( handler -> handler.handlePlayerQuit( player, false ) );

        // Check if list empty
        if ( this.entities.size() <= 1 ) {
            this.stop( false );
        }
    }

    /**
     * Get the current Entity
     *
     * @return the current Entity
     */
    public AbstractEntity getCurrentEntity() {
        if ( this.currentEntityIndex >= this.entities.size() ) {
            if ( this.entities.isEmpty() ) {
                return null;
            }
            this.currentEntityIndex = 0;
        }
        return this.entities.get( this.currentEntityIndex );
    }

    /**
     * Calculate the ranking with the points
     *
     * @return the ranking
     */
    private List<AbstractEntity> getRanking() {
        return this.entities.stream().sorted( Comparator.comparingInt( AbstractEntity::getAllPoints ).reversed() ).collect( Collectors.toList() );
    }

    public enum Type {
        SINGLEPLAYER,
        MULTIPLAYER_LOCAL,
        MULTIPLAYER_ONLINE;
    }
}
