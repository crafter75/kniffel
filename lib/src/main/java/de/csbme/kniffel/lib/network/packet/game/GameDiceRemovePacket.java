package de.csbme.kniffel.lib.network.packet.game;

import de.csbme.kniffel.lib.game.util.Dice;
import de.csbme.kniffel.lib.network.Packet;
import lombok.Getter;

public class GameDiceRemovePacket extends Packet {

    private int dice;

    @Getter
    private int diceIndex;

    public GameDiceRemovePacket( Dice dice, int diceIndex ) {
        this.dice = dice.ordinal();
        this.diceIndex = diceIndex;
    }

    public Dice getDice() {
        return Dice.values()[this.dice];
    }
}
