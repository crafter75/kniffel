package de.csbme.kniffel.lib.network.packet;

import de.csbme.kniffel.lib.network.Packet;
import de.csbme.kniffel.lib.user.Stats;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class UserStatsResponsePacket extends Packet {

    private String username;

    private Stats stats;

    private int ranking;
}
