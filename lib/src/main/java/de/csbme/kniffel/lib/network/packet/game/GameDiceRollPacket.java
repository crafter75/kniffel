package de.csbme.kniffel.lib.network.packet.game;

import de.csbme.kniffel.lib.game.util.Dice;
import de.csbme.kniffel.lib.network.Packet;
import lombok.Getter;

import java.util.List;
import java.util.stream.Collectors;

public class GameDiceRollPacket extends Packet {

    private List<Integer> dice;

    @Getter
    private List<Integer> savedIndex;

    @Getter
    private int rollsLeft;

    public GameDiceRollPacket( List<Dice> dice, List<Integer> savedIndex, int rollsLeft ) {
        this.dice = dice.stream().map( Enum::ordinal ).collect( Collectors.toList() );
        this.savedIndex = savedIndex;
        this.rollsLeft = rollsLeft;
    }

    public List<Dice> getDice() {
        return this.dice.stream().map( i -> Dice.values()[i] ).collect( Collectors.toList() );
    }
}
