package de.csbme.kniffel.lib.time;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class TimeUtil {

    private static final DateTimeFormatter DATE_FORMATTER_FULL = DateTimeFormatter.ofPattern( "dd.MM.yyyy HH:mm:ss" ),
            DATE_FORMATTER_ONLY_DATE = DateTimeFormatter.ofPattern( "dd.MM.yy" );

    /**
     * Format a date from milliseconds
     *
     * @param time          to format
     * @param showTimeOfDay of the date
     * @return the formatted date
     */
    public static String getDateFromMilliseconds( long time, boolean showTimeOfDay ) {
        // Check if lower or equals 0
        if ( time <= 0L ) {
            return "Unbekannt";
        }
        // Get full formatter
        DateTimeFormatter formatter = DATE_FORMATTER_FULL;

        // Check if format with time should be shown
        if ( !showTimeOfDay ) {
            // No, replace with date formatter
            formatter = DATE_FORMATTER_ONLY_DATE;
        }
        // Format time
        return Instant.ofEpochMilli( time ).atZone( ZoneId.systemDefault() ).format( formatter );
    }

    /**
     * Format milliseconds to Days, Hours, Minutes, Seconds
     *
     * @param time to format
     * @return the formatted string
     */
    public static String getDurationFromMilliseconds( long time ) {
        // Check if lower or equals 0
        if ( time <= 0L ) {
            return "Unbekannt";
        }
        // Milliseconds to full seconds
        int fullSeconds = (int) ( time / 1000 );

        // Full seconds to hours
        int hours = ( fullSeconds % 86400 ) / 3600;

        // Full seconds to minutes
        int minutes = ( fullSeconds % 3600 ) / 60;

        // Full seconds to rest seconds
        int seconds = fullSeconds % 60;

        // Create builder
        StringBuilder builder = new StringBuilder();

        // Check if hours greater then 0
        if ( hours > 0 ) {
            // Add hours
            builder.append( hours ).append( " " ).append( hours == 1 ? "Stunde" : "Stunden" ).append( " " );
        }

        // Check if minutes greater then 0
        if ( minutes > 0 ) {
            // Add minutes
            builder.append( minutes ).append( " " ).append( minutes == 1 ? "Minute" : "Minuten" ).append( " " );
        }

        // Check if seconds greater then 0
        if ( seconds > 0 ) {
            // Add hours
            builder.append( seconds ).append( " " ).append( seconds == 1 ? "Sekunde" : "Sekunden" ).append( " " );
        }
        return builder.toString();
    }
}
