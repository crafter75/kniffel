package de.csbme.kniffel.lib.game.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import de.csbme.kniffel.lib.game.Game;
import de.csbme.kniffel.lib.game.GameManager;
import de.csbme.kniffel.lib.game.player.AbstractEntity;
import de.csbme.kniffel.lib.game.player.GamePlayer;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class GameSerialization {

    @Getter
    private static final Gson GAME_SERIALIZER = new GsonBuilder().registerTypeAdapter( Game.class, new GameSerializer() ).create();

    /**
     * Serialization for Game
     */
    private static class GameSerializer extends TypeAdapter<Game> {

        @Override
        public void write( JsonWriter writer, Game game ) throws IOException {
            writer.beginObject();
            writer.name( "id" ).value( game.getGameId().toString() );
            writer.name( "players" ).value( StringUtils.join( game.getEntities().stream().map( AbstractEntity::getName ).collect( Collectors.toList() ), ";" ) );
            writer.endObject();
        }

        @Override
        public Game read( JsonReader reader ) throws IOException {
            UUID gameId = null;
            List<GamePlayer> players = null;

            reader.beginObject();
            while ( reader.hasNext() ) {
                switch ( reader.nextName().toLowerCase() ) {
                    case "id":
                        gameId = UUID.fromString( reader.nextString() );
                        break;
                    case "players":
                        players = Arrays.stream( reader.nextString().split( ";" ) ).map( GamePlayer::new ).collect( Collectors.toList() );
                        break;
                }
            }
            reader.endObject();

            if ( gameId != null && players != null ) {
                return new Game( GameManager.getInstance(), gameId, Game.Type.MULTIPLAYER_ONLINE ).withPlayers( players );
            }
            return null;
        }
    }
}
