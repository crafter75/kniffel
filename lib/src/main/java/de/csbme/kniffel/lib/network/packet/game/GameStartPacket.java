package de.csbme.kniffel.lib.network.packet.game;

import de.csbme.kniffel.lib.game.Game;
import de.csbme.kniffel.lib.game.util.GameSerialization;
import de.csbme.kniffel.lib.network.Packet;

public class GameStartPacket extends Packet {

    private String jsonGame;

    public GameStartPacket( Game game ) {
        this.jsonGame = GameSerialization.getGAME_SERIALIZER().toJson( game );
    }

    public Game getGame() {
        return GameSerialization.getGAME_SERIALIZER().fromJson( this.jsonGame, Game.class );
    }
}
