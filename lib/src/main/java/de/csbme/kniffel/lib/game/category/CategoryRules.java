package de.csbme.kniffel.lib.game.category;

import de.csbme.kniffel.lib.game.util.Dice;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class CategoryRules {

    static final Predicate<List<Dice>> ALLOW_EVERY_DICE = dice -> true;

    /**
     * Check if the list of dice has one die from type parameter
     *
     * @param die to check
     * @return a Predicate with the result
     */
    static Predicate<List<Dice>> hasAtLeastOne( Dice die ) {
        return dice -> dice.contains( die );
    }

    /**
     * Check if the dice list has all the same value
     *
     * @return a Predicate with the result
     */
    static Predicate<List<Dice>> allDiceAreEqual() {
        return dice -> dice.stream().allMatch( die -> die.equals( dice.get( 0 ) ) );
    }

    /**
     * Check if the dice list has a die with a specific amount
     *
     * @param times of the die
     * @return a Predicate with the result
     */
    static Predicate<List<Dice>> oneDieOccursAtLeastXTimes( int times ) {
        return dice -> dice.stream().anyMatch( die -> dice.stream().filter( d -> d.equals( die ) ).count() >= times );
    }

    /**
     * Check if the dice list has a another die with a specific amount
     *
     * @param times of the die
     * @return a Predicate with the result
     */
    static Predicate<List<Dice>> anotherDieOccursAtLeastXTimes( int times ) {
        return dice -> dice.stream().filter( die -> dice.stream().filter( d -> d.equals( die ) ).count() < 3 )
                .anyMatch( die -> dice.stream().filter( d -> d.equals( die ) ).count() >= times );
    }

    /**
     * Check if the dice list has dice in a row
     *
     * @param length of the row
     * @return a Predicate with the result
     */
    static Predicate<List<Dice>> diceInARow( int length ) {
        Set<Set<Dice>> diceRows = IntStream.rangeClosed( 0, Dice.values().length - length )
                .mapToObj( i -> getDiceRow( i, length ) ).collect( Collectors.toSet() );

        return dice -> diceRows.stream().anyMatch( dice::containsAll );
    }

    /**
     * Get all dice between start and length
     *
     * @param start  of the dice
     * @param length of the row
     * @return the dice as Set
     */
    private static Set<Dice> getDiceRow( int start, int length ) {
        return Arrays.stream( Dice.values(), start, start + length ).collect( Collectors.toSet() );
    }
}
