package de.csbme.kniffel.lib.game.player;

import de.csbme.kniffel.lib.game.category.Category;
import de.csbme.kniffel.lib.game.util.Dice;
import de.csbme.kniffel.lib.game.util.Roll;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class GameBot extends AbstractEntity {

    public GameBot( String name ) {
        super( name, true );
    }

    /**
     * Calculate the best category for the bot
     *
     * @param roll to calculate
     */
    public void calculate( Roll roll ) {
        Category lastCategory = null;
        Category toCalculateCategory = null;
        while ( true ) {

            // Check if game running
            if ( !roll.getGame().isRunning() ) break;

            try {
                Thread.sleep( 3000L );
            } catch ( InterruptedException e ) {
                e.printStackTrace();
            }

            // Check if game running
            if ( !roll.getGame().isRunning() ) break;

            // Get dice, categories for calculation
            List<Dice> dice = roll.getDice();
            Set<Category> categories = roll.getPossibleCategories();
            List<Category> availableCategories = this.getAvailableCategories();

            // Kniffel is the best. Use it if available
            if ( categories.contains( Category.KNIFFEL ) ) {
                roll.setResult( Category.KNIFFEL );
                return;
            }

            // Filter the best category with the most points
            Optional<Category> best = categories.stream().filter( category -> !category.equals( Category.CHANCE ) )
                    .max( Comparator.comparingInt( value -> value.getPoints( dice ) ) );

            // Check if category found
            if ( !best.isPresent() ) {
                if ( availableCategories.size() == 1 && availableCategories.contains( Category.CHANCE ) ) {
                    // Only chance left. Save high dice (6, 5, 4)
                    Set<Integer> indexes = IntStream.range( 0, dice.size() ).filter( i ->
                            Arrays.stream( Dice.values(), 3, Dice.values().length ).anyMatch( die ->
                                    dice.get( i ).equals( die ) ) ).boxed().collect( Collectors.toSet() );
                    indexes.forEach( roll::saveDice );
                }

                // Check if three of a kind or four of a kind available
                if ( ( availableCategories.contains( Category.THREE_OF_A_KIND ) || availableCategories.contains( Category.FOUR_OF_A_KIND ) )
                        && ( toCalculateCategory == null || toCalculateCategory.equals( Category.FOUR_OF_A_KIND )
                        || toCalculateCategory.equals( Category.THREE_OF_A_KIND ) ) ) {
                    // Get dice with the highest amount
                    Optional<Dice> optionalHighestAmountOfDice = dice.stream().filter( d -> dice.stream().filter( otherDie ->
                            otherDie.equals( d ) ).count() > 1 ).max( Comparator.comparingDouble( d ->
                            dice.stream().filter( otherDie -> otherDie.equals( d ) ).count() ) );

                    // Check if exists
                    if ( optionalHighestAmountOfDice.isPresent() ) {
                        // Get all dices with the highest amount
                        Dice highestAmountOfDice = optionalHighestAmountOfDice.get();
                        Set<Integer> indexes = IntStream.range( 0, dice.size() ).filter( i ->
                                dice.get( i ).equals( highestAmountOfDice ) ).boxed().collect( Collectors.toSet() );

                        // Save dices
                        indexes.forEach( roll::saveDice );

                        // Save category for next calculation in the next roll
                        if ( toCalculateCategory == null ) {
                            toCalculateCategory = availableCategories.contains( Category.FOUR_OF_A_KIND ) ? Category.FOUR_OF_A_KIND
                                    : Category.THREE_OF_A_KIND;
                        }
                    } else if ( roll.getSavedIndex().isEmpty() ) {
                        // Get highest dice
                        Optional<Integer> optionalIndex = IntStream.range( 0, dice.size() ).boxed().max(
                                Comparator.comparingInt( i -> dice.get( i ).getValue() ) );

                        // Save highest dice if found
                        optionalIndex.ifPresent( roll::saveDice );

                        // Save category for next calculation in the next roll
                        if ( toCalculateCategory == null ) {
                            toCalculateCategory = availableCategories.contains( Category.FOUR_OF_A_KIND ) ? Category.FOUR_OF_A_KIND
                                    : Category.THREE_OF_A_KIND;
                        }
                    }
                    // Check if small or big street available
                } else if ( ( availableCategories.contains( Category.SMALL_STREET ) || availableCategories.contains( Category.BIG_STREET ) )
                        && ( toCalculateCategory == null || toCalculateCategory.equals( Category.SMALL_STREET )
                        || toCalculateCategory.equals( Category.BIG_STREET ) ) ) {
                    // Get set with 3 dices in a row
                    Optional<Set<Dice>> optionalDicesToSave = getDicesInARow( dice, 3 );

                    // Check if 3 dices in a row
                    if ( !optionalDicesToSave.isPresent() ) {
                        // No, then get the set of 2 dices in a row
                        optionalDicesToSave = getDicesInARow( dice, 2 );
                    }

                    // Check if 3 or 2 dices in a row
                    if ( optionalDicesToSave.isPresent() ) {
                        // Get dices as set
                        Set<Dice> dicesToSave = optionalDicesToSave.get();

                        // Get all indexes of dice (non duplicate)
                        Set<Integer> indexes = dicesToSave.stream().filter( die -> roll.getSavedIndex().stream().noneMatch( i ->
                                dice.get( i ).equals( die ) ) ).mapToInt( dice::indexOf ).boxed().collect( Collectors.toSet() );

                        // Save index
                        indexes.forEach( roll::saveDice );

                        // Save category for next calculation in the next roll
                        if ( toCalculateCategory == null ) {
                            toCalculateCategory = availableCategories.contains( Category.BIG_STREET ) ? Category.BIG_STREET
                                    : Category.SMALL_STREET;
                        }
                    }
                }

                // Check if no rolls left
                if ( roll.getRollsLeft() == 0 ) {
                    // Reset attribute
                    if ( toCalculateCategory != null ) {
                        toCalculateCategory = null;
                    }

                    // Choose kniffel category at first if it available
                    if ( !this.hasCategory( Category.KNIFFEL ) ) {
                        roll.setResult( Category.KNIFFEL );
                        return;
                    }
                    // Choose big street category at second if it available
                    if ( !this.hasCategory( Category.BIG_STREET ) ) {
                        roll.setResult( Category.BIG_STREET );
                        return;
                    }
                    // Choose Chance category at third if it available
                    if ( !this.hasCategory( Category.CHANCE ) ) {
                        roll.setResult( Category.CHANCE );
                        return;
                    }
                    // Choose a random category at the last
                    roll.setResult( availableCategories.get( new Random().nextInt( availableCategories.size() ) ) );
                    return;
                }
                // Roll again
                roll.roll();
                continue;
            }
            // Get category and dice for the category
            Category category = best.get();
            Optional<Dice> optionalDice = this.getDiceForCategory( category );

            // Check if dice exists
            if ( optionalDice.isPresent() ) {
                // Get dice
                Dice search = optionalDice.get();

                // Count dice from current category
                long diceAmount = dice.stream().filter( die -> die.equals( search ) ).count();

                // Search for the highest dice amount
                final long finalDiceAmount = diceAmount;
                Optional<Dice> optionalMaxDice = dice.stream().filter( die -> !die.equals( search )
                        && dice.stream().filter( die::equals ).count() > finalDiceAmount )
                        .max( Comparator.comparingLong( die -> dice.stream().filter( die::equals ).count() ) );

                // Check if found
                if ( optionalMaxDice.isPresent() ) {
                    // Get Dice
                    Dice die = optionalMaxDice.get();

                    // Get category from dice
                    Optional<Category> optionalNewCategory = this.getCategoryForDice( die );

                    // Check if exists
                    if ( optionalNewCategory.isPresent() && !this.hasCategory( optionalNewCategory.get() ) ) {
                        // Update current category and amount
                        category = optionalNewCategory.get();
                        diceAmount = dice.stream().filter( otherDie -> otherDie.equals( die ) ).count();
                    }
                }

                // Check if last category exists, not selected and not the same category
                if ( lastCategory != null && !this.hasCategory( lastCategory ) && !lastCategory.equals( category ) ) {
                    // Get dice from last category
                    Optional<Dice> optionalLastDice = this.getDiceForCategory( lastCategory );

                    // Check if exists
                    if ( optionalLastDice.isPresent() ) {
                        // Check if dice amount from last category higher than the current dice amount of the current category
                        if ( diceAmount < dice.stream().filter( die -> die.equals( optionalLastDice.get() ) ).count() ) {
                            // Update current category
                            category = lastCategory;
                        }
                    }
                }
            }
            // Update last category to current
            lastCategory = category;

            // Check if the category has not a fixed value and more then one roll left
            if ( getSummarizeCategories().contains( category ) && roll.getRollsLeft() > 0 ) {
                // Get all indexes from current dice list
                Set<Integer> indexes = this.getDiceIndexesToSave( category, dice, roll.getSavedIndex() );

                // Iterate and save the index
                indexes.forEach( roll::saveDice );

                // Roll again
                roll.roll();
                continue;
            }

            // Reset attribute
            if ( toCalculateCategory != null ) {
                toCalculateCategory = null;
            }

            // Use four of a kind instead of three of a kind if available
            if ( category.equals( Category.THREE_OF_A_KIND ) && categories.contains( Category.FOUR_OF_A_KIND ) ) {
                category = Category.FOUR_OF_A_KIND;
            }

            // Set result from best category
            roll.setResult( category );
            return;
        }
    }

    /**
     * Get all indexes from the dice list to save it
     *
     * @param category   we search for
     * @param dice       list of the roll
     * @param savedIndex where already indexes saved
     * @return the indexes as set
     */
    private Set<Integer> getDiceIndexesToSave( Category category, List<Dice> dice, List<Integer> savedIndex ) {
        // Get dice for category
        Optional<Dice> optionalDice = this.getDiceForCategory( category );

        // Check if category already selected
        if ( this.hasCategory( category ) ) {
            // Yes, return empty set
            return Collections.emptySet();
        }

        // Check if dice exists
        if ( !optionalDice.isPresent() ) {
            // Check if indexes already saved
            if ( savedIndex.size() > 0 ) {
                // Yes, filter for same indexes then already in the list and save it into a set
                return IntStream.range( 0, dice.size() ).filter( i -> dice.get( i ).equals( dice.get( savedIndex.get( 0 ) ) )
                        && !savedIndex.contains( i ) ).boxed().collect( Collectors.toSet() );
            }
            // Get the dice with the max amount
            Optional<Dice> optionalMaxDiceAmount = dice.stream().filter( die -> dice.stream().filter( die::equals )
                    .count() > 1 ).max( Comparator.comparingLong( die ->
                    dice.stream().filter( die::equals ).count() ) );

            // Save all indexes and return it as a set
            return optionalMaxDiceAmount.map( die -> IntStream.range( 0, dice.size() ).filter( i -> dice.get( i ).equals( die ) )
                    .boxed().collect( Collectors.toSet() ) ).orElse( Collections.emptySet() );
        }
        // Filter for indexes of the dice from current category and save it into a set
        return IntStream.range( 0, dice.size() ).filter( i -> dice.get( i ).equals( optionalDice.get() ) ).boxed()
                .collect( Collectors.toSet() );
    }

    /**
     * Get dice from a category
     *
     * @param category of the dice
     * @return the dice as optional
     */
    private Optional<Dice> getDiceForCategory( Category category ) {
        switch ( category ) {
            case ONES:
                return Optional.of( Dice.ONE );
            case TWOS:
                return Optional.of( Dice.TWO );
            case THREES:
                return Optional.of( Dice.THREE );
            case FOURS:
                return Optional.of( Dice.FOUR );
            case FIVES:
                return Optional.of( Dice.FIVE );
            case SIXES:
                return Optional.of( Dice.SIX );
            default:
                return Optional.empty();
        }
    }

    /**
     * Get category from dice
     *
     * @param dice of the category
     * @return the category as optional
     */
    private Optional<Category> getCategoryForDice( Dice dice ) {
        switch ( dice ) {
            case ONE:
                return Optional.of( Category.ONES );
            case TWO:
                return Optional.of( Category.TWOS );
            case THREE:
                return Optional.of( Category.THREES );
            case FOUR:
                return Optional.of( Category.FOURS );
            case FIVE:
                return Optional.of( Category.FIVES );
            case SIX:
                return Optional.of( Category.SIXES );
            default:
                return Optional.empty();
        }
    }

    /**
     * Get all categories where the result not a fixed value and the values summarize
     *
     * @return the categories as set
     */
    private Set<Category> getSummarizeCategories() {
        Set<Category> set = Arrays.stream( Category.values(), 0, 6 ).collect( Collectors.toSet() );
        set.add( Category.THREE_OF_A_KIND );
        set.add( Category.FOUR_OF_A_KIND );
        return set;
    }

    /**
     * Get all dices in a row to save
     *
     * @param dice   to calculate
     * @param length of the row
     * @return the dices as set
     */
    private Optional<Set<Dice>> getDicesInARow( List<Dice> dice, int length ) {
        Set<Set<Dice>> diceRows = IntStream.rangeClosed( 0, Dice.values().length - length )
                .mapToObj( i -> getDiceRow( i, length ) ).collect( Collectors.toSet() );

        return diceRows.stream().filter( dice::containsAll ).findFirst();
    }

    /**
     * Get all dice between start and length
     *
     * @param start  of the dice
     * @param length of the row
     * @return the dice as Set
     */
    private static Set<Dice> getDiceRow( int start, int length ) {
        return Arrays.stream( Dice.values(), start, start + length ).collect( Collectors.toSet() );
    }
}
